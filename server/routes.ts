import express from "express";
import { isAdmin, isLoggedIn, upload } from "./guards";
import { medalController, postController, recycleBinController, shopController, userController, dmController, tipsController} from "./main";

export const routes = express.Router();

console.log(isLoggedIn);

// add isLoggedIn for post routes VVVVVVV
routes.get("/posts", postController.loadPosts);
routes.post("/post", isLoggedIn, upload.any(), postController.createPost);
routes.put("/post/:postID", isLoggedIn, postController.editPost);
routes.delete("/post/:postID", isLoggedIn, isAdmin, postController.deletePost);
routes.get("/post-photos/:postID", postController.loadPostPhotos);
routes.post("/post-photos/:postID", isLoggedIn, upload.array("uploads"), postController.insertPostPhotos);
routes.get("/save-post/:postID", isLoggedIn, postController.getSavedStatus);
routes.post("/save-post/:postID", isLoggedIn, postController.savePost);
routes.get("/saved-posts", isLoggedIn, postController.loadSavedPosts);
routes.get("/newestPost", postController.loadNewestPost);
routes.put("/backPost/:postID", isLoggedIn, isAdmin, postController.putBackPost);

// DM related routes
routes.post("/direct-message", isLoggedIn, upload.any(), dmController.postDM);
routes.get("/direct-messages", isLoggedIn, dmController.loadDMs);
routes.get("/adminDMs", isLoggedIn, isAdmin, dmController.loadAdminDMs);
routes.get("/allUsers", isLoggedIn, dmController.loadAllUsers);

// recycle-bin routes
routes.get("/search/:districtID", recycleBinController.searchRecycleBin);
routes.get("/bins", recycleBinController.loadRecycleBin);
routes.get("/district", recycleBinController.getDistrict);

//user routes
routes.get("/currentUser", isLoggedIn, userController.checkLogin);
routes.post("/checkUsername",userController.checkUsername);
routes.post("/checkEmail", userController.checkEmail);
routes.post("/login", userController.login);
routes.post("/register", userController.register);
routes.put("/newPassword", isLoggedIn, userController.newPassword);
routes.post("/checkPassword", isLoggedIn, userController.checkPassword);
routes.get("/checkAdmin", isLoggedIn, userController.checkAdmin);

routes.get("/ecoTypes", postController.loadEcoTypes);


//shop routes
routes.get("/shop", shopController.getShop);
routes.post("/shop", isLoggedIn, upload.any(), shopController.addShop);

// medal routes
routes.get('/objective',isLoggedIn,medalController.loadAllObjectives);
routes.put('/level/:id', medalController.addLevel);
routes.get('/level/:id', medalController.loadLevel);
routes.get('/objective/login',isLoggedIn,medalController.firstLogin);

// tips routes
routes.get('/tips', tipsController.loadTips);
routes.get('/tips-photos/:tipsID', tipsController.loadPhotos);