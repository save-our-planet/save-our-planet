import type Knex from "knex";

export class MedalService {
    constructor(private knex: Knex) {}

    public getObjective = async (id: number) => {
        try {
            await this.insertUserAchievement(id);

            const result = await this.knex
                .select(
                    "objective.*",
                    "objective_category.name as objective_category",
                    "user_achievement.*",
                    "medalImg.ImgName"
                )
                .from("objective")
                .leftJoin("objective_category", "objective.category_id", "objective_category.id")
                .leftJoin("user_achievement", "user_achievement.objective_id", "objective.id")
                .leftJoin('medalImg',"medalImg.objective_id","objective.id")
                .where("user_id", id)
                .orderBy("level", "desc");

            // console.log(result);

            return result;
        } catch (e) {
            throw e;
        }
    };

    public insertUserAchievement = async (id: number) => {
        // const trx = await this.knex.transaction();
        try {
            const objectiveId = await this.knex("objective").select("id");
            // console.log(objectiveId);
            const userAchievementId = await this.knex("user_achievement")

                .select("objective_id as id")
                .where("user_id", id);
            // console.log(userAchievementId);
            const objectiveIdMap = objectiveId.map((objective) => objective.id);
            const userAchievementIdMap = userAchievementId.map(
                (userAchievement) => userAchievement.id
            );
            // console.log(objectiveIdMap);
            // console.log("userAchievementIdMap ", userAchievementIdMap);

            if (objectiveIdMap.length !== userAchievementIdMap.length) {
                console.log("objectiveIdMap.length != userAchievementIdMap.length");

                const insertData = objectiveIdMap.filter(
                    (obj) => userAchievementIdMap.indexOf(obj) == -1
                );
                // console.log(insertData);
                for (const data of insertData) {
                    await this.knex("user_achievement").insert({
                        objective_id: data,
                        user_id: id,
                    });
                }
                // await trx.commit();
            }
        } catch (e) {
            // await trx.rollback();
            throw e;
        }
    };

    public updateLevel = async (id: number, level: number) => {
        return await this.knex("user_achievement")
            .update("level", level)
            .returning(["id", "level"])
            .where("id", id);
    };
    public getLevel = async (id: number) => {
        return await this.knex("user_achievement").select("level").where("id", id);
    };
    public getFirstLogin = async (id: number) => {
        let currentLevel = await this.knex("user_achievement")
            .select(
                "user_achievement.level",
                "user_achievement.user_id",
                "objective.name",
                "objective.required_star",
                "user_achievement.id"
            )
            .innerJoin("objective", "objective.id", "user_achievement.objective_id")
            .where("name", "登入獎勵")
            .where("user_id", id);
        if (currentLevel[0].level !== currentLevel[0].required_star) {
            return await this.knex("user_achievement")
                .update("level", currentLevel[0].required_star)
                .returning(["id", "level"])
                .where("id", currentLevel[0].id)
                .where("user_id", id);
                
        }
        return currentLevel
    };
    // public getLevelObjective = async (id: number) => {
    //     try {

    //         const result = await this.knex
    //             .select(
    //                 "objective.*",
    //                 "objective_category.name as objective_category",
    //                 "user_achievement.*"
    //             )
    //             .from("objective")
    //             .leftJoin("objective_category", "objective.category_id", "objective_category.id")
    //             .leftJoin("user_achievement", "user_achievement.objective_id", "objective.id")
    //             .where("user_id", id)
    //             .orderBy("level", "desc");

    //         // console.log(result.length);

    //         return result;
    //     } catch (e) {
    //         throw e;
    //     }
    // };
}
