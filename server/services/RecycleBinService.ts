import type Knex from "knex";

export class RecycleBinService {
    constructor(private knex: Knex) {}

    public getRecycleBin = async () => {
        try {
            const result = await this.knex
                .select(
                    "recycle_bin.*",
                    "district.name as district",
                    // "location_type.name as location_type"
                )
                .from("recycle_bin")
                .innerJoin("district", "recycle_bin.district_id", "district.id")
                // .innerJoin("location_type", "recycle_bin.location_type_id", "location_type.id");
            
            return result;
        } catch (e) {
            throw e;
        }
    };

    public searchRecycleBin = async (
        // location_type: string,
        districtID: string
    ) => {
        try {
            const results = await this.knex
                .select(
                    "recycle_bin.*",
                    "district.name as district",
                    // "location_type.name as location_type"
                )
                .from("recycle_bin")
                .leftJoin("district", "recycle_bin.district_id", "district.id")
                // .leftJoin("location_type", "recycle_bin.location_type_id", "location_type.id")
                .where("district.id", parseInt(districtID));
            // .andWhere("location_type.name", location_type);
            console.log(results)

            return results;
        } catch (e) {
            throw e;
        }
    };
    public getDistrict = async () => {
        try {
            const result = await this.knex.select("*").from("district");
            return result;
        } catch (e) {
            throw e;
        }
    };
}
