import Knex from "knex";

export class TipsService {
    public constructor(private knex: Knex){

    }

    public selectTips = async () => {
        return await this.knex("recycle_tips")
                .select("*")
                .orderBy("id", "asc")
    }

    public selectPhotos = async(tipsID: number) => {
        return await this.knex("recycle_tips_photo")
                        .select("*")
                        .where('recycle_tips_id', tipsID)
    }

}