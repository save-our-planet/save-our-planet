import type Knex from 'knex';

export class PostService {
    public constructor(private knex: Knex){

    }

    public insertPost = async (title: string, content: string, user_id: number, shop_id: string, eco_type_id: string) => {
        return await this.knex("post")
                    .insert({
                        title,
                        content,
                        user_id,
                        shop_id,
                        eco_type_id
                    }).returning('id')
    }

    public selectPosts = async () => {
        return await this.knex("post")
                        .select(
                            "post.*",
                            "user.name as username",
                            "shop.name as shop_name",
                            "shop.address as shop_address",
                            "shop.district_id as district_id",
                            "eco_type.name as eco_type"
                            )
                        .innerJoin("user", "post.user_id", "user.id")
                        .innerJoin("eco_type", "post.eco_type_id", "eco_type.id")
                        .innerJoin("shop", "post.shop_id", "shop.id")
                        .orderBy("created_at", "desc")
    }

    public updatePost = async (id: string, title: string, content: string) => {
        let hasPost = await this.knex("post")
                            .select("*")
                            .where('id', id);

                            
        if (hasPost.length > 0){
            return await this.knex("post")
                            .where('id', id)
                            .update({
                                title,
                                content
                            }).returning('id')

        } else {
            return false
        }
    }

    public deletePost = async (id: string) => {
        let hasPost = await this.knex("post")
                            .select("*")
                            .where('id', id);

        if (hasPost.length > 0){
            await this.knex("post")
                        .where('id', id)
                        .update('is_hidden', true)
            return true

        } else {
            return false
        }
    }

    public insertPhotos = async (files: any, post_id: string) => {
        let hasPost = await this.knex("post")
                            .select("*")
                            .where('id', post_id);

        if (hasPost.length > 0){
            for(let i in files){
                if (parseInt(i) === 0){
                    await this.knex("post_photo")
                            .where('id', post_id)
                                    .insert({
                                        path: files[i].key,
                                        is_cover: true,
                                        post_id
                                    })
                } else {
                    await this.knex("post_photo")
                                .where('id', post_id)
                                .insert({
                                    path: files[i].key,
                                    is_cover: false,
                                    post_id
                                })
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public selectPhotos = async (post_id: string) => {
        return await this.knex("post_photo")
                            .select("*")
                            .where("post_id", parseInt(post_id))
    }

    public checkSavedStatus = async (post_id: number, user_id: number) => {
        let isSaved = await this.knex("saved_post")
                                .select("*")
                                .where('post_id', post_id)
                                .andWhere('user_id', user_id)
        
        if(isSaved.length === 0){return false}
        return true
    }

    public saveOrUnsavePost = async (post_id: number, user_id: number) => {
        let hasPost = await this.knex("post")
                            .select("*")
                            .where('id', post_id)

        if(hasPost.length > 0){
            let savedPost = await this.knex("saved_post")
                                    .select("id")
                                    .where('post_id', post_id)
                                    .andWhere('user_id', user_id)
            
            if(savedPost.length > 0){
                await this.knex("saved_post")
                            .where('id', savedPost[0].id)
                            .delete();
                
                return false
            } else {
                await this.knex("saved_post")
                            .insert({
                                post_id,
                                user_id
                            }).returning('id')
                
                return true
            }
        } else {
            return null
        }
    }

    public selectEcoTypes = async () => {
        return await this.knex("eco_type").select("*")
    }

    public selectNewestPost = async () => {
        return await this.knex("post")
                        .select("post.*", "shop.name", "shop.address")
                        .innerJoin("shop", "post.shop_id", "shop.id")
                        .where("is_hidden", false)
                        .orderBy("created_at", "desc")
                        .limit(1)
    }

    public selectSavedPosts = async (id: number) => {
        return await this.knex("post")
                        .select(
                            "post.*",
                            "user.name as username",
                            "shop.name as shop_name",
                            "eco_type.name as eco_type",
                            "shop.address as shop_address"
                            )
                        .innerJoin("saved_post", "post.id", "saved_post.post_id")
                        .innerJoin("user", "post.user_id", "user.id")
                        .innerJoin("eco_type", "post.eco_type_id", "eco_type.id")
                        .innerJoin("shop", "post.shop_id", "shop.id")
                        .where("saved_post.user_id", id)
                        .orderBy("created_at", "desc")
    }

    public putBackPost = async (id: string) => {
        let hasPost = await this.knex("post")
                            .select("*")
                            .where('id', id);

        if (hasPost.length > 0){
            await this.knex("post")
                        .where('id', id)
                        .update('is_hidden', false)
            return true

        } else {
            return false
        }
    }

    public insertDM = async (
        admin_id: number,
        user_id: number, 
        sender_id: number, 
        content: string) => {

            return await this.knex("direct_message")
                            .insert({
                                admin_id,
                                user_id,
                                sender_id,
                                content
                            }).returning("*")
        }
}