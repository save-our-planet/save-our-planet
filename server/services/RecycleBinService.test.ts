import Knex from "knex";
import { RecycleBinService } from "./RecycleBinService";
const knexConfig = require("../knexfile");
const knex = Knex(knexConfig["testing"]);

describe("RecycleBin Service", () => {
    let recycleBinService: RecycleBinService;

    beforeEach(async () => {
        await knex.migrate.forceFreeMigrationsLock()
        // await knex.migrate.rollback();
        await knex.migrate.latest();
        await knex.seed.run();
        recycleBinService = new RecycleBinService(knex);
    });
    it("can load recycle bin", async () => {
        const data = await recycleBinService.getRecycleBin();

        expect(data).toHaveLength(74);
        expect(data[1].name).toBeTruthy();
        expect(data[1].address).toBeTruthy();
        expect(data[1].district_id).toBeTruthy();
        // expect(data[1].location_type_id).toBeTruthy();
    });
    it("can search recycle bin", async () => {
        const data = await recycleBinService.searchRecycleBin("2");

        expect(data[0]).toBeUndefined();
        // expect(data[0].address).toBeFalsy();
        // expect(data[0].district_id).toBeFalsy();
        // expect(data[0].location_type_id).toBeFalsy();
        // expect(data[0].location_type).toBeFalsy();
        // expect(data[0].district).toBeFalsy();
    });
});

afterAll(() => {
    knex.destroy();
});
