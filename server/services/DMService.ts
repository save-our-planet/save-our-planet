import type Knex from 'knex';

export class DMService {
    public constructor(private knex: Knex){

    }

    public insertDM = async (
        admin_id: number,
        user_id: number, 
        sender_id: number, 
        content: string,
        isAdmin: boolean) => {

            let dmID = await this.knex("direct_message")
                            .insert({
                                admin_id,
                                user_id,
                                sender_id,
                                content
                            }).returning("id")

            console.log("dmID:", dmID[0]);
            
            if (isAdmin === true){
                console.log("Admin");
                
                let dm = await this.knex("direct_message")
                    .select(
                        "direct_message.*",
                        "user.name"
                        )
                    .innerJoin("user", "direct_message.admin_id", "user.id")
                    .where("direct_message.id", dmID[0])

                let receiverName = await this.knex("direct_message")
                                .select(
                                    "user.name"
                                )
                                .innerJoin("user", "direct_message.user_id", "user.id")
                                .where("direct_message.id", dmID[0])

                return {
                    ...dm,
                    "receiver_name": receiverName[0].name
                }

            } else {
                console.log("user");
                
                let dm = await this.knex("direct_message")
                    .select(
                        "direct_message.*",
                        "user.name"
                        )
                    .innerJoin("user", "direct_message.user_id", "user.id")
                    .where("direct_message.id", dmID[0])

                let receiverName = await this.knex("direct_message")
                .select(
                    "user.name"
                )
                .innerJoin("user", "direct_message.admin_id", "user.id")
                .where("direct_message.id", dmID[0])

                    console.log(dm);
                    

                return {
                    ...dm[0],
                    "receiver_name": receiverName[0].name
                }
                
            }
        }

    public selectDMs = async (user_id: number) => {
        
        return await this.knex("direct_message")
                        .select(
                            "direct_message.*",
                            "user.name"
                            )
                        .innerJoin("user", "direct_message.admin_id", "user.id")
                        .where("direct_message.user_id", user_id)
                        .orderBy("direct_message.created_at", "desc")
    }

    public selectAdminDMs = async (admin_id: number) => {
        return await this.knex("direct_message")
                        .select(
                            "direct_message.*",
                            "user.name"
                            )
                        .innerJoin("user", "direct_message.user_id", "user.id")
                        .where("direct_message.admin_id", admin_id)
                        .orderBy("direct_message.created_at", "desc")
    }

    public selectAllUsers = async () => {
        return await this.knex("user")
                            .select("user.id", "user.name", "user.is_admin")
    }
}