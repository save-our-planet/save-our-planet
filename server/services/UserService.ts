import Knex from "knex";
// import { User } from "../guards";
import { hashPassword } from '../hash';

export class UserService {
    constructor(private knex: Knex){

    }

    async getUser(username: string){
        const result = await this.knex.select("*")
        .from('user')
        .where('name',username);
        // console.log("get user: ",result);
        
        return result
    }

    async getUserByEmail(email: string){
        const result = await this.knex.select("*")
        .from('user')
        .where('email',email);
        // console.log("get email: ",result);
        
        return result
    }

    async createUser(name: string, email: string, password: string){

        const hash = await hashPassword(password);
        return this.knex.insert({
            name, 
            email,
            password: hash 
        }).into('user').returning("*");
    }

    async changePassword(id:number, password: string) {

        const hash = await hashPassword(password);
        return this.knex.update({
            password: hash
        }).into("user")
        .where('id', id)
        .returning("*")
    } 

    // async getPassword(id: number, password: string){
    //     const result = await this.knex.select("*")
    //     .from('user')
    //     .where('id', id)
    // }

    async checkAdmin(id: number){
        return await this.knex("user")
                            .select("*")
                            .where("id", id)
        
    }
}