import Knex from "knex";

export class ShopService {
    constructor(private knex: Knex) {}

    public addShop = async (

        name: string, 
        address: string,
        districtID: number

        ) => {

        try{
            return this.knex
            .insert({
                name,
                address,
                district_id: districtID
            })
            .into("shop")
            .returning("*");

        }catch(e){
            throw e;
        }

    }

    public getShop = async () => {
        try {
            const result = await this.knex
                .select("shop.*", "district.name as district")
                .from("shop")
                .innerJoin("district", "shop.district_id", "district.id");

            return result;
        } catch (e) {
            throw e;
        }
    };
}
