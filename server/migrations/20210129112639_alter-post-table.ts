import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("post")){
        await knex.schema.alterTable("post", table =>{
            table.integer('eco_type_id').notNullable();
            table.foreign('eco_type_id').references('eco_type.id')
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("post")){
        await knex.schema.alterTable("post", table =>{
            table.dropColumn('eco_type_id');
        });  
    }
}

