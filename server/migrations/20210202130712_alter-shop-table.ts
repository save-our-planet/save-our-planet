import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("shop")){
        await knex.schema.alterTable("shop", table =>{
            table.string('name', 40).notNullable();
        });  
    }
}

export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("shop")){
        await knex.schema.alterTable("shop", table =>{
            table.dropColumn('name');
        });  
    }
}

