import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("shop", table=>{
        table.increments();
        table.string('address', 255).notNullable();
        table.integer('district_id').notNullable();
        table.foreign('district_id').references('district.id');
        table.timestamps(false, true);
    })

    await knex.schema.createTable("post", table=>{
        table.increments();
        table.string('title', 40).notNullable();
        table.text('content');
        table.integer('user_id').notNullable();
        table.foreign('user_id').references('user.id');
        table.integer('shop_id').notNullable();
        table.foreign('shop_id').references('shop.id');
        table.timestamps(false, true);
    })

    await knex.schema.createTable("post_photo", table=>{
        table.increments();
        table.string('path', 255).notNullable();
        table.boolean('is_cover').notNullable();
        table.integer('post_id').notNullable();
        table.foreign('post_id').references('post.id');
        table.timestamps(false, true);
    })

    await knex.schema.createTable("saved_post", table=>{
        table.increments();
        table.integer('post_id').notNullable();
        table.foreign('post_id').references('post.id');
        table.integer('user_id').notNullable();
        table.foreign('user_id').references('user.id');
        table.timestamps(false, true);
    })
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("saved_post")
    await knex.schema.dropTable("post_photo")
    await knex.schema.dropTable("post")
    await knex.schema.dropTable("shop")
}

