import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("user", table=>{
        table.increments();
        table.string('name', 40).notNullable();
        table.string('email', 255).notNullable();
        table.string('password', 255).notNullable();
        table.boolean('is_admin').defaultTo(false);
        table.timestamps(false, true);
    })
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("user");
}

