import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("medalImg", (t) => {
        t.increments();
        t.integer("objective_id").notNullable();
        t.foreign("objective_id").references("objective.id");
        t.string("ImgName").notNullable();
        t.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("medalImg");
}
