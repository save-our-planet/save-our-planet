import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("post")){
        await knex.schema.alterTable("post", table =>{
            table.boolean('is_hidden').notNullable().defaultTo(false);
        });  
    }
    
}


export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("post")){
        await knex.schema.alterTable("post", table =>{
            table.dropColumn('is_hidden');
        });  
    }
}

