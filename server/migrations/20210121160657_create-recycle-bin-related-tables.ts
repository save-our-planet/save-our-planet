import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("location_type", table => {
        table.increments();
        table.string('name', 40).notNullable();
        table.timestamps(false, true);
    })

    await knex.schema.createTable("recycle_bin", table => {
        table.increments();
        table.string('name', 40).notNullable();
        table.string('address', 255).notNullable();
        table.integer('district_id');
        table.foreign('district_id').references('district.id');
        table.integer('location_type_id');
        table.foreign('location_type_id').references('location_type.id');
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("recycle_bin")
    await knex.schema.dropTable("location_type")
}

