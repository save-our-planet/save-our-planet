import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("recycle_tips", table=>{
        table.increments();
        table.string('category', 255).notNullable();
        table.string('classification', 255).notNullable();
        table.string('characteristic', 255).notNullable();
        table.string('item', 255).notNullable();
        table.string('remark', 255)
        table.string('recycle_point', 255).notNullable();
        table.timestamps(false, true);
    })

    await knex.schema.createTable("recycle_tips_photo", table=>{
        table.increments();
        table.string('path', 255).notNullable();
        table.integer('recycle_tips_id').notNullable();
        table.foreign('recycle_tips_id').references('recycle_tips.id');
        table.timestamps(false, true);
    })
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("recycle_tips_photo");
    await knex.schema.dropTable("recycle_tips");
}

