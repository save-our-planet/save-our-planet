import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable("recycle_bin", table => {
        table.string('office_hour', 255);
        table.string('contact', 255);
        table.string('type',255);
    })
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable("recycle_bin", table => {
        table.dropColumn("type");
        table.dropColumn("contact");
        table.dropColumn("office_hour");
    })
}

