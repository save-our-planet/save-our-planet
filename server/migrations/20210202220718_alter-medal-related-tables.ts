import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.dropTable("medal_won");
    await knex.schema.dropTable("medal");
    await knex.schema.alterTable("user_achievement", (table) => {
        table.integer("level").notNullable().unsigned().defaultTo(0);
    });
    await knex.schema.alterTable("objective", (table) => {
        table.integer("required_star").notNullable().unsigned();
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.createTable("medal", (table) => {
        table.increments();
        table.string("name", 40).notNullable();
        table.integer("objective_id").notNullable();
        table.foreign("objective_id").references("objective.id");
        table.integer("category_id").notNullable();
        table.foreign("category_id").references("objective_category.id");
        table.decimal("objective_value").unsigned().notNullable();
        table.timestamps(false, true);
    });
    await knex.schema.createTable("medal_won", (table) => {
        table.increments();
        table.integer("medal_id").notNullable();
        table.foreign("medal_id").references("medal.id");
        table.integer("user_id").notNullable();
        table.foreign("user_id").references("user.id");
        table.timestamps(false, true);
    });
    await knex.schema.alterTable("user_achievement", (table) => {
        table.dropColumn("level");
    });
    await knex.schema.alterTable("objective", (table) => {
        table.dropColumn("required_star");
    });
}
