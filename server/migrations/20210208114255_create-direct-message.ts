import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("direct_message", table =>{
        table.increments();
        table.text('content').notNullable();
        table.integer('admin_id').notNullable();
        table.foreign('admin_id').references('user.id');
        table.integer('user_id').notNullable();
        table.foreign('user_id').references('user.id');
        table.integer('sender_id').notNullable();
        table.foreign('sender_id').references('user.id');
        table.boolean('is_read').defaultTo(false)
        table.timestamps(false, true);
    });  
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable("direct_message")
}

