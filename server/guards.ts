import multer from "multer";
import { Bearer } from "permit";
import jwtSimple from "jwt-simple";
import express from "express";
import { userService } from "./main";
import multerS3 from "multer-s3";
import AWS from "aws-sdk";
import dotenv from 'dotenv'

dotenv.config()

// const storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, `${__dirname}/uploads`);
//   },
//   filename: function (req, file, cb) {
//     cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
//   },
// });

// export const upload = multer({ storage });


const s3 = new AWS.S3({
  accessKeyId: process.env.REACT_APP_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.REACT_APP_AWS_SECRET_ACCESS_KEY,
  region: 'ap-southeast-1',
  // maxRetries: 5,
  // httpOptions: {
  //   connectTimeout: 120000, // time succeed in starting the call
  //   timeout: 0 , // time to wait for a response
    // the aws-sdk defaults to automatically retrying
    // if one of these limits are met.
  // },
});

export const upload = multer({
  storage: multerS3({
      s3: s3,
      bucket: 'greenbit-uploads',
      metadata: (req,file,cb)=>{
        
          cb(null,{fieldName: file.fieldname});
      },
      key: (req,file,cb)=>{
        
          cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
      }
  })
})

const permit = new Bearer({
  query: "access_token",
});

export async function isLoggedIn(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  
  const token = permit.check(req);

  try {
    // console.log(token);
    if (!token) {
    
      return res.status(401).json({ msg: "Permission Denied 1" });
    }

    const payload = jwtSimple.decode(token, process.env.JWT_SECRET!);
    
    
    const user= await userService.getUser(payload.username);

    
    if (user) {
      req['user'] = user[0];
      
      return next();
    } else {
      return res.status(401).json({ msg: "Permission Denied 2" });
    }
  } catch (e) {
    console.log(e);
    
    return res.status(401).json({ msg: "Permission Denied 3" });
  }
}

export function isAdmin(
  req: express.Request, 
  res: express.Response, 
  next: express.NextFunction
  ){
    

    if (req['user'].is_admin){
      return next()
    }
  return res.status(401).json({ msg: "You are not admin" });
}