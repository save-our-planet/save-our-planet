import * as Knex from "knex";
import path from "path";
import XLSX from "xlsx";
import {hashPassword} from "../hash";

interface ExcelRecycleBin {
    name: string;
    address: string;
    district: string;
    // location_type: string;
    office_hour: string;
    contact: string;
    type: string;
}

interface User {
    name: string;
    email: string;
    password: string;
    is_admin: boolean;
}

interface ExcelObjective {
    name: string;
    description: string;
    unit: string;
    category: string;
    required_star: number;
}
interface Objective {
    id: number;
    name: string;
    description: string;
    unit: string;
    category_id: number;
    required_star: number;
}
interface ExcelUserAchievement {
    objective: string;
    user: string;
}

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("recycle_tips_photo").del();
    await knex("recycle_tips").del();
    await knex("direct_message").del();
    await knex("saved_post").del();
    await knex("post_photo").del();
    await knex("saved_post").del();
    await knex("direct_message").del();
    await knex("post").del();
    await knex("shop").del();
    await knex("recycle_bin").del();
    await knex("district").del();
    await knex("location_type").del();
    await knex("user_achievement").del();
    await knex("user").del();
    await knex("eco_type").del();
    await knex("medalImg").del();
    await knex("objective").del();
    await knex("objective_category").del();

    const workbook = XLSX.readFile(path.join(__dirname, "seed.xlsx"));

    // Recycle Page
    const districtSheet = workbook.Sheets["district"];
    const districtJson = XLSX.utils.sheet_to_json(districtSheet);
    const districtReturningData: Array<{id: number; name: string}> = await knex("district")
        .insert(districtJson)
        .returning(["id", "name"]);
    const districtMapping = new Map();
    for (const row of districtReturningData) {
        districtMapping.set(row.name, row.id);
    }
    // console.log(districtMapping);

    const locationTypeSheet = workbook.Sheets["location_type"];
    const locationTypeJson = XLSX.utils.sheet_to_json(locationTypeSheet);
    const locationTypeReturningData: Array<{
        id: number;
        name: string;
    }> = await knex("location_type").insert(locationTypeJson).returning(["id", "name"]);
    const locationTypeMapping = new Map();
    for (const row of locationTypeReturningData) {
        locationTypeMapping.set(row.name, row.id);
    }
    // console.log(locationTypeMapping);

    // const recycleBinSheet = workbook.Sheets["recycle_bin"];
    // const recycleBinJson = XLSX.utils.sheet_to_json<ExcelRecycleBin>(recycleBinSheet);

    // const recycleBinData = [];
    // for (const recycleBin of recycleBinJson) {
    //     const {location_type, district, ...others} = recycleBin;
    //     recycleBinData.push({
    //         district_id: districtMapping.get(district),
    //         location_type_id: locationTypeMapping.get(location_type),
    //         ...others,
    //     });
    // }
    // // console.log(recycleBinData);

    // await knex("recycle_bin").insert(recycleBinData);

    const recycleBinSheet = workbook.Sheets["recycle_bin_2"];
    const recycleBinJson = XLSX.utils.sheet_to_json<ExcelRecycleBin>(recycleBinSheet);

    const recycleBinData = [];
    for (const recycleBin of recycleBinJson) {
        const {district, ...others} = recycleBin;
        recycleBinData.push({
            district_id: districtMapping.get(district),
            ...others,
        });
    }
    // console.log(recycleBinData);

    await knex("recycle_bin").insert(recycleBinData);



    // User
    const userSheet = workbook.Sheets["user"];
    let userJson = XLSX.utils.sheet_to_json<User>(userSheet);
    // console.log('UserJSON: ', userJson);
    let userJsonHash = [];

    for (const user of userJson) {
        let hashedPassword = await hashPassword(user.password.toString());
        userJsonHash.push({...user, password: hashedPassword});
    }
    // console.log('HASH: ',userJsonHash);

    const userReturningData: Array<{
        id: number;
        name: string;
        email: string;
        password: string;
        is_admin: boolean;
    }> = await knex("user")
        .insert(userJsonHash)
        .returning(["id", "name", "email", "password", "is_admin"]);
    // console.log("ReturnData: ", userReturningData);

    const userMapping = new Map();
    for (const row of userReturningData) {
        userMapping.set(row.name, row.id);
    }
    console.log("userMapping: ", userMapping);

    // Shop
    const ecoType: Array<{
        id: number;
        name: string;
    }> = await knex("eco_type")
        .insert([{name: "裸買"}, {name: "自攜容器"}, {name: "租用餐盒"}])
        .returning("*");

    console.log("Eco_type: ", ecoType);

    const shop: Array<{
        id: number;
        name: string;
        address: string;
    }> = await knex("shop")
        .insert([
            {
                name: "test_shop_1",
                address: "test_address_1",
                district_id: districtReturningData[0].id,
            },
        ])
        .returning("*");

    console.log("Shops: ", shop);

    // Medal
    const objectiveCategorySheet = workbook.Sheets["objective_category"];
    const objectiveCategoryJson = XLSX.utils.sheet_to_json(objectiveCategorySheet);
    const objectiveCategoryReturningData: Array<{
        id: number;
        name: string;
    }> = await knex("objective_category").insert(objectiveCategoryJson).returning(["id", "name"]);
    const objectiveCategoryMapping = new Map();
    for (const row of objectiveCategoryReturningData) {
        objectiveCategoryMapping.set(row.name, row.id);
    }
    // console.log(objectiveCategoryMapping);

    const objectiveSheet = workbook.Sheets["objective"];
    const objectiveJson = XLSX.utils.sheet_to_json<ExcelObjective>(objectiveSheet);
    const objectiveData = [];
    for (const objective of objectiveJson) {
        const {category, ...others} = objective;
        objectiveData.push({
            category_id: objectiveCategoryMapping.get(category),
            ...others,
        });
    }
    // console.log("objectiveData: ",objectiveData);
    const objectiveReturningData: Array<Objective> = await knex("objective")
        .insert(objectiveData)
        .returning(["id", "name"]);

    const objectiveMapping = new Map();
    for (const row of objectiveReturningData) {
        objectiveMapping.set(row.name, row.id);
    }
    console.log(objectiveMapping);

    const userAchievementSheet = workbook.Sheets["user_achievement"];
    const userAchievementJson = XLSX.utils.sheet_to_json<ExcelUserAchievement>(
        userAchievementSheet
    );
    const userAchievementData = [];
    for (const userAchievement of userAchievementJson) {
        const {objective, user, ...others} = userAchievement;
        console.log(userAchievement);

        userAchievementData.push({
            user_id: userMapping.get(user),
            objective_id: objectiveMapping.get(objective),
            ...others,
        });
    }
    console.log("userAchievementData: ", userAchievementData);

    let twID = await knex("district").select("id").where("name", "荃灣區");

    console.log("TW id: ", twID);

    let shopID = await knex("shop")
        .insert({
            name: "上海美味一品香菜館",
            address: "荃灣享和街43-59號都城大樓地鋪",
            district_id: twID[0].id,
        })
        .returning("id");

    let postID = await knex("post")
        .insert({
            title: "自攜餐盒食上海好東西",
            content:
                "今日去荃灣上training，同行朋友推薦附近一間好食的上海菜，間鋪頭係荃灣大會堂後面，近車房個邊，好彩有朋友帶路，如果唔係應該會蕩失路。鋪頭入面既環境比較古舊，好有家庭式小店feel，午餐都有幾多選擇，客飯麵食都有，仲會送豆漿，埋單都係$45一位，好抵食！等野食期間見到有人買外賣，鋪頭員工都會主動問佢地有冇自攜餐盒，店家都好歡迎客人街坊自備容器買野食，下次再黎荃灣可以再encore呢間鋪頭！",
            shop_id: shopID[0],
            user_id: userReturningData[1].id,
            eco_type_id: ecoType[1].id,
        })
        .returning("id");

    await knex("post_photo")
        .insert([{
            path: "uploads-1613290478143.jpeg",
            is_cover: true,
            post_id: postID[0],
        },{
            path: "uploads-1613290478148.jpeg",
            is_cover: false,
            post_id: postID[0],
        }]);

    await knex("user_achievement").insert(userAchievementData);
    const medalImgSheet = workbook.Sheets["medalImg"];
    const medalImgJson = XLSX.utils.sheet_to_json<ExcelUserAchievement>(medalImgSheet);
    const medalImgData = [];
    for (const medalImg of medalImgJson) {
        const {objective, ...others} = medalImg;
        console.log(medalImg);

        medalImgData.push({
            objective_id: objectiveMapping.get(objective),
            ...others,
        });
    }
    console.log("medalImgData: ", medalImgData);
    await knex("medalImg").insert(medalImgData);

    const recycleTipsWB = XLSX.readFile(path.join(__dirname, "recycle_tips.xlsx"));
    const recycleTipsSheet = recycleTipsWB.Sheets["recycle_tips"];
    const recycleTipsJson = XLSX.utils.sheet_to_json(recycleTipsSheet);
    const recycleTipsReturningData = await knex("recycle_tips").insert(recycleTipsJson).returning("*");
    console.log(recycleTipsReturningData);

    await knex("recycle_tips_photo").insert([
        {
            path: '1A_plastic.png',
            recycle_tips_id: recycleTipsReturningData[0].id
        },
        {
            path: '1B_plastic.png',
            recycle_tips_id: recycleTipsReturningData[0].id
        },
        {
            path: '1C_plastic.png',
            recycle_tips_id: recycleTipsReturningData[0].id
        },
        {
            path: '2A_plastic.png',
            recycle_tips_id: recycleTipsReturningData[1].id
        },
        {
            path: '2B_plastic.png',
            recycle_tips_id: recycleTipsReturningData[1].id
        },
        {
            path: '3A_plastic.png',
            recycle_tips_id: recycleTipsReturningData[2].id
        },
        {
            path: '3B_plastic.png',
            recycle_tips_id: recycleTipsReturningData[2].id
        },
        {
            path: '4A_plastic.png',
            recycle_tips_id: recycleTipsReturningData[3].id
        },
        {
            path: '4B_plastic.png',
            recycle_tips_id: recycleTipsReturningData[3].id
        },
        {
            path: '5A_plastic.png',
            recycle_tips_id: recycleTipsReturningData[4].id
        },
        {
            path: '6A_plastic.png',
            recycle_tips_id: recycleTipsReturningData[5].id
        },
        {
            path: '6B_plastic.png',
            recycle_tips_id: recycleTipsReturningData[5].id
        },
        {
            path: '7A_plastic.png',
            recycle_tips_id: recycleTipsReturningData[6].id
        },
        {
            path: '7B_plastic.png',
            recycle_tips_id: recycleTipsReturningData[6].id
        },
        {
            path: 'drinks_1.png',
            recycle_tips_id: recycleTipsReturningData[7].id
        },
        {
            path: 'drinks_recycle.png',
            recycle_tips_id: recycleTipsReturningData[7].id
        },
    ])
    
}
