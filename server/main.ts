import express from 'express';
import expressSession from 'express-session';
import dotenv from 'dotenv';
import Knex from 'knex';
import http from 'http';
import { Server as SocketIO } from 'socket.io';
import { PostService } from './services/postService';
import { PostController } from './controllers/postController';
import { RecycleBinService } from './services/RecycleBinService';
import { RecycleBinController } from './controllers/RecycleBinController';
import { UserService } from './services/UserService';
import { UserController } from './controllers/UserController';
import { ShopService } from './services/ShopService';
import { ShopController } from './controllers/ShopController';
import { MedalService } from './services/MedalService';
import { MedalController } from './controllers/MedalController';
import { DMService } from './services/DMService'
import { DMController } from './controllers/DMControllers';
import { TipsService } from './services/TipsService';
import { TipsController } from './controllers/TipsController';

dotenv.config();
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV|| "development"]);

const app = express();
const server = new http.Server(app);
const io = new SocketIO(server);

// io.on('connection', function(socket){
//     console.log("Socket connected at ", socket.id);

//     socket.on('sayhi', (msg: any)=>{
//         console.log(msg);
//     })
// })

app.use(express.urlencoded({extended:true}));
app.use(express.json());
app.use(
    expressSession({
        secret: 'sop sop sop sop sop sop sop sop',
        resave: true,
        saveUninitialized: true,
    })
);
    
const postService = new PostService(knex);
const recycleBinService = new RecycleBinService(knex);
export const userService = new UserService(knex);
export const shopService = new ShopService(knex);
const medalService = new MedalService(knex);
const dmService = new DMService(knex);
const tipsService = new TipsService(knex);
export const postController = new PostController(postService);
export const recycleBinController = new RecycleBinController(recycleBinService);
export const userController = new UserController(userService);
export const shopController = new ShopController(shopService);
export const medalController = new MedalController(medalService)
export const dmController = new DMController(dmService, io);
export const tipsController = new TipsController(tipsService);

import { routes } from './routes';

app.use(express.static('uploads'))
app.use(express.static('devPhotos'))
app.use('/', routes)
app.get('/', (req, res) => res.send("Save our planet."));

let PORT = 8080;
server.listen(PORT, ()=>{
    console.log('Listening on http://localhost:' + PORT);
})