import fs from "fs";
import puppeteer from "puppeteer";

(async () => {
    const browser = await puppeteer.launch({headless: false});
    try {
        // open the headless browser
        // open a new page
        const page = await browser.newPage();
        // enter url in page
        // await page.goto(`https://www.wastereduction.gov.hk/tc/quickaccess/vicinity.htm`);
        await page.goto(`https://www.wastereduction.gov.hk/tc/quickaccess/vicinity.htm?collection_type=outlet&material_type=col_plastics&district_id=0`);
        // await page.goto(`https://news.ycombinator.com/`);
        await page.waitForSelector("table.green_table");
        // await page.waitForSelector("a.storylink");

        const news = await page.evaluate(() => {
            const titleNodeList: any = document.querySelector(`table.green_table`);
            const wording: any = titleNodeList.innerText.trim();
            const split = wording.split(/[\n\t]+/);

            // const dataArray = [];
            // for (let i = 0; i < split.length; i++) {
            //     dataArray[i] = {
            //         收集點類別: "",
            //         "機構/位置": split[i],
            //         地址: split[i],
            //         地區: split[i],
            //         // link: titleNodeList[i].getAttribute("href"),
            //         // age: ageList[i].innerText.trim(),
            //         // score: scoreList[i].innerText.trim(),
            //     };
            // }
            // return dataArray;
            return split;
        });
        console.log(news);
        await browser.close();
        // Writing the news inside a json file
        fs.writeFile("recycle_bin_test.json", JSON.stringify(news), function (err) {
            if (err) throw err;
            console.log("Saved!");
        });
        console.log("done");
    } catch (err) {
        // Catch and display errors
        await browser.close();
        console.error(err);
    }
})();
