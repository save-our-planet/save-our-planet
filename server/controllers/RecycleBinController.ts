import { Request, Response } from "express";
import type { RecycleBinService } from "../services/RecycleBinService";

export class RecycleBinController {
    constructor(private recycleBinService: RecycleBinService) {}

    public loadRecycleBin = async (req: Request, res: Response) => {
        try {
            const result = await this.recycleBinService.getRecycleBin();
            res.json({ isSuccess: true, data: result });
        } catch (e) {
            console.error(e);
            res.json({ isSuccess: false, msg: e.toString() });
        }
    };

    public searchRecycleBin = async (req: Request, res: Response) => {
        try {
            // console.log(req.params.district);

            const result = await this.recycleBinService.searchRecycleBin(
                // req.body.location_type,
                req.params.districtID
            );
            // console.log(result);
            // console.log(result.length);
            // if (result.length !== 0) {
                // console.log(result);

                res.json({ isSuccess: true, data: result });
            // } else {
            //     // console.log('no search');
            //     res.json({
            //         isSuccess: false,
            //         data: [
            //             {
            //                 name: "所選擇地區沒有回收資訊",
            //                 address: "",
            //                 district: "",
            //                 location_type: "",
            //             },
            //         ],
            //     });
            // }
        } catch (e) {
            console.error(e);
            res.json({ isSuccess: false, msg: e.toString() });
        }
    };
    public getDistrict = async (req: Request, res: Response) => {
        try {
            const result = await this.recycleBinService.getDistrict();
            // console.log(result);

            res.json({ isSuccess: true, data: result });
        } catch (e) {
            console.error(e);
            res.json({ isSuccess: false, msg: e.toString() });
        }
    };
}
