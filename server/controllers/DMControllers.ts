import { Request, Response } from 'express';
import type { DMService } from '../services/DMService';
import moment from 'moment';
import { Server as SocketIO } from 'socket.io';


export class DMController {
    public constructor(private dmService: DMService,
                        private io: SocketIO){

    }

    public postDM = async (req: Request, res: Response) => {
        let directMessage;
        if (req.body.isAdmin == "true"){
            
            directMessage = await this.dmService.insertDM(
                parseInt(req['user'].id), 
                parseInt(req.body.userID), 
                parseInt(req['user'].id), 
                req.body.content,
                true
                )
        } else {
            
            directMessage = await this.dmService.insertDM(
                parseInt(req.body.adminID), 
                parseInt(req['user'].id), 
                parseInt(req['user'].id), 
                req.body.content,
                false
                )
        }
        directMessage.created_at = moment(directMessage.created_at).utcOffset('+0800').format('D MMM,YY HH:mm')
        this.io.emit(`direct-message`, directMessage)
        res.json(directMessage)
    }

    public loadDMs = async (req: Request, res: Response) => {
        let directMessages = await this.dmService.selectDMs(req['user'].id);
        
        for(let message of directMessages){
            message.created_at = moment(message.created_at).utcOffset('+0800').format('D MMM,YY HH:mm')
        }
        
        res.json(directMessages)
    }

    public loadAdminDMs = async (req: Request, res: Response) => {
        let directMessages = await this.dmService.selectAdminDMs(req['user'].id);

        for(let message of directMessages){
            message.created_at = moment(message.created_at).utcOffset('+0800').format('D MMM,YY HH:mm')
        }
        
        res.json(directMessages)
    }

    public loadAllUsers = async (req: Request, res: Response) => {
        let allUsers = await this.dmService.selectAllUsers()
        res.json(allUsers)


    }

}