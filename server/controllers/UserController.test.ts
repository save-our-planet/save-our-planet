import Knex from "knex";
import { UserService } from "../services/UserService"
import { UserController } from "./UserController";

describe("Testing user controller", ()=>{

    let req: {
        body?: {
            username?: string,
            password?: string,
            email?: string,
        },
        user?: {
            id?: number | string,
            name?: string,
            email?: string,
            password?: string,
            is_admin?: boolean,
        } 
    }

    let res: {
        json: ()=>{},
        status?: (number: number)=>{
            json: ()=>{}
        }
    }
    let service: any;
    let controller: any;
    let resJson: any;

    beforeEach(function(){
        service = new UserService({} as Knex);
        jest.spyOn(service, 'getUser').mockImplementation((username) => Promise.resolve([{
            id: 1,
            name: 'Alex',
            email: 'Alex@tecky.io',
            password: '123456',
            is_admin: false,
        }]));
        jest.spyOn(service, 'getUserByEmail').mockImplementation((email) => Promise.resolve([{
            id: 1,
            name: 'Alex',
            email: 'Alex@tecky.io',
            password: '123456',
            is_admin: false,
        }]));
        jest.spyOn(service, 'createUser').mockImplementation((name, email, password) => Promise.resolve([{
            id:1,
            name: 'Alex',
            email: 'Alex@tecky.io',
            password: '123456',
            is_admin: false,
        }]))        
        jest.spyOn(service,'changePassword').mockImplementation((id,password) => Promise.resolve([{
            id:'1',
            name: 'Alex',
            email: 'Alex@tecky.io',
            password: '123456789',
            is_admin: false,
        }]))
        jest.spyOn(service, 'checkAdmin').mockImplementation((id) => Promise.resolve([{
            id:'1',
            ame: 'Alex',
            email: 'Alex@tecky.io',
            password: '123456',
            is_admin: false,
        }]))

        controller = new UserController(service);

        res = {
            json: ()=>{},
            status: ()=>{
                json: ()=>{}
            }
        } as any;

        resJson = jest.spyOn(res, 'json');
    })

    it('should check login', async ()=>{
        req = {
            user:{
                name: "alex"
            }
        }

        await controller.checkLogin(req, res);
        expect(resJson).toBeCalledTimes(1)
    })

    it('should catch error if no name for checklogin', async()=>{
        req = {
            user:{

            }
        }

        await controller.checkLogin(req, res);
        expect(resJson).toBeCalled();

    })

    // it('should call getUser', async()=>{
    //     req = {
    //         body:{
    //             username: "Alex",
    //             password: "1234"
    //         }
    //     }

    //     await controller.login(req, res);
    //     expect(service.getUser).toBeCalledTimes(1)

    // })


})