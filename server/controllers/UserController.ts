import {Request, Response} from "express";
import {UserService} from "../services/UserService";
import jwtSimple from "jwt-simple";
import {checkPassword} from "../hash";
// import fetch from 'node-fetch';

export class UserController {
    constructor(private userService: UserService) {}

    public checkLogin = async (req: Request, res: Response) => {
        try{
            res.json(req['user'].name)
        }catch (e){
            throw(e)
        }
    };

    public login = async (req: Request, res: Response) => {
        try {
            if (!req.body.username || !req.body.password) {
                res.status(401).json({msg: "Wrong Username/Password"});
                return;
            }
            const {username, password} = req.body;

            // console.log("UserController.ts ReqBody: ", req.body);
            // console.log("UserController.ts user: ", await this.userService.getUser(username))

            const user = (await this.userService.getUser(username))[0];

            if (!user || !(await checkPassword(password, user.password))) {
                // if(!user || await checkPassword(password,user.password) === false){
                res.status(401).json({msg: "Wrong Username/Password"});
                return;
            }
            const payload = {
                id: user.id,
                username: user.name,
            };

            const token = jwtSimple.encode(payload, process.env.JWT_SECRET!);
            // req["user"] = user[0];
            console.log(user);

            console.log("Login", req["user"]);

            res.json({
                token: token,
            });

            // console.log("UserController.ts res.json", res.json);
        } catch (e) {
            console.log(e);
            res.status(500).json({msg: e.toString()});
        }
    };

    public checkUsername= async(req: Request, res: Response) => {
        try{
            let {username} = req.body;
            // username = username.toLowerCase();
            // console.log("userController username", username)
            const existingUsername = await this.userService.getUser(username);
            if (existingUsername[0]) {
                console.log("UserController existed username");
                res.status(401).json({msg: "Username already existed", status: 401});
                return;
            }
            res.json({result: "running"})
        }catch(e){
            console.log(e);
            res.status(500).json({msg: e.toString()});
        }
    }

    public checkEmail = async(req: Request, res: Response) => {
        try{
            let {email} = req.body;
            email = email.toLowerCase();
            console.log("userController email", email)
            const existingEmail = await this.userService.getUserByEmail(email);
            if (existingEmail[0]) {
                console.log("UserController existed email");
                res.status(401).json({msg: "Email already existed", status: 401});
                return;
            }
            res.json({result: "running"})
        }catch(e){
            console.log(e);
            res.status(500).json({msg: e.toString()});
        }
    }

    public register = async (req: Request, res: Response) => {
        try {
            let {username, password, email} = req.body;
            // username = username.toLowerCase();
            email = email.toLowerCase();

            // const existingUsername = await this.userService.getUser(username);
            // if (existingUsername[0]) {
            //     console.log("UserController existed username");
            //     res.status(401).json({msg: "Username already existed", status: 401});
            //     return;
            // }
            // const existingEmail = await this.userService.getUserByEmail(email);
            // if (existingEmail[0]) {
            //     console.log("UserController existed email");
            //     res.status(401).json({msg: "Email already existed", status: 401});
            //     return;
            // }
            console.log("UserController - req.body: ", req.body);

            let user = await this.userService.createUser(username, email, password);

            console.log("UserController user: ", user);
            res.json({msg: "Successfully Register"});
        } catch (e) {
            console.log(e);
            res.status(500).json({msg: e.toString()});
        }
    };

    public newPassword = async (req: Request, res: Response) => {
        try {
            let id = req['user'].id
            // let id = req['user'][0].id
            console.log("-----------------newPassword-------------------");
            
            let {newPassword} = req.body;
            let result = await this.userService.changePassword(id, newPassword);
            // console.log("user controller result: ", result[0]);
        
            console.log("-----------------newPassword-------------------");

            if (result[0]){
                res.json({result: 'success'});
            } else{
                res.json({result: 'fail'})
            }

        } catch (e) {
            console.log(e);
            res.status(500).json({msg: e.toString()});
        }
    };

    public checkPassword = async (req: Request, res: Response) => {
        try {
            console.log("******************checkPassword**********************");
            const {currentPassword} = req.body;
            
            // let checking = await checkPassword(currentPassword, req['user'][0].password)
            let checking = await checkPassword(currentPassword, req['user'].password)

            console.log("current pw checking (boolean): ", checking);
            
            if (!checking) {
                res.status(401).json({msg: "Current Password Invalid Input", status:401});
                return;
            }
            
            console.log("******************checkPassword**********************");
            
            res.json({permission: true})

        } catch (e) {
            console.log(e);
            res.status(500).json({msg: e.toString()});
        }
    };

    public checkAdmin = async (req: Request, res: Response) => {
        let isAdmin = await this.userService.checkAdmin(req["user"].id);
        res.json(isAdmin);
    };

    // loginFacebook = async (req:Request,res:Response)=>{
    //     try{
    //         if(!req.body.accessToken){
    //             res.status(401).json({msg:"Wrong Access Token!"});
    //             return;
    //         }
    //         const {accessToken} = req.body;
    //         const fetchResponse =await fetch(`https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`);
    //         const result = await fetchResponse.json();
    //         if(result.error){
    //             res.status(401).json({msg:"Wrong Access Token!"});
    //             return ;
    //         }
    //         let user = (await this.userService.getUser(result.email))[0];

    //         // Create a new user if the user does not exist
    //         if (!user) {
    //             user = (await this.userService.createUser(result.email))[0];
    //         }
    //         const payload = {
    //             id: user.id,
    //             username: user.username
    //         };
    //         const token = jwtSimple.encode(payload, jwt.jwtSecret);
    //         res.json({
    //             token: token
    //         });
    //     }catch(e){
    //         res.status(500).json({msg:e.toString()})
    //     }
    // }
}
