import { Request, Response } from 'express';
import type { TipsService } from '../services/TipsService';

export class TipsController {
    public constructor(private tipsService: TipsService){

    }

    public loadTips = async (req: Request, res: Response) => {
        let tips = await this.tipsService.selectTips();
        res.json(tips)
    }

    public loadPhotos = async (req: Request, res: Response) => {
        let photos = await this.tipsService.selectPhotos(parseInt(req.params.tipsID));
        res.json(photos)
    }

}