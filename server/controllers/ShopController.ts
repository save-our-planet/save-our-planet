import {Request,Response} from 'express';
import { ShopService } from '../services/ShopService';
export class ShopController {
    constructor(private shopService: ShopService) {}

    addShop = async (req: Request, res: Response) => {
        try{
            let { name, address, districtID } = req.body;
            let shop = await this.shopService.addShop(
                name,
                address,
                districtID
            )

            console.log("shop controller - shop", shop)
            res.json({msg: "Successfully added shop details", data: shop})

        }catch(e){
            console.log(e)
            res.json({ msg: e.toString()})
        }
    }

    getShop = async (req: Request, res: Response) => {
        try {
            const result = await this.shopService.getShop();
            res.json({result})
        } catch (e) {
            console.log(e)
            res.json({msg: e.toString()})
        }


    }
}




