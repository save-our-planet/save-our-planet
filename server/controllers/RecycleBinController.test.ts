import {RecycleBinController} from "./RecycleBinController";
import {RecycleBinService} from "../services/RecycleBinService";
import {Response} from "express";
import Knex from "knex";
jest.mock("express");

describe("RecycleBinController", () => {
    let controller: RecycleBinController;
    let service: RecycleBinService;
    let resJson: jest.SpyInstance;
    let req = {
        body: {},
        params: {},
    } as any;
    let res: Response;

    beforeEach(function () {
        service = new RecycleBinService({} as Knex);
        jest.spyOn(service, "getRecycleBin").mockImplementation(() =>
            Promise.resolve([
                {
                    id: 1,
                    name: "lau kong wa",
                    address: "rubbish bin",
                    district: "his ass",
                    location_type: "unknown",
                },
                {
                    id: 2,
                    name: "locker",
                    address: "everywhere",
                    district: "trial&error",
                    location_type: "unknown",
                },
            ])
        );
        // jest.spyOn(service, "searchRecycleBin").mockImplementation((body) => {
        //     Promise.resolve([
        //         {
        //             id: 1,
        //             name: "lau kong wa",
        //             address: "rubbish bin",
        //             district_name: "his ass",
        //             location_type_name: "unknown",
        //         },
        //     ]);
        // });
        controller = new RecycleBinController(service);
        // req = ({
        //     body: {},
        //     params:{}
        // } as any) as Request;
        res = ({json: () => {}} as any) as Response;
        resJson = jest.spyOn(res, "json");
    });

    it("should handle get method correctly", async () => {
        await controller.loadRecycleBin(req, res);
        expect(service.getRecycleBin).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({
            data: [
                {
                    address: "rubbish bin",
                    district: "his ass",
                    id: 1,
                    location_type: "unknown",
                    name: "lau kong wa",
                },
                {
                    address: "everywhere",
                    district: "trial&error",
                    id: 2,
                    location_type: "unknown",
                    name: "locker",
                },
            ],
            isSuccess: true,
        });
    });
    it("should search by district and location type", async () => {
        req = {
            params: {
                // location_type: "unknown",
                districtID: "1",
            },
        };
        const searchRecycleBin: any = jest.spyOn(service, "searchRecycleBin");
        searchRecycleBin.mockResolvedValue({
            id: 1,
            address: "rubbish bin",
            district_id: 1,
            district: "his ass",
            location_type_id: 1,
            location_type: "unknown",
            name: "lau kong wa",
        });
        await controller.searchRecycleBin(req, res);

        expect(searchRecycleBin).toHaveBeenCalled();
        expect(resJson).toBeCalledWith({
            data: {
                id: 1,
                address: "rubbish bin",
                district_id: 1,
                district: "his ass",
                location_type_id: 1,
                location_type: "unknown",
                name: "lau kong wa",
            },

            isSuccess: true,
        });
    });
});
