import {Request, Response} from "express";
import {MedalService} from "../services/MedalService";

export class MedalController {
    constructor(private medalController: MedalService) {}

    public loadAllObjectives = async (req: Request, res: Response) => {
        try {
            let id = req['user'].id

            const result = await this.medalController.getObjective(parseInt(id));

            res.json({isSuccess: true, data: result});
        } catch (e) {
            console.error(e);
            res.json({isSuccess: false, msg: e.toString()});
        }
    };
    public addLevel = async (req: Request, res: Response) => {
        try {
            let id = parseInt(req.params.id);

            const result = await this.medalController.updateLevel(id, req.body.level);
            res.json({isSuccess: true, data: result});
        } catch (e) {
            console.error(e);
            res.json({isSuccess: false, msg: e.toString()});
        }
    };
    public loadLevel = async (req: Request, res: Response) => {
        try {
            let id = parseInt(req.params.id);
            // console.log(id);
            const result = await this.medalController.getLevel(id);
            // console.log("load level: ", result);

            res.json({isSuccess: true, data: result});
        } catch (e) {
            console.error(e);
            res.json({isSuccess: false, msg: e.toString()});
        }
    };
    public firstLogin= async (req: Request, res: Response) => {
        try {
            
            let id = req['user'].id
            const result = await this.medalController.getFirstLogin(id);
            // console.log("firstTimeLogin check: ", result);
            
            res.json({isSuccess: true, data: result});
        } catch (e) {
            console.error(e);
            res.json({isSuccess: false, msg: e.toString()});
        }
    }
}
