import { Request, Response } from 'express';
import type { PostService } from '../services/postService';
import moment from 'moment';

export class PostController {
    public constructor(private postService: PostService){

    }

    public createPost = async (req: Request, res: Response) => {
        let postID = await this.postService.insertPost(req.body.title, req.body.content, req['user'].id, req.body.shopID, req.body.ecoTypeID);
        res.json({result: postID});
    }

    public loadPosts = async (req: Request, res: Response) => {
        let posts = await this.postService.selectPosts()
        for(let post of posts){
            post.created_at = moment(post.created_at).utcOffset('+0800').format('D MMM,YY HH:mm')
            
        }
        
        res.json(posts);
    }

    public editPost = async (req: Request, res: Response) => {
        let post = await this.postService.updatePost(req.params.postID, req.body.title, req.body.content);
        res.json(post)
    }

    public deletePost = async (req: Request, res: Response) => {
        let isDeleted = await this.postService.deletePost(req.params.postID);
        res.json({result: isDeleted})
    }

    public insertPostPhotos = async (req: Request, res: Response) => {
        let areUploaded = await this.postService.insertPhotos(req.files, req.params.postID);
        res.json({result: areUploaded})
    }

    public loadPostPhotos = async (req: Request, res: Response) => {
        let photos = await this.postService.selectPhotos(req.params.postID);
        res.json(photos)
    }

    public savePost = async (req: Request, res: Response) => {
        let isSaved = await this.postService.saveOrUnsavePost(parseInt(req.params.postID), req['user'].id);
        res.json({result: isSaved})
    }

    public getSavedStatus = async (req: Request, res: Response) => {
        let isSaved = await this.postService.checkSavedStatus(parseInt(req.params.postID), req['user'].id);
        res.json({result: isSaved})
    }

    public loadEcoTypes = async (req: Request, res: Response) => {
        let ecoTypes = await this.postService.selectEcoTypes()
        res.json(ecoTypes)
    }

    public loadNewestPost = async (req: Request, res: Response) => {
        let newestPost = await this.postService.selectNewestPost()
        res.json(newestPost)
    }

    public loadSavedPosts = async (req: Request, res: Response) => {
        let savedPosts = await this.postService.selectSavedPosts(req['user'].id);
        for(let post of savedPosts){
            post.created_at = moment(post.created_at).utcOffset('+0800').format('D MMM,YY HH:mm')
        }

        res.json(savedPosts)
    }

    public putBackPost = async (req: Request, res: Response) => {
        let putBackPost = await this.postService.putBackPost(req.params.postID);
        res.json(putBackPost)
    }

}