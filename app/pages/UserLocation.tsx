import React, {useEffect, useState} from "react";
import {Dimensions, Platform, SafeAreaView, StatusBar, StyleSheet, Text, View} from "react-native";
import MapView, {PROVIDER_GOOGLE, Region} from "react-native-maps";
import Geolocation from "react-native-geolocation-service";

// export interface IUserLocation {
//     latitude: number;
//     longitude: number;
// }

export function UserCurrentLocation() {
    const [region, setRegion] = useState<Region>({
        latitude: 22.302711,
        longitude: 114.177216,
        latitudeDelta: 0.0018,
        longitudeDelta: 0.0018,
    });
    
    const [hasLocationPermission, setHasLocationPermission] = useState<boolean>(false)

    async function requestPermissions() {
        if (Platform.OS === 'ios') {
            const auth = await Geolocation.requestAuthorization("whenInUse");
            console.log("userLocation - Platform: is ios")
            console.log(auth);
            
            if(auth === "granted") {
                console.log("userLocation: auth is granted")
                setHasLocationPermission(true)
            }
          }
    }
    requestPermissions()

    

        useEffect(() => {
            if (hasLocationPermission) {
              Geolocation.getCurrentPosition(
                  (position) => {
                    console.log("position");
                    console.log(position);

                    let long= position.coords.longitude
                      let lat= position.coords.latitude
                      let latDelta = 0.0018
                      let longDelta = 0.0018

                      console.log('long',long)
                      console.log('lat',lat)

                      setRegion({
                          latitude: position.coords.latitude,
                          longitude: position.coords.longitude,
                          latitudeDelta: 0.0018,
            longitudeDelta: 0.0018,
                      })
                  },
                  (error) => {
                    // See error code charts below.
                    console.log(error.code, error.message);
                  },
                  { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
              );
            }
          },[hasLocationPermission])

    return (
        <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView >
                <View style={styles.container}>

                    <MapView
                        style={styles.map}
                        provider={PROVIDER_GOOGLE}
                        showsUserLocation
                        region={region}
                        >

                    </MapView>
                </View>
            </SafeAreaView>
        </>
    );
}

// 22.37251440792494, 114.10763204694608

const styles = StyleSheet.create({
    container: {
        height: Dimensions.get("window").height,
        width: Dimensions.get("window").width,
    },
    map:{
        ...StyleSheet.absoluteFillObject,
    }
});
