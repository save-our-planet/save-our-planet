import React, { useEffect, useState } from "react";
import { FlatList, SafeAreaView, Text, useWindowDimensions, View } from "react-native";
import { useDispatch } from "react-redux";
import { REACT_APP_BACKEND_URL } from "../env";
import { beginLoading, doneLoading } from "../redux/nav/action";
import { ITip, TipContainer } from "./components/tip";

export function Tips(){
    const dimensions = useWindowDimensions();
    const [tips, setTips] = useState([]);
    const dispatch = useDispatch();

    async function loadTips(){
        let res = await fetch(`${REACT_APP_BACKEND_URL}/tips`)
        let result = await res.json();
        setTips(result)

    }

    useEffect(()=>{
        dispatch(beginLoading())
        loadTips().then(() => dispatch(doneLoading()))
    }, [])

    return (
        // <SafeAreaView style={{
        //     width: dimensions.width,
        //     flex: 1, 
        //     // justifyContent: 'center',
        //     alignItems: 'center',
        //     backgroundColor: '#DCEBD8',}}>
            
            <FlatList
            style={{
                width: dimensions.width,
                flex: 1,
                backgroundColor: '#DCEBD8'
            }}
            ListHeaderComponentStyle={{
                marginTop: 60
            }}
            ListHeaderComponent={
                <Text
                    style={{
                        marginVertical: 30,
                        fontSize: 30,
                        fontWeight: "bold",
                        textAlign: "center",

                    }}>
                    回收小知識
                </Text>
            }
            contentContainerStyle={{
                justifyContent:'center',
                alignItems: 'center'
            }}
            data={tips}
            keyExtractor={(item, index) => index.toString()}
            renderItem={tip => (
                <TipContainer tipItem={tip.item}/>
            )}
            
            />
        // </SafeAreaView>
    )
}