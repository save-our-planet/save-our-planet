import {useNavigation, useRoute} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import React, {useEffect, useState} from "react";
import { ScrollView, StyleSheet, Text, TouchableOpacity, useWindowDimensions, View} from "react-native";
import {SafeAreaView} from "react-native-safe-area-context";
import {useDispatch, useSelector, useStore} from "react-redux";
import MainTrophy from "../dev/photos/svg/trophy.svg";
import {Medal} from "./components/Medal";
import {MedalGoal} from "./MedalGoal";
import { REACT_APP_BACKEND_URL } from "../env";
import { beginLoading, doneLoading } from "../redux/nav/action";
import BulbSVG from "../dev/photos/svg/suggestion.svg";
import Icon from "react-native-ionicons";
import Modal from "react-native-modal";
import {Button} from "react-native-paper";
import { RootState } from "../store";
import { Login } from "./Login";
import { SignUp } from "./Signup";

// export interface IMedal {
//     id: number,
//     category: string,
//     description: string,
//     level: number,
//     created_at: string,
//     SVG?: string
// }
// let medals: IMedal[]=[
//     {
//         id: 1,
//         category: "登入",
//         description: "登入獎勵",
//         level: 4,
//         created_at: '2021-01-24T09:11:00.000Z',
//     },
//     {
//         id: 2,
//         category: "走塑",
//         description: "自攜杯",
//         level: 11,
//         created_at: '2021-01-24T09:11:00.000Z'
//     },
//     {
//         id: 3,
//         category: "走塑",
//         description: "自攜餐具",
//         level: 2,
//         created_at: '2021-01-24T09:11:00.000Z'
//     },
//     {
//         id: 4,
//         category: "減碳",
//         description: "每晚關燈一小時",
//         level: 0,
//         created_at: '2021-01-24T09:11:00.000Z'
//     },

// ]

function Trophy() {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const dimensions = useWindowDimensions();
    const state = useStore().getState();
    const [isRemindModalVisible, setRemindModalVisible] = useState(false);
    
    const [medals, setMedals] = useState<
        {
            id: number;
            name: string;
            description: string;
            unit: string;
            category_id: number;
            required_star: number;
            objective_category: string;
            user_id: number;
            level: number;
            ImgName: string;
        }[]
    >([]);

    async function loadObjective() {
        const res = await fetch(`${REACT_APP_BACKEND_URL}/objective`, {
            headers: {
                Authorization: "Bearer " + state.auth.token,
            },
        });
        const results = await res.json();
        setMedals(results.data);
    }

    // useEffect(() => {
    //     loadObjective();
    // }, []);
    useEffect(() => {
        const updateObject = navigation.addListener("focus", () => {
            // The screen is focused
            // Call any action
            dispatch(beginLoading())
            loadObjective().then(()=> dispatch(doneLoading()));
        });

        // Return the function to unsubscribe from the event so it gets removed on unmount
        return updateObject;
    }, [navigation]);
    return (
        <>
            {/* <SafeAreaView> */}
            <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
                <View
                    style={{
                        // justifyContent: "center",
                        alignItems: "center",
                    }}>
                        <View style={{
                            position: 'absolute',
                            width: dimensions.width,
                            alignItems: 'flex-end',
                            paddingTop: 20,
                            padding: 10,
                        }}>

                            <TouchableOpacity
                            style={{margin: 10}}
                            onPress={() => {
                                setRemindModalVisible(!isRemindModalVisible);
                            }}>
                                <BulbSVG 
                                // style={styles.remind} 
                                width={50} height={50} fill="#000" />
                            </TouchableOpacity>
                            {/* </View> */}
                        </View>
                    <View style={styles.container}>
                        <MainTrophy width={100} height={100} fill="#000" />
                    </View>
                    <View style={styles.goalContainer}>
                        {medals.map((medal, i) => (
                            <Medal key={i} medal={medal} />
                        ))}
                    </View>
                </View>
                <Modal
                        testID={"modal"}
                        isVisible={isRemindModalVisible}
                        backdropColor="#aaa"
                        backdropOpacity={0.8}
                        animationIn="zoomIn"
                        animationOut="zoomOut"
                        animationInTiming={450}
                        animationOutTiming={450}
                        backdropTransitionInTiming={600}
                        backdropTransitionOutTiming={800}>
                        <View style={styles.content}>
                            <Text style={styles.remindContentTitle}>
                            指引
                            </Text>
                            <View style={{
                                justifyContent: 'flex-start'
                            }}>
                                <View style={{
                                    flexDirection: 'row',
                                    paddingVertical: 10
                                }}>
                                    <Text style={[styles.text, {width: 20}]}>1. </Text>
                                    <Text style={[styles.text, {width: 300}]}>
                                        每個獎章代表不同任務，完成任務5次可獲銅牌、10次可獲銀牌，15次可獲金牌
                                    </Text>
                                </View>
                                <View style={{
                                    flexDirection: 'row',
                                    paddingVertical: 10
                                }}>
                                    <Text style={[styles.text, {width: 20}]}>2. </Text>
                                    <Text style={[styles.text, {width: 300}]}>
                                        請按下相關獎章圖案以了解更多任務內容
                                    </Text>
                                </View>
                                
                            </View>
                            <View style={styles.buttonGroup}>
                                <Button
                                    testID={"close-button"}
                                    onPress={() => {
                                        setRemindModalVisible(false)
                                    }}>
                                    <Icon name={"close-sharp"} size={30} color={"blue"} />
                                </Button>
                            </View>
                        </View>
                    </Modal>
            </ScrollView>
            {/* </SafeAreaView> */}
        </>
    );
}
const MedalStack = createStackNavigator();


export function TrophyNavigator() {

    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    
    return (
        <MedalStack.Navigator headerMode="none">
            {!isAuthenticated && <MedalStack.Screen name='Login' component={Login} options={{headerShown: false}}/>}
            {!isAuthenticated && <MedalStack.Screen name='Sign-up' component={SignUp} options={{headerShown: false}}/>}
            {isAuthenticated && <MedalStack.Screen name="Trophy" component={Trophy} />}
            {isAuthenticated && <MedalStack.Screen name="MedalGoal" component={MedalGoal} />}

        </MedalStack.Navigator>
    );
}
const styles = StyleSheet.create({
    container: {
        // flex: 1,
        paddingTop: 40,
        alignItems: "center",
    },
    goalContainer: {
        alignItems: "center",
        // padding: 40,
        justifyContent: "center",
        flexDirection: "row",
        flexWrap: "wrap",
    },
    scrollView: {
        height: "200%",
        backgroundColor: "#DCEBD8",
    },
    content: {
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 15,
        borderColor: "rgba(0, 0, 0, 0.1)",
    },
    remindContentTitle: {
        fontSize: 30,
        margin: 20,
    },
    buttonGroup: {
        alignItems: "flex-start",
        padding: 5,
        width: "100%",
        justifyContent: "space-around",
        flexDirection: "row",
        flexWrap: "wrap",
    },
    text: {
        fontSize: 16,
        lineHeight: 25
    }
});
