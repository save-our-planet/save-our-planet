import {useNavigation, useRoute} from "@react-navigation/native";
import React, {useEffect, useState} from "react";
import {
    Dimensions,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    useWindowDimensions,
    View,
} from "react-native";
import {SafeAreaView} from "react-native-safe-area-context";
// import MedalSVG from "../dev/photos/svg/medal.svg";
// import CopperMedalSVG from "../dev/photos/svg/medalcopper.svg";
// import SilverMedalSVG from "../dev/photos/svg/medalSilver.svg";
// import GoldMedalSVG from "../dev/photos/svg/medalGold.svg";
import BulbSVG from "../dev/photos/svg/suggestion.svg";

import {Button} from "react-native-paper";

import Icon from "react-native-ionicons";
import {SmallStar} from "./components/SmallStar";
import {BigStar} from "./components/BigStar";
import Modal from "react-native-modal";
import {MedalModal} from "./components/MedalModal";
import {REACT_APP_BACKEND_URL} from "../env";
import {useDispatch} from "react-redux";
import {beginLoading, doneLoading} from "../redux/nav/action";
import {TouchableOpacity} from "react-native-gesture-handler";
import BlankSmallStar from "../dev/photos/svg/blankSmallStar.svg";

// const smallStars: {id: number}[] = [{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}];

const bigStars: {id: number}[] = [{id: 1}, {id: 2}, {id: 3}];

const images: any = {
    Bottle: require("../dev/photos/medals/Bottle.png"),
    BringYourOwnCup: require("../dev/photos/medals/BringYourOwnCup.png"),
    BringYourOwnCutlery: require("../dev/photos/medals/BringYourOwnCutlery.png"),
    Electric: require("../dev/photos/medals/Electric.png"),
    First_login_award: require("../dev/photos/medals/First_login_award.png"),
    GoVegeterian: require("../dev/photos/medals/Go_vegeterian.png"),
    Grocery: require("../dev/photos/medals/Grocery.png"),
    LunchBox: require("../dev/photos/medals/LunchBox.png"),
    PlasticCutlery: require("../dev/photos/medals/PlasticCutlery.png"),
    Recycle: require("../dev/photos/medals/Recycle.png"),
    RecycleBag: require("../dev/photos/medals/RecycleBag.png"),
    ShareToFriend: require("../dev/photos/medals/ShareToFriend.png"),
    Straw: require("../dev/photos/medals/Straw.png"),
    Transport: require("../dev/photos/medals/Transport.png"),
    TurnOffLight1hour: require("../dev/photos/medals/TurnOffLight1hour.png"),
};
export function MedalGoal() {
    // const dispatch = useDispatch();
    const route = useRoute();
    const {
        objective_category,
        name,
        objective_id,
        required_star,
        unit,
        created_at,
        description,
        id,
        level,
        user_id,
        ImgName,
    }: any = route.params;
    let imgName = ImgName;
    const smallStars: {id: number}[] = [];
    for (let i = 1; i <= required_star / 3; i++) {
        smallStars.push({id: i});
    }

    const navigation = useNavigation();
    // function addLevel() {
    //     addLevelFetch(level);
    // }
    async function addLevelFetch(levels: number) {
        // console.log("level:", level);

        const res = await fetch(`${REACT_APP_BACKEND_URL}/level/${id}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({level: level + 1}),
        });
        res.json()
            .then((json) => {
                console.log("json level: ", json);

                try {
                    console.log("LoadlevelFetch", levels);
                    Promise.all([
                        navigation.setParams({
                            level: json.data[0].level,
                        }),
                    ]);
                } catch (e) {
                    console.log(e);
                }
            })
            .then(() => loadLevelFetch())
            .then(() => console.log("done"));
    }
    async function loadLevelFetch() {
        const res = await fetch(`${REACT_APP_BACKEND_URL}/level/${id}`);
        const json = await res.json();
        // console.log("jsonData", json.data[0].level);

        if (json) {
            // console.log("LoadlevelFetch", level);
            navigation.setParams({
                level: json.data[0].level,
            });
        }
    }
    // async function clearLevel() {
    //     //for dev purpose
    //     const res = await fetch(`${REACT_APP_BACKEND_URL}/level/${id}`, {
    //         method: "PUT",
    //         headers: {
    //             "Content-Type": "application/json",
    //         },
    //         body: JSON.stringify({level: 4}),
    //     });
    //     const json = await res.json();
    //     console.log("clear level reset to ", json);
    //     setAlreadyBronze(false);
    //     setAlreadySilver(false);
    //     setAlreadyGold(false);
    //     loadLevelFetch();
    // }
    useEffect(() => {
        // console.log("useEffect");

        loadLevelFetch();
    }, []);

    useEffect(() => {
        console.log("level useEffect: ", level);
        if (level % (required_star / 3) == 0) {
            setAlreadyBronze(false);
            setAlreadySilver(false);
            setAlreadyGold(false);
        }
    }, [level]);

    const [isModalVisible, setModalVisible] = useState(false);
    const [isCopperModalVisible, setCopperModalVisible] = useState(false);
    const [isSilverModalVisible, setSilverModalVisible] = useState(false);
    const [isGoldModalVisible, setGoldModalVisible] = useState(false);
    const [alreadyBronze, setAlreadyBronze] = useState(false);
    const [alreadySilver, setAlreadySilver] = useState(false);
    const [alreadyGold, setAlreadyGold] = useState(false);
    const [isRemindModalVisible, setRemindModalVisible] = useState(false);

    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };
    const toggleRemindModal = () => {
        setRemindModalVisible(!isRemindModalVisible);
    };
    const toggleCopperModal = () => {
        if (alreadyBronze !== true && level == required_star / 3) {
            // console.log("medalGoal: ", level);
            console.log(alreadyBronze);

            setAlreadyBronze(true);
            setCopperModalVisible(!isCopperModalVisible);
        }
    };
    const toggleCopperModalClose = () => {
        setCopperModalVisible(false);
    };
    const toggleSilverModal = () => {
        if (level == required_star * (2 / 3) && alreadySilver !== true) {
            setAlreadySilver(true);
            setSilverModalVisible(!isSilverModalVisible);
        }
    };
    const toggleSilverModalClose = () => {
        setSilverModalVisible(false);
    };
    const toggleGoldModal = () => {
        if (level == required_star && alreadyGold !== true) {
            setAlreadyGold(true);
            setGoldModalVisible(!isGoldModalVisible);
        }
    };
    const toggleGoldModalClose = () => {
        setGoldModalVisible(false);
    };
    // function checkMedalSVG() {
    //     if (level < 5) {
    //         return <MedalSVG style={styles.bigTrophy} width={200} height={200} fill="#000" />;
    //     } else if (level >= 5 && level < 10) {
    //         return <CopperMedalSVG style={styles.bigTrophy} width={200} height={200} fill="#000" />;
    //     } else if (level >= 10 && level < 15) {
    //         return <SilverMedalSVG style={styles.bigTrophy} width={200} height={200} fill="#000" />;
    //     } else {
    //         return <GoldMedalSVG style={styles.bigTrophy} width={200} height={200} fill="#000" />;
    //     }
    // }

    function checkMedalSVG() {
        let width = 200;
        let height = 200;
        if (level < 5) {
            return (
                <>
                    <View style={styles.trophyNormalBackground}>
                        <Image style={{height: height, width: width}} source={images[imgName]} />
                    </View>
                </>
            );
        } else if (level >= 5 && level < 10) {
            return (
                <View style={styles.trophyBronzeBackground}>
                    <Image style={{height: height, width: width}} source={images[imgName]} />
                </View>
            );
        } else if (level >= 10 && level < 15) {
            return (
                <View style={styles.trophySilverBackground}>
                    <Image style={{height: height, width: width}} source={images[imgName]} />
                </View>
            );
        } else if (level >= 15) {
            return (
                <View style={styles.trophyGoldBackground}>
                    <Image style={{height: height, width: width}} source={images[imgName]} />
                </View>
            );
        }
    }

    return (
        <>
            {/* <SafeAreaView> */}
            {/* <View style={styles.scrollView}> */}
            <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
                <View style={styles.backButton}>
                    <Icon
                        name={"chevron-back-outline"}
                        onPress={() => {
                            navigation.goBack();
                        }}
                    />
                    {/* dev purpose */}
                    {/* <Button
                        // testID={"close-button"}
                        onPress={() => {
                            clearLevel();
                        }}
                        compact={true}>
                        clear
                    </Button> */}
                    <TouchableOpacity
                        onPress={() => {
                            toggleRemindModal();
                        }}>
                        <BulbSVG style={styles.remind} width={50} height={50} fill="#000" />
                    </TouchableOpacity>
                </View>
                <View
                    style={{
                        justifyContent: "center",
                        alignItems: "center",
                    }}>
                    <View style={styles.container}>
                        <View style={styles.bigStarContainer}>
                            {bigStars.map((bigStar) => (
                                <BigStar
                                    key={bigStar.id}
                                    level={level}
                                    starId={bigStar.id}
                                    requiredStar={required_star}
                                />
                            ))}
                        </View>

                        <Text
                            style={{
                                marginTop: Dimensions.get("window").height * 0.04,
                                marginHorizontal:Dimensions.get("window").width * 0.07,
                                fontSize: 30,
                                fontWeight: "bold",
                                textAlign: "center",
                            }}>
                            {description}
                        </Text>
                    </View>
                    <View style={styles.goalContainer}>{checkMedalSVG()}</View>
                    <View style={styles.smallStarContainer}>
                        {level < required_star ? (
                            smallStars.map((smallStar) => (
                                <SmallStar
                                    key={smallStar.id}
                                    level={level}
                                    starId={smallStar.id}
                                    requiredStar={required_star}
                                    // onClick={addLevel}
                                    onClick={() => {
                                        toggleModal();
                                    }}
                                />
                            ))
                        ) : (
                            <View>
                                <Text
                                    style={{
                                        marginTop: 20,
                                        fontSize: 30,
                                        fontWeight: "bold",
                                        textAlign: "center",
                                    }}>
                                    任務已完成
                                </Text>
                            </View>
                        )}
                    </View>
                </View>
                <View style={{flex: 1}}>
                    <Modal
                        testID={"modal"}
                        isVisible={isModalVisible}
                        backdropColor="#aaa"
                        backdropOpacity={0.8}
                        animationIn="zoomIn"
                        animationOut="zoomOut"
                        animationInTiming={450}
                        animationOutTiming={450}
                        backdropTransitionInTiming={600}
                        backdropTransitionOutTiming={800}
                        onModalHide={() => {
                            toggleCopperModal();
                            toggleSilverModal();
                            toggleGoldModal();
                        }}>
                        <View style={styles.content}>
                            <Text style={styles.contentTitle}>完成任務?</Text>
                            <View style={styles.buttonGroup}>
                                <Button
                                    accessibilityComponentType=""
                                    accessibilityTraits=""
                                    testID={"close-button"}
                                    onPress={() => {
                                        addLevelFetch(level).then(() => {
                                            console.log("onPress Level:", level);
                                        });
                                        toggleModal();
                                    }}>
                                    <Icon name={"checkmark-sharp"} size={30} color={"blue"} />
                                </Button>
                                <Button
                                    accessibilityComponentType=""
                                    accessibilityTraits=""
                                    testID={"close-button"}
                                    onPress={() => {
                                        setAlreadyBronze(true);
                                        setAlreadySilver(true);
                                        setAlreadyGold(true);
                                        toggleModal();
                                    }}>
                                    <Icon name={"close-sharp"} size={30} color={"blue"} />
                                </Button>
                            </View>
                        </View>
                    </Modal>
                </View>
                <View>
                    <MedalModal
                        isModalVisible={isCopperModalVisible}
                        onBackdropPress={() => {
                            toggleCopperModalClose();
                            setAlreadyBronze(true);
                        }}
                        medalType={"獲得銅牌！"}
                        imgName={imgName}
                    />
                </View>
                <View>
                    <MedalModal
                        isModalVisible={isSilverModalVisible}
                        onBackdropPress={() => {
                            toggleSilverModalClose();
                        }}
                        medalType={"獲得銀牌！"}
                        imgName={imgName}
                    />
                </View>
                <View>
                    <MedalModal
                        isModalVisible={isGoldModalVisible}
                        onBackdropPress={() => {
                            toggleGoldModalClose();
                        }}
                        medalType={"獲得金牌！"}
                        imgName={imgName}
                    />
                </View>
                <View>
                    <Modal
                        testID={"modal"}
                        isVisible={isRemindModalVisible}
                        backdropColor="#aaa"
                        backdropOpacity={0.8}
                        animationIn="zoomIn"
                        animationOut="zoomOut"
                        animationInTiming={450}
                        animationOutTiming={450}
                        backdropTransitionInTiming={600}
                        backdropTransitionOutTiming={800}>
                        <View style={styles.content}>
                            <Text style={styles.remindContentTitle}>指引</Text>
                            <Text
                                style={{
                                    fontSize: 16,
                                    lineHeight: 25,
                                }}>
                                當你完成一次任務，請按一下
                                <BlankSmallStar width={20} height={20} fill="#aaa" />
                                以記錄完成任務。
                            </Text>
                            <View style={styles.buttonGroup}>
                                <Button
                                    accessibilityComponentType=""
                                    accessibilityTraits=""
                                    testID={"close-button"}
                                    onPress={() => {
                                        toggleRemindModal();
                                    }}>
                                    <Icon name={"close-sharp"} size={30} color={"blue"} />
                                </Button>
                            </View>
                        </View>
                    </Modal>
                </View>
            </ScrollView>
            {/* </View> */}
            {/* </SafeAreaView> */}
        </>
    );
}
let borderRadius = 99;
let styleHeight = 200;
let styleWidth = 200;
let stylePadding = 20;
const styles = StyleSheet.create({
    container: {
        // flex: 1,
        paddingTop: Dimensions.get("window").height * 0.0001,
        alignItems: "center",
    },
    smallStarContainer: {
        alignItems: "flex-start",
        marginTop: Dimensions.get("window").height * 0.05,
        // paddingT: Dimensions.get("window").height*0.05,
        justifyContent: "center",
        flexDirection: "row",
        // flexWrap: "wrap",
        flex: 1,
    },
    goalContainer: {
        alignItems: "flex-start",
        padding: Dimensions.get("window").height * 0.028,
        justifyContent: "space-between",
        flexDirection: "row",
        flexWrap: "wrap",
    },
    scrollView: {
        height: Dimensions.get("window").height,
        backgroundColor: "#DCEBD8",
        // flex:1,
    },
    bigTrophy: {
        margin: 10,
    },
    bigStarContainer: {
        alignItems: "center",
        // padding: 20,
        // marginTop: 10,
        justifyContent: "space-between",
        flexDirection: "row",
    },
    bigStar: {
        margin: 10,
    },
    backButton: {
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: "row",
        justifyContent: "space-between",
    },
    remind: {},
    content: {
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 15,
        borderColor: "rgba(0, 0, 0, 0.1)",
    },
    contentTitle: {
        fontSize: 30,
        margin: 12,
    },
    remindContentTitle: {
        fontSize: 30,
        margin: 20,
    },
    buttonGroup: {
        alignItems: "flex-start",
        padding: 5,
        width: "100%",
        justifyContent: "space-around",
        flexDirection: "row",
        flexWrap: "wrap",
    },
    trophyNormalBackground: {
        width: styleWidth,
        height: styleHeight,
        borderRadius: borderRadius,
        paddingTop: stylePadding,
        backgroundColor: "#000000",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: (Dimensions.get("window").width - 90 * 3) / (4 * 2),
    },
    trophyBronzeBackground: {
        width: styleWidth,
        height: styleHeight,
        borderRadius: borderRadius,
        paddingTop: stylePadding,
        backgroundColor: "#c27e3d",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: (Dimensions.get("window").width - 90 * 3) / (4 * 2),
    },
    trophySilverBackground: {
        width: styleWidth,
        height: styleHeight,
        borderRadius: borderRadius,
        paddingTop: stylePadding,
        backgroundColor: "#C0C0C0",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: (Dimensions.get("window").width - 90 * 3) / (4 * 2),
    },
    trophyGoldBackground: {
        width: styleWidth,
        height: styleHeight,
        borderRadius: borderRadius,
        paddingTop: stylePadding,
        backgroundColor: "#FFD700",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: (Dimensions.get("window").width - 90 * 3) / (4 * 2),
    },
});
