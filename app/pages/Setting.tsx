import React, {useState} from "react";
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    StatusBar,
    SafeAreaView,
    ScrollView,
    Dimensions,
    Button,
} from "react-native";
import {useNavigation} from "@react-navigation/native";
import Modal from "react-native-modal";
import {useDispatch, useSelector} from "react-redux";
import {createStackNavigator} from "@react-navigation/stack";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {logoutSuccess} from "../redux/auth/action";
import {ChangePassword} from "./SettingPassword";
import {ContactUs} from "./ContactUs";
import {AboutUs} from "./AboutUs";
import {Credit} from "./Credit";
import {doneLoading} from "../redux/nav/action";
import Icon from "react-native-ionicons";
// import { logout } from "../redux/auth/action";

function Setting() {
    const navigation = useNavigation();
    const dispatch = useDispatch();

    const [isModalLogoutVisible, setModalLogoutVisible] = useState(false);
    const toggleModalLogout = () => {
        setModalLogoutVisible(!isModalLogoutVisible);
    };

    async function logout() {
        // const logoutRes = await fetch(`${env.REACT_APP_BACKEND_URL}/logout`, {
        //   method: 'POST',
        // })
        AsyncStorage.removeItem("@storageToken_Key").then(() => dispatch(doneLoading()));
        dispatch(logoutSuccess());
        toggleModalLogout();
    }

    return (
        <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
                    {/* <View style={styles.header}> */}
                    <View
                        style={{
                            flexDirection: "row",
                            alignItems: "center",
                            margin: 15,
                        }}>
                        <View style={{flex: 1}}>
                            <Icon
                                name={"chevron-back-outline"}
                                onPress={() => {
                                    navigation.goBack();
                                }}
                            />
                        </View>

                        <View style={{flex: 10}}>
                            <Text
                                style={{
                                    fontSize: 30,
                                    textAlign: "center",
                                }}>
                                設定
                            </Text>
                        </View>
                        <View style={{flex: 1}}></View>
                    </View>

                    {/* </View> */}

                    <View style={styles.main}>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => navigation.navigate("SettingPassword")}>
                            <Text style={styles.input}>更改密碼</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => navigation.navigate("ContactUs")}>
                            <Text style={styles.input}>聯絡我們</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => navigation.navigate("AboutUs")}>
                            <Text style={styles.input}>關於我們</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => navigation.navigate("Credit")}>
                            <Text style={styles.input}>參考資料</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.footer}>
                        {/* <Text>footer</Text> */}

                        <TouchableOpacity
                            style={styles.logoutButton}
                            onPress={() => toggleModalLogout()}>
                            <Text style={styles.logoutInput}>登出</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{flex: 1}}>
                        <Modal
                            testID={"logout"}
                            isVisible={isModalLogoutVisible}
                            backdropColor="#aaa"
                            backdropOpacity={0.8}
                            animationIn="zoomIn"
                            animationOut="zoomOut"
                            animationInTiming={150}
                            animationOutTiming={150}
                            backdropTransitionInTiming={300}
                            backdropTransitionOutTiming={500}>
                            <View style={styles.content}>
                                <Text style={styles.contentTitle}>確認登出？</Text>
                                <Text></Text>
                                <View style={styles.buttonGroup}>
                                    <Button
                                        testID={"confirm-button"}
                                        onPress={() => {
                                            logout();
                                        }}
                                        title="確認"
                                    />
                                </View>
                                <View style={styles.buttonGroup}>
                                    <Button
                                        testID={"close-button"}
                                        onPress={() => {
                                            toggleModalLogout();
                                        }}
                                        title="關閉"
                                    />
                                </View>
                            </View>
                        </Modal>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </>
    );
}

const SettingStack = createStackNavigator();

export function SettingNavigator() {
    return (
        <SettingStack.Navigator headerMode="none">
            <SettingStack.Screen name="Setting" component={Setting} />
            <SettingStack.Screen name="SettingPassword" component={ChangePassword} />
            <SettingStack.Screen name="ContactUs" component={ContactUs} />
            <SettingStack.Screen name="AboutUs" component={AboutUs} />
            <SettingStack.Screen name="Credit" component={Credit} />
        </SettingStack.Navigator>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#DCEBD8",
    },

    scrollView: {
        height: "100%",
        backgroundColor: "#DCEBD8",
    },

    header: {
        flex: 1,
    },

    main: {
        paddingTop: 20,
        alignItems: "center",
    },
    footer: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center",
    },
    input: {
        textAlign: "center",
        fontSize: 20,
        padding: 2,
    },
    logoutInput: {
        textAlign: "center",
        fontSize: 20,
        color: "white",
    },
    button: {
        display: "flex",
        justifyContent: "center",
        backgroundColor: "#ffffff",
        borderRadius: 10,
        padding: 10,
        width: "80%",
        marginBottom: 20,
        shadowColor: 'grey',
        shadowOpacity: 0.5,
        shadowOffset: {
            width: 5,
            height: 5,
        },
    },
    logoutButton: {
        display: "flex",
        justifyContent: "center",
        backgroundColor: "#006400",
        borderRadius: 10,
        padding: 10,
        width: "80%",
        marginBottom: 20,
        shadowColor: 'grey',
        shadowOpacity: 0.7,
        shadowOffset: {
            width: 5,
            height: 5,
        },
    },
    content: {
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 15,
        borderColor: "rgba(0, 0, 0, 0.1)",
    },
    contentTitle: {
        fontSize: 20,
        marginBottom: 12,
        textAlign: "center",
        lineHeight: 30,
    },
    buttonGroup: {
        alignItems: "flex-start",
        padding: 5,
        width: "100%",
        justifyContent: "space-around",
        flexDirection: "row",
        flexWrap: "wrap",
    },
});
