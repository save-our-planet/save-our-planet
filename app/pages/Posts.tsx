
import { Picker } from "@react-native-picker/picker";
import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { RefreshControl, SafeAreaView, StyleSheet, Text, useWindowDimensions, View } from "react-native";
import { FlatList, ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { useSelector, useDispatch } from "react-redux";
import { loadPosts, postsUpdated } from "../redux/posts/action";
import { RootState } from "../store";
import Post from "./components/Post";
import { SelectionRow } from "./components/SelectionRow";
import { REACT_APP_BACKEND_URL } from "../env";
import { beginLoading, doneLoading } from "../redux/nav/action";

export interface IPost {
    id: number,
    title: string,
    content: string,
    user_id: number,
    username: string,
    is_hidden: boolean,
    shop_name: string,
    shop_address: string,
    eco_type: string,
    eco_type_id: number,
    district_id: number,
    photos: any,
    created_at: string
}

// const posts: IPost[] = [
//     {
//         title: 'TEST1',
//         content: 'Content',
//         username: 'user1',
//         is_hidden: false,
//         shop_name: 'Shop1',
//         shop_photo: require('../dev/photos/shop1.png'),
//         created_at: '2021-01-24T09:11:00.000Z'
//     },
//     {
//         title: 'TEST2',
//         content: 'Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content Content ',
//         username: 'user2',
//         is_hidden: false,
//         shop_name: 'Shop2',
//         shop_photo: require('../dev/photos/shop2.png'),
//         created_at: '2021-01-24T09:11:00.000Z'
//     },
//     {
//         title: 'TEST3',
//         content: 'Content',
//         username: 'user3',
//         is_hidden: false,
//         shop_name: 'Shop3',
//         shop_photo: require('../dev/photos/shop3.jpg'),
//         created_at: '2021-01-24T09:11:00.000Z'
//     },
// ]



export function Posts(){
    const navigation = useNavigation();
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    const dimensions = useWindowDimensions();
    const [districts, setDistricts] = useState<
    {
      id: number;
      name: string;
      created_at: string;
      updated_at: string;
    }[]
  >([]);;
    const [selectedDistrict, setSelectedDistrict] = useState<number>(99999);
    const [ecoTypes, setEcoTypes] = useState([]);
    const [selectedEcoType, setSelectedEcoType] = useState<number>(99999);
    const [refresh, setRefresh] = useState<boolean>(true);
    
    const [shownPosts, setShownPosts] = useState<IPost[]>([]);

    const posts = useSelector((state: RootState) => state.posts.posts)
    const dispatch = useDispatch();

    async function loadEcoTypes() {
        const res = await fetch(`${REACT_APP_BACKEND_URL}/ecoTypes`);
        const results = await res.json();
        results.unshift({
            id: 99999,
            name: '所有分類',
            created_at: '12:34',
            updated_at: '12:34'
        })
        
            setEcoTypes(results.map((el: any) => {
                return {
                    name: el.name,
                    id: el.id
                }}))
            setSelectedEcoType(results[0].id)
    }

    async function loadDistrict() {
        // console.log(process.env.REACT_APP_BACKEND_URL);
    
        const res = await fetch(`${REACT_APP_BACKEND_URL}/district`);
        const results = await res.json();
        results.data.unshift({
            id: 99999,
            name: '所有地區',
            created_at: '12:34',
            updated_at: '12:34'
        })
        
            setDistricts(results.data);
            setSelectedDistrict(results.data[0].id);

    }

    function filterPosts(){
        setShownPosts(posts.filter(post => 
            (post.eco_type_id == selectedEcoType || selectedEcoType == 99999)
            && (post.district_id == selectedDistrict || selectedDistrict == 99999)
            && (post.is_hidden == false)
            ))
    }

    useEffect(() => {
        dispatch(beginLoading())
        Promise.all([
            loadDistrict(),
            loadEcoTypes()
        ]).then(()=>dispatch(loadPosts()))

    }, []);

    useEffect(() => {
        dispatch(beginLoading())
        Promise.all([
            filterPosts()
        ]).then(()=>dispatch(loadPosts()))
        // dispatch(doneLoading())

    }, [selectedEcoType, selectedDistrict])

    useEffect(() => {
        filterPosts()
        if(refresh){
            dispatch(loadPosts())
            setRefresh(false)
            
        }
    }, [posts, refresh]);

    return (
    <SafeAreaView style={{
        flex: 1, 
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#DCEBD8',}}>
        
                <FlatList
                    ListHeaderComponent={
                        <>
                            {/* <ScrollView
                                showsVerticalScrollIndicator={false}
                                refreshControl={
                                <RefreshControl
                                    refreshing={refresh}
                                    onRefresh={() => setRefresh(true)}/>}
                                contentInsetAdjustmentBehavior="automatic"
                                style={styles.scrollView}> */}
                            <View style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingTop: 30,
                                // width: dimensions.width
                            }}>
                                <Text style={{
                                    fontSize: 30,
                                    fontWeight: '600'
                                }}>綠色資訊</Text>
                            <View style={{
                                width: dimensions.width * 0.8,
                            flexDirection: 'row',
                            margin: 20,
                        }}>
                            <SelectionRow
                                label='商舖地區'
                                value={selectedDistrict}
                                onValueChange={(itemValue: any) => setSelectedDistrict(itemValue)}
                                list={districts}/>
                            <SelectionRow
                                label='環保分類'
                                value={selectedEcoType}
                                onValueChange={(itemValue: any) => setSelectedEcoType(itemValue)}
                                list={ecoTypes}/>
                        </View>

                                {/* <SafeAreaView>

                                    <Picker
                                        selectedValue={selectedDistrict}
                                        style={{
                                            height: 200,
                                            width: 200,
                                        }}
                                        onValueChange={(itemValue: any) => setSelectedDistrict(itemValue)
                                    }>
                                        {districts.map((d, i) => (
                                            <Picker.Item key={i} label={d.name} value={d.name} />
                                        ))}
                                    </Picker>
                                </SafeAreaView> */}

                                <View style={{
                                    width: '100%',
                                    paddingRight: 30,
                                    alignItems: 'flex-end'
                                }}>
                                    <TouchableOpacity style={{
                                        width: '100%',
                                        borderRadius: 10,
                                        backgroundColor: 'tomato',
                                        justifyContent: 'flex-end',
                                        shadowColor: 'grey',
                                        shadowOpacity: 0.5,
                                        shadowOffset: {
                                            width: 5,
                                            height: 5,
                                        },
                                    }} onPress={() => {
                                            navigation.navigate('CreatePost')
                                        }}>
                                        <Text style={{
                                            color: 'white',
                                            fontWeight: '700',
                                            fontSize: 20,
                                            margin: 10,
                                            
                                        }}>新增文章</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {/* </ScrollView> */}
                        </>                   
                    }
                    ListEmptyComponent={
                        <Text style={{
                            margin: 30,
                            fontSize: 20,
                            textAlign: 'center'
                        }}>
                            沒有文章 :(
                        </Text>
                    }
                    refreshing={refresh}
                    onRefresh={() => setRefresh(true)}
                    contentContainerStyle={{
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                    data={shownPosts}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={post => (
                        <Post
                            // photos={loadPhotos(post.item.id)}
                            index={post.index}
                            ipost={post.item}
                            setRefresh={() => setRefresh(true)} />
                )}/>
            
        
    </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    scrollView: {
      backgroundColor: '#DCEBD8',
    },
    image: {
        width: '100%',
        height: 200,
        resizeMode: 'cover',
    },
    postContainer: {
        backgroundColor: 'white',
        marginVertical: 10,
        marginHorizontal: 30,
        alignItems: 'center'
    }
  });