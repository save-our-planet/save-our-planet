import { useIsFocused, useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { RefreshControl, StyleSheet } from "react-native";
import { View, Text, FlatList, Picker, SafeAreaView, ScrollView, TouchableOpacity } from "react-native";
import Icon from "react-native-ionicons";
import { useDispatch, useSelector, useStore } from "react-redux";
import { loadPosts } from "../redux/posts/action";
import Post from "./components/Post";
import { REACT_APP_BACKEND_URL } from "../env";

export function SavedPosts(){
    const navigation = useNavigation();
    const [districts, setDistricts] = useState<
    {
      id: number;
      name: string;
      created_at: string;
      updated_at: string;
    }[]
  >([]);;
    const [selectedDistrict, setSelectedDistrict] = useState('');
    const [ecoType, setEcoType] = useState('');
    const [selectedEcoType, setSelectedEcoType] = useState('');
    const [posts, setPosts] = useState<any[]>([])
    const [refresh, setRefresh] = useState<boolean>(true)
    const state = useStore().getState()
    const dispatch = useDispatch()

    async function loadSavedPosts(){
        const postsRes = await fetch(`${REACT_APP_BACKEND_URL}/saved-posts`, {
            headers: {
              Authorization: 'Bearer ' + state.auth.token,
            },
          });
        const json = await postsRes.json();
        console.log(json);
        
        for(let post of json){
            post.photos = await loadPhotos(post.id)
        }

        setPosts(json)
    }

    async function loadPhotos(postID: number){

        const photosRes = await fetch(`${REACT_APP_BACKEND_URL}/post-photos/${postID}`);
        const photos = await photosRes.json()
        
        return photos
    }


    const isFocused = useIsFocused()
    const [changePage, setChangePage] = useState<boolean>(false)

    useEffect(() => {
        if(refresh){
            loadSavedPosts();
            dispatch(loadPosts())
            setRefresh(false)
        }
    }, [posts, refresh]);

    useEffect(()=>{
        if(!isFocused || changePage){
            navigation.reset({
                index: 0,
                routes:[
                    {name: "HomePage"}
                ]
            })
        }
    }, [isFocused, changePage])

    return (
        
        <SafeAreaView style={{flex: 1, backgroundColor: '#DCEBD8',}}>
        <FlatList
                    ListHeaderComponent={
                            // <ScrollView
                            // style={{
                            //     flex: 1
                            // }}
                            // contentInsetAdjustmentBehavior="automatic"
                            // refreshControl={
                            //     <RefreshControl
                            //       refreshing={refresh}
                            //       onRefresh={() => setRefresh(true)}/>}
                            // style={styles.scrollView}>
                            <View style={{
                                flex: 1,
                                
                            }}>
                                <View style={{
                                    
                                    flexDirection: 'row', 
                                    justifyContent: 'flex-start'}}>
                                        <View style={styles.backButton}>
                                            <Icon
                                                name={"chevron-back-outline"}
                                                onPress={() => {
                                                    setChangePage(true)
                                                }}
                                            />
                                        </View>
                                        <View style={styles.container}>
                                            <Text
                                                style={{
                                                fontSize: 30,
                                                fontWeight: 'bold',
                                                textAlign: 'center',
                                                }}
                                                onPress={() => {
                                                    setChangePage(true)
                                                }}>
                                                我的最愛
                                            </Text>
                                        </View>
                                    </View>
                                <View style={{
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>

                    </View>
                    </View>

                    }
                    style={{
                        backgroundColor: '#DCEBD8',
                    }}
                    ListHeaderComponentStyle={{
                        width: "100%",
                        paddingLeft: 10,
                        alignItems: 'flex-start'
                    }}
                    contentContainerStyle={{
                        alignItems: 'center'
                    }}
                    data={posts}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={post => (
                        <Post
                            // photos={loadPhotos(post.item.id)}
                            index={post.index}
                            ipost={post.item}
                            setRefresh={() => setRefresh(true)} />
                )}/>
                {/* // </ScrollView> */}
    </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    scrollView: {
      backgroundColor: '#DCEBD8',
    },
    backButton: {
        // paddingTop: 40,
        marginTop: 10,
        marginLeft: 10,
    },
    image: {
        width: '100%',
        height: 200,
        resizeMode: 'cover',
    },
    container: {
        paddingTop: 10,
        alignItems: 'center',
        marginBottom: 10,
    },
    postContainer: {
        backgroundColor: 'white',
        marginVertical: 10,
        marginHorizontal: 30,
        alignItems: 'center'
    }
  });