import {useNavigation} from "@react-navigation/native";
import React, { useState } from "react";
import {
    Dimensions,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    View,
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import Icon from "react-native-ionicons";

export function Credit() {
    const navigation = useNavigation();
    const [filteredBins, setFilteredBins] = useState<any>([])
    const credit = [
        "React Native",
        "flaticon",
        "Freepik",
        "環境保護署",
        "綠色和平",
        "不是垃圾站",
        "回收俠 - 紙包飲品盒回收",
        "Mil Mill 喵坊",
        "迷失的寶藏：發泡膠回收行動"
    ];


    return (
        <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                {/* <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}> */}
                    
                    {/* {credit.map((row)=>(<Text style={{color: "black", fontSize: 30, margin: 10}}>{row}
                            </Text>))} */}
                  
                    <FlatList
                        ListHeaderComponent={
                            <View>
                                <View
                                    style={{
                                        flexDirection: "row",
                                        alignItems: "center",
                                        margin: 15,
                                    }}>
                                    <View style={{flex: 1}}>
                                        <Icon
                                            name={"chevron-back-outline"}
                                            onPress={() => {
                                                navigation.goBack();
                                            }}
                                        />
                                    </View>

                                    <View style={{flex: 10}}>
                                        <Text
                                            style={{
                                                fontSize: 30,
                                                textAlign: "center",
                                            }}>
                                            參考資料
                                        </Text>
                                    </View>

                                    <View style={{flex: 1}}></View>
                                </View>
                            </View>
                        }
                        data={credit}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={
                            <View>
                                <Text style={{color: "black", fontSize: 30, margin: 10, textAlign:'center'}}>
                                    連接伺服器中...
                            </Text>
                            </View>
                        }
                        renderItem={(row) => {
                            return (
                                <Text style={{color: "black", fontSize: 20, margin: 10, textAlign:'center'}}>{row.item}
                            </Text>
                            )
                        }}
                        
                        />

                        
                
                {/* </ScrollView> */}
            </SafeAreaView>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#DCEBD8",
    },
    scrollView: {
        height: "100%",
        backgroundColor: "#DCEBD8",
    },
    backButton: {
        marginTop: 10,
        marginLeft: 10,
    },
    main: {
        marginTop: 20,
        // justifyContent: "center",
        alignItems: "center",
        // height: Dimensions.get("window").height * 0.2,
    },
});
