import React, {useState} from "react";
import {
    Button,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    StatusBar,
    SafeAreaView,
    ScrollView,
} from "react-native";
import {useNavigation} from "@react-navigation/native";
import Icon from "react-native-ionicons";
import Modal from "react-native-modal";
import { REACT_APP_BACKEND_URL } from "../env";
import {NotiModal} from "./components/NotiModal";

export function SignUp() {
    const navigation = useNavigation();
    const [username, setUsername] = useState<string>("");
    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [confirmPassword, setConfirmPassword] = useState<string>("");

    //check if boxes are empty
    const [emptyTextInput, setEmptyTextInput] = useState<boolean>(false);

    //check if password length less than 6 digit or alphabet
    const [passwordLength, setPasswordLength] = useState<boolean>(false);

    //check if email is valid or not (consist @)
    const [emailValidate, setEmailValidate] = useState<boolean>(false);

    const [existed, setExisted] = useState<boolean>(false);
    const [existedEmail, setExistedEmail] = useState<boolean>(false);
    const [wrongPassword, setWrongPassword] = useState<boolean>(false);
    const [register, setRegister] = useState<boolean>(false);

    // const [isModalSuccessfullyRegister, setModalSuccessfullyRegister] = useState(false);
    // const toggleMedalSuccessfullyRegister = () => {
    //     setModalSuccessfullyRegister(!isModalSuccessfullyRegister);
    // };

    async function submit() {
        if (!username.length || !email.length || !password.length || confirmPassword.length === 0) {
            setEmptyTextInput(true);
            return;
        }

        const emailValidation = /\S+@\S+\.\S+/.test(email);
        if (!emailValidation) {
            setEmailValidate(true);
            return;
        }

        // function emailIsValid(email: string) {
        //     const emailValidation = /\S+@\S+\.\S+/.test(email);
        //     if (!emailValidation) {
        //         setEmailValidate(true);
        //         return;
        //     }
        // }
        // emailIsValid(email);

        const checkUsernameRes = await fetch(`${REACT_APP_BACKEND_URL}/checkUsername`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                username: username,
            }),
        });

        const checkUsernameJson = await checkUsernameRes.json();
        if (checkUsernameJson.status == 401) {
            setExisted(true);
            console.log("signup.tsx existed username");
            return;
        }

        const checkEmailRes = await fetch(`${REACT_APP_BACKEND_URL}/checkEmail`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                email: email,
            }),
        });

        const checkEmailJson = await checkEmailRes.json();
        if (checkEmailJson.status == 401) {
            setExistedEmail(true);
            console.log("signup.tsx existed email");
            return;
        }

        if (password.length < 6) {
            setPasswordLength(true);
            return;
        }

        if (password != confirmPassword) {
            console.log("wrong password");
            setWrongPassword(true);
            return;
        }

        const registerRes = await fetch(`${REACT_APP_BACKEND_URL}/register`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                username: username,
                email: email,
                password: password,
            }),
        });
        const json = await registerRes.json();
        console.log("Sign up tsx - " + json.msg);

        setRegister(true)
        // toggleMedalSuccessfullyRegister();
        console.log("success");
    }

    return (
        <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
                    <View style={styles.backButton}>
                        <Icon
                            name={"chevron-back-outline"}
                            onPress={() => {
                                navigation.goBack();
                            }}
                        />
                    </View>

                    <View
                        style={{
                            height: "auto",
                            justifyContent: "center",
                            alignItems: "center",
                        }}>
                        <View
                            style={{
                                paddingTop: 40,
                                paddingBottom: 10,
                            }}>
                            <Text
                                style={{
                                    fontSize: 35,
                                    fontWeight: "bold",
                                    textAlign: "center",
                                    color: "green",
                                }}>
                                新用戶登記
                            </Text>
                        </View>

                        <View
                            style={{
                                paddingBottom: 40,
                            }}>
                            <Text
                                style={{
                                    fontSize: 20,
                                    textAlign: "center",
                                }}></Text>
                        </View>

                        <TextInput
                            style={styles.input}
                            onChangeText={(text) => setUsername(text)}
                            value={username}
                            placeholder="用戶名稱"
                            placeholderTextColor='#696969'
                            autoCapitalize="none"
                        />
                        <TextInput
                            style={styles.input}
                            onChangeText={(text) => setEmail(text)}
                            value={email}
                            placeholder="電郵地址"
                            placeholderTextColor='#696969'
                            autoCapitalize="none"
                            keyboardType="email-address"
                        />
                        <TextInput
                            style={styles.input}
                            onChangeText={(password) => setPassword(password)}
                            value={password}
                            placeholder="密碼 (最少6位數字或字母,可包含符號）"
                            placeholderTextColor='#696969'
                            secureTextEntry={true}
                            autoCapitalize="none"
                        />
                        <TextInput
                            style={styles.input}
                            onChangeText={(password) => setConfirmPassword(password)}
                            value={confirmPassword}
                            placeholder="確認密碼"
                            placeholderTextColor='#696969'
                            secureTextEntry={true}
                            autoCapitalize="none"
                        />
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => {
                                submit();
                            }}>
                            <Text>提交</Text>
                        </TouchableOpacity>
                    </View>

                    <NotiModal
                        testID={"existed-username"}
                        text={`用戶名稱已登記`}
                        buttonTestId={"close-button"}
                        modalVisible={existed}
                        toggleModal={() => setExisted(false)}
                    />

                    <NotiModal
                        testID={"existed-username-email"}
                        text={`電郵地址已登記`}
                        buttonTestId={"close-button"}
                        modalVisible={existedEmail}
                        toggleModal={() => setExistedEmail(false)}
                    />

                    <NotiModal
                        testID={"wrong-password"}
                        text={"密碼錯誤"}
                        buttonTestId={"close-button"}
                        modalVisible={wrongPassword}
                        toggleModal={() => setWrongPassword(false)}
                    />

                    <NotiModal
                        testID={"empty-textInput"}
                        text={`所有資料必須填寫`}
                        buttonTestId={"close-button"}
                        modalVisible={emptyTextInput}
                        toggleModal={() => setEmptyTextInput(false)}
                    />

                    <NotiModal
                        testID={"password-length"}
                        text={`請輸入最少6位數字或\n英文字母的密碼\n（可包含符號）`}
                        buttonTestId={"close-button"}
                        modalVisible={passwordLength}
                        toggleModal={() => setPasswordLength(false)}
                    />

                    <NotiModal
                        testID={"email-validation"}
                        text={`請輸入有效的電郵地址`}
                        buttonTestId={"close-button"}
                        modalVisible={emailValidate}
                        toggleModal={() => setEmailValidate(false)}
                    />

                    <NotiModal
                        testID={"successfully-register"}
                        text={`成功註冊!`}
                        buttonTestId={"close-button"}
                        modalVisible={register}
                        toggleModal={() => {
                            setRegister(false)
                            navigation.navigate("Login")
                        }}
                    />
                    
                </ScrollView>
            </SafeAreaView>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#DCEBD8",
    },
    body: {
        backgroundColor: "#DCEBD8",
    },
    scrollView: {
        height: "100%",
        backgroundColor: "#DCEBD8",
    },
    input: {
        textAlign: "center",
        height: 40,
        borderColor: "gray",
        backgroundColor: "#ffffff",
        borderWidth: 0,
        borderRadius: 10,
        marginBottom: 20,
        width: "80%",
    },
    button: {
        display: "flex",
        justifyContent: "center",
        backgroundColor: "#ffffff",
        borderRadius: 10,
        padding: 10,
        margin: 5,
    },
    backButton: {
        marginTop: 10,
        marginLeft: 10,
    },
    content: {
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 15,
        borderColor: "rgba(0, 0, 0, 0.1)",
    },
    contentTitle: {
        fontSize: 20,
        marginBottom: 12,
        textAlign: "center",
        lineHeight: 30,
    },
    buttonGroup: {
        alignItems: "flex-start",
        padding: 5,
        width: "100%",
        justifyContent: "space-around",
        flexDirection: "row",
        flexWrap: "wrap",
    },
});
