import React, {useState} from "react";
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    StatusBar,
    SafeAreaView,
    ScrollView,
    Dimensions,
    Button,
} from "react-native";
import {useNavigation} from "@react-navigation/native";
import {REACT_APP_BACKEND_URL} from "../env";
import {NotiModal} from "./components/NotiModal";
import Icon from "react-native-ionicons";
import { useStore } from "react-redux";

export function ChangePassword() {
    const navigation = useNavigation();
    const state = useStore().getState();

    const [currentPassword, setCurrentPassword] = useState<string>("");
    const [newPassword, setNewPassword] = useState<string>("");
    const [confirmNewPassword, setConfirmNewPassword] = useState<string>("");

    const [emptyTextInput, setEmptyTextInput] = useState<boolean>(false);

    //New password modal setting
    const [wrongPassword, setWrongPassword] = useState<boolean>(false);
    const [passwordLength, setPasswordLength] = useState<boolean>(false);
    const [changePw, setChangePw] = useState<boolean>(false);
    const [currentPasswordWrong, setCurrentPasswordWrong] = useState<boolean>(false);
    const [serverProblem, setServerProblem] = useState<boolean>(false);

    async function submit() {
        const checkPasswordRes = await fetch(`${REACT_APP_BACKEND_URL}/checkPassword`, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + state.auth.token,
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                currentPassword: currentPassword,
            }),
        });
        const checkPasswordJson = await checkPasswordRes.json();
        if(checkPasswordJson.status === 401){
            setCurrentPasswordWrong(true)
            return;
        }
        
        if (!currentPassword.length || !newPassword.length || !confirmNewPassword.length) {
            setEmptyTextInput(true);
            return;
        }

        if (newPassword.length < 6) {
            setPasswordLength(true);
            return;
        }

        if (!matchPassword(newPassword, confirmNewPassword)) {
            //toggleModal
            setWrongPassword(true);
            return;
        }

        

        // if (newPassword != confirmNewPassword) {
        //     console.log("wrong password");
        //     setWrongPassword(true);
        //     return;
        // }

        function matchPassword(newPassword: string, confirmNewPassword: string) {
            if (newPassword == confirmNewPassword) {
                return true;
            }
            return false;
        }



        const changePasswordRes = await fetch(`${REACT_APP_BACKEND_URL}/newPassword`, {
            method: "PUT",
            headers: {
                Authorization: "Bearer " + state.auth.token,
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                newPassword: newPassword,
            }),
        });

        const changePasswordJson = await changePasswordRes.json();
        if (changePasswordJson.result === 'success'){
            setChangePw(true)
        } else {
            setServerProblem(true)
        }

    }

    return (
        <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
                    <View style={styles.header}>

                    <View style={{
                            flexDirection: "row",
                            alignItems:'center',
                            margin: 15,
                            }}>

                        <View style={{flex: 1}}>
                            <Icon
                                name={"chevron-back-outline"}
                                onPress={() => {
                                    navigation.goBack();
                                }}
                            />
                        </View>

                        <View
                            style={{flex: 10}}>
                            <Text style={{
                                fontSize: 30,
                                textAlign: 'center',
                        }}>更改密碼</Text>
                        </View>

                        <View
                            style={{
                                flex: 1,
                                
                            }}>
                            
                        </View>

                    </View>
                    </View>

                    <View style={styles.main}>
                        {/* <Text> main</Text> */}
                        <TextInput
                            style={styles.input}
                            onChangeText={(currentPassword) => setCurrentPassword(currentPassword)}
                            value={currentPassword}
                            placeholder="舊密碼"
                            placeholderTextColor='#696969'
                            secureTextEntry={true}
                        />
                        <TextInput
                            style={styles.input}
                            onChangeText={(newPassword) => setNewPassword(newPassword)}
                            value={newPassword}
                            placeholder="新密碼(最少6位數字或字母的密碼(可包含符號)"
                            placeholderTextColor='#696969'
                            secureTextEntry={true}
                        />

                        <TextInput
                            style={styles.input}
                            onChangeText={(newPassword) => setConfirmNewPassword(newPassword)}
                            value={confirmNewPassword}
                            placeholder="確認新密碼"
                            placeholderTextColor='#696969'
                            secureTextEntry={true}
                        />

                        <TouchableOpacity 
                            style={styles.button} 
                            onPress={() => submit()}>
                            <Text>更改</Text>
                        </TouchableOpacity>
                    </View>

                    {/* <View style={styles.footer}>
                    <Text>footer
                    </Text>
                </View> */}

                    <NotiModal
                        testID={"wrong-password"}
                        text={"密碼錯誤"}
                        buttonTestId={"close-button"}
                        modalVisible={wrongPassword}
                        toggleModal={() => setWrongPassword(false)}
                    />

                    <NotiModal
                        testID={"current-password-wrong"}
                        text={"密碼錯誤"}
                        buttonTestId={"close-button"}
                        modalVisible={currentPasswordWrong}
                        toggleModal={() => setCurrentPasswordWrong(false)}
                    />

                    <NotiModal
                        testID={"password-length"}
                        text={`請輸入最少6位數字或\n英文字母的密碼\n（可包含符號）`}
                        buttonTestId={"close-button"}
                        modalVisible={passwordLength}
                        toggleModal={() => setPasswordLength(false)}
                    />

                    <NotiModal
                        testID={"empty-textInput"}
                        text={`所有資料必須填寫`}
                        buttonTestId={"close-button"}
                        modalVisible={emptyTextInput}
                        toggleModal={() => setEmptyTextInput(false)}
                    />

                    <NotiModal
                        testID={"successfully-change-pw"}
                        text={`成功更改密碼!`}
                        buttonTestId={"close-button"}
                        modalVisible={changePw}
                        toggleModal={() => {
                            setChangePw(false)
                            navigation.navigate("Setting")
                        }}
                    />

                    <NotiModal
                        testID={"server-problem"}
                        text={`伺服器錯誤`}
                        buttonTestId={"close-button"}
                        modalVisible={serverProblem}
                        toggleModal={() => {
                            setServerProblem(false)
                        }}
                    />

                </ScrollView>
            </SafeAreaView>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#DCEBD8",
    },

    scrollView: {
        height: "100%",
        backgroundColor: "#DCEBD8",
    },

    header: {
        // backgroundColor: '#dad9c4',
    },

    main: {
        paddingTop: 20,
        alignItems: "center",
        // height: Dimensions.get("window").height * 0.7,
    },
    footer: {
        flex: 1,
    },

    input: {
        textAlign: "center",
        fontSize:14,
        height: 40,
        borderColor: "gray",
        backgroundColor: "#ffffff",
        borderWidth: 0,
        borderRadius: 10,
        marginBottom: 20,
        width: "80%",
    },
    button: {
        display: "flex",
        justifyContent: "center",
        backgroundColor: "#ffffff",
        borderRadius: 10,
        padding: 10,
        margin: 5,
    },
    content: {
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 15,
        borderColor: "rgba(0, 0, 0, 0.1)",
    },
    contentTitle: {
        fontSize: 20,
        marginBottom: 12,
        textAlign: "center",
        lineHeight: 30,
    },
    buttonGroup: {
        alignItems: "flex-start",
        padding: 5,
        width: "100%",
        justifyContent: "space-around",
        flexDirection: "row",
        flexWrap: "wrap",
    },
    backButton: {
        marginTop: 10,
        marginLeft: 10,
    },
});
