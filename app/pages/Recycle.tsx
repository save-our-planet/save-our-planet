import {Picker} from "@react-native-picker/picker";
import {useNavigation} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import React, {useEffect, useState} from "react";
import {
    Button,
    FlatList,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    useWindowDimensions,
    View,
} from "react-native";
import {useDispatch} from "react-redux";
import {setMapLocation} from "../redux/Map/action";
import {IMap, RecycleMap} from "./RecycleMap";
// import {Colors} from 'react-native/Libraries/NewAppScreen';
// import { District } from '../components/district';
import {REACT_APP_BACKEND_URL} from "../env";
import {UserCurrentLocation} from "./UserLocation";
import {SelectionRow} from "./components/SelectionRow";
import {beginLoading, doneLoading} from "../redux/nav/action";

function Recycle() {
    const navigation = useNavigation();
    const dimensions = useWindowDimensions();
    const dispatch = useDispatch();

    const [bin, setBin] = useState<
        {
            id: number;
            name: string;
            address: string;
            district: string;
            office_hour: string;
            contact: string;
            type: string;
            created_at: string;
            updated_at: string;
        }[]
    >([]);
    const [search, setSearch] = useState<
        {
            id: number;
            name: string;
            address: string;
            district: string;
            office_hour: string;
            contact: string;
            type: string;
            created_at: string;
            updated_at: string;
        }[]
    >([]);

    console.log("search", search);

    const [selectedTypeValue, setSelectedTypeValue] = useState<string>("所有種類");

    const [district, setDistrict] = useState<
        {
            id: number;
            name: string;
            created_at: string;
            updated_at: string;
        }[]
    >([]);

    const [selectedValue, setSelectedValue] = useState<number>();
    const [type, setType] = useState<string>("abc");
    const [filteredBins, setFilteredBins] = useState<any>([])

    const recycle_type = ["所有種類", "舊書", "衣服", "電腦用品", "電器", "食物", "慳電膽及光管", "玻璃樽", "可充電式電池", "紙包飲品盒", "塑膠廢料", "其他"]

    async function loadDistrict() {
        // console.log(process.env.REACT_APP_BACKEND_URL);

        const res = await fetch(`${REACT_APP_BACKEND_URL}/district`);
        const results = await res.json();
        // console.log("loadDistrict",results);
        setDistrict(results.data);
        setSelectedValue(results.data[0].id)
    }
    async function loadBins() {
        // console.log(process.env.REACT_APP_BACKEND_URL);

        const res = await fetch(`${REACT_APP_BACKEND_URL}/bins`);
        const results = await res.json();
        setBin(results.data);
        // console.log("loadBins: ", results.data)
    }
    async function searchBins() {
        // console.log(process.env.REACT_APP_BACKEND_URL);

        const res = await fetch(`${REACT_APP_BACKEND_URL}/search/` + selectedValue);
        const results = await res.json();
        setSearch(results.data);
        return results.data
    }

    async function loadMap(location: IMap) {
        dispatch(setMapLocation(location));
        navigation.navigate("RecycleMap");
    }

    function filterBins(bins: {type: string}[], value: string){
        let result;
        if(bins){
            result = bins.filter(el => selectedTypeValue === "所有種類" || el.type.includes(value));
        }
        setFilteredBins(result)
    }


    useEffect(() => {
        dispatch(beginLoading())
        loadDistrict()
        .then(()=>dispatch(doneLoading()))
    },[])

    useEffect(() => {
        dispatch(beginLoading());
        loadBins()
        .then(() => searchBins())
        .then(result => filterBins(result, selectedTypeValue))
        .then(() => dispatch(doneLoading()));

        console.log("selected Value: ", selectedValue);
        console.log("filtered Bins: ", filteredBins);
        
        
        // console.log(selectedValue);
    }, [selectedValue, selectedTypeValue]);


    return (
        <SafeAreaView
            style={{
                flex: 1,
                // justifyContent: "center",
                // alignItems: "center",
                backgroundColor: "#DCEBD8",
            }}>
            <FlatList
                contentContainerStyle={{
                    // flex: 1,
                    // margin: 5,
                    // justifyContent: "center",
                    // alignItems: "center",
                    backgroundColor: "#DCEBD8",
                    padding: 10
                }}
                ListHeaderComponent={
                    <>
                        {/* <ScrollView
                                contentInsetAdjustmentBehavior="automatic"
                                style={styles.scrollView}> */}
                        {/* <View
                                    style={styles.container}> */}
                        <Text
                            style={{
                                fontSize: 30,
                                fontWeight: "bold",
                                textAlign: "center",
                            }}>
                            回收地點資訊
                        </Text>

                        {/* <TouchableOpacity
                                        onPress={() => navigation.navigate("UserLocation")}>
                                        <Text
                                            style={{
                                                fontSize: 20,
                                                margin: 5,
                                            }}>
                                            Map
                                        </Text>
                                    </TouchableOpacity> */}

                        <View style={{flexDirection: "row", marginTop:25}}>
                            <SelectionRow
                                label="地區"
                                value={selectedValue}
                                onValueChange={(itemValue: any) => setSelectedValue(itemValue)}
                                list={district}
                            />

                            <SelectionRow
                                label="回收種類"
                                value={selectedTypeValue}
                                useItemAsValue
                                onValueChange={(itemValue: any) => setSelectedTypeValue(itemValue)}
                                list={recycle_type}
                            />

                            {/* <Picker
                                        selectedValue={selectedValue}
                                        style={{height: 100, minWidth: "40%"}}
                                        onValueChange={
                                            (itemValue: any, itemIndex) =>
                                                setSelectedValue(itemValue)
                                            // console.log(itemValue)
                                        }>
                                        {district.map((d, i) => (
                                            <Picker.Item key={i} label={d.name} value={d.name} />
                                        ))}
                                    </Picker> */}

                            {/* <Text> abc</Text> */}

                            {/* <Picker
                                        selectedValue={selectedTypeValue}
                                        style={{height: 100, minWidth: "40%"}}
                                        onValueChange={
                                            (itemValue: any, itemIndex) =>
                                                setSelectedTypeValue(itemValue)
                                            // console.log(itemValue)
                                        }
                                        >
                                        {recycle_type.map((d, i) => (
                                            <Picker.Item key={i} label={d} value={d} />
                                        ))}
                                    </Picker> */}
                        </View>
                        {/* </View> */}
                        {/* </ScrollView> */}
                    </>
                }
                data={filteredBins}
                keyExtractor={(item, index) => index.toString()}
                ListEmptyComponent={
                    <View style={styles.info}>
                        <Text style={styles.name}>所選擇 地區/ 種類 沒有回收資訊</Text>
                    </View>
                }
                renderItem={(row) => {
                    return (
                        <TouchableOpacity
                            onPress={() => loadMap(row.item)}
                            key={row.index}
                            style={styles.info}>
                            <Text style={styles.name}>{row.item.name}</Text>
                            <Text style={{fontSize: 16, paddingBottom:10}}>{row.item.address}</Text>
                            <Text style={{fontSize: 16, paddingBottom:10}}>{row.item.office_hour}</Text>
                            <Text style={{fontSize: 16, paddingBottom:10}}>{row.item.contact}</Text>
                            <Text style={{fontSize: 16, paddingBottom:10}}>{row.item.type}</Text>
                        </TouchableOpacity>
                    );
                }}
            />
        </SafeAreaView>
    );
}

const RecycleStack = createStackNavigator();

export function RecycleNavigator() {
    const [preLoadMap, setPreLoadMap] = useState<boolean>(true);
    return (
        <RecycleStack.Navigator headerMode="none">
            <RecycleStack.Screen name="Recycle" component={Recycle} />
            <RecycleStack.Screen name="RecycleMap" component={RecycleMap} />
            <RecycleStack.Screen name="UserLocation" component={UserCurrentLocation} />
        </RecycleStack.Navigator>
    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
    },
    name: {
        fontSize: 22,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    info: {
        backgroundColor: "#ffffff",
        padding: 15,
        borderRadius: 15,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        shadowColor: 'grey',
        shadowOpacity: 0.5,
        shadowOffset: {
            width: 5,
            height: 5,
        },
    },
    scrollView: {
        // backgroundColor: "#DCEBD8",
    },
});
