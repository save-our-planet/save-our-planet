import React from "react";
import {StatusBar, StyleSheet, Text} from "react-native";
import {RecycleNavigator} from "./Recycle";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {TrophyNavigator} from "./Trophy";
import Icon, {IconName} from "react-native-ionicons";
import {SettingNavigator} from "./Setting";
import {PostsNavigator} from "./navigations/PostsNav";
import {HomePageNavigator} from "./navigations/HomePageNav";
import { Tips } from "./Tips";

export function HomeTab() {
    const Tab = createBottomTabNavigator();

    return (
        <Tab.Navigator
            screenOptions={({route}) => ({
                tabBarLabel: ({focused, color}) =>{
                    let label;
                    switch (route.name) {
                        case "HomePage":
                            label = "主頁";
                            break;
                        case "Recycle":
                            label = "回收點";
                            break;
                        case "Posts":
                            label = "綠色資訊";
                            break;
                        case "Trophy":
                            label = "任務";
                            break;
                        case "Tips":
                            label = "小知識";
                            break;
                        default:
                            label = "sad"
                    }

                    return <Text style={{
                        color: color,
                        fontSize: 12,
                        padding: 3
                    }}>
                        {label}
                    </Text>
                },
                tabBarIcon: ({focused, color, size}) => {
                    let iconName: IconName;

                    switch (route.name) {
                        case "HomePage":
                            iconName = focused ? "home" : "home-outline";
                            break;
                        case "Recycle":
                            iconName = focused ? "leaf" : "leaf-outline";
                            break;
                        case "Posts":
                            iconName = focused ? "newspaper" : "newspaper-outline";
                            break;
                        case "Trophy":
                            iconName = focused ? "trophy" : "trophy-outline";
                            break;
                        case "Tips":
                            iconName = focused ? "book" : "book-outline";
                            break;
                        default:
                            iconName = focused ? "sad" : "sad-outline";
                    }

                    return <Icon name={iconName} size={size} color={color} />;
                },
            })}
            tabBarOptions={{
                activeTintColor: "tomato",
                inactiveTintColor: "gray",
                activeBackgroundColor: "#eeeeee",
                // showLabel: false,
            }}>
            <Tab.Screen name="Recycle" component={RecycleNavigator} />
            <Tab.Screen name="Posts" component={PostsNavigator} />
            <Tab.Screen name="HomePage" component={HomePageNavigator} />
            <Tab.Screen name="Trophy" component={TrophyNavigator} />
            <Tab.Screen name="Tips" component={Tips} />
        </Tab.Navigator>
    );
}

const styles = StyleSheet.create({});
