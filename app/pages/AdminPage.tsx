import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { Button, RefreshControl } from "react-native";
import { FlatList, SafeAreaView, ScrollView, StyleSheet, Text, View } from "react-native";
import Icon from "react-native-ionicons";
import { useDispatch, useSelector } from "react-redux";
import { beginLoading, doneLoading } from "../redux/nav/action";
import { loadPosts } from "../redux/posts/action";
import { RootState } from "../store";
import Post from "./components/Post";

export function AdminPage(){
    const navigation = useNavigation();
    const [districts, setDistricts] = useState<
    {
      id: number;
      name: string;
      created_at: string;
      updated_at: string;
    }[]
  >([]);;
    // const [selectedDistrict, setSelectedDistrict] = useState('');
    // const [ecoType, setEcoType] = useState('');
    // const [selectedEcoType, setSelectedEcoType] = useState('');
    // const [posts, setPosts] = useState<any>([])
    const [refresh, setRefresh] = useState<boolean>(true)
    
    const posts = useSelector((state: RootState) => state.posts.posts)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(beginLoading())
        if(refresh){
            dispatch(loadPosts())
            setRefresh(false)
        }
        dispatch(doneLoading())
        
    }, [posts, refresh]);

    return (
        
        
        <FlatList
                            ListHeaderComponent={
                                <SafeAreaView style={{flex: 1}}>
                                    <ScrollView
                                    contentInsetAdjustmentBehavior="automatic"
                                    refreshControl={
                                        <RefreshControl
                                          refreshing={refresh}
                                          onRefresh={() => setRefresh(true)}/>}
                                    style={styles.scrollView}>
                                        <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                                                <View style={styles.backButton}>
                                                    <Icon
                                                        name={"chevron-back-outline"}
                                                        onPress={() => {
                                                            navigation.goBack();
                                                        }}
                                                    />
                                                </View>
                                                <View style={styles.container}>
                                                    <Text
                                                        style={{
                                                        fontSize: 30,
                                                        fontWeight: 'bold',
                                                        textAlign: 'center',
                                                        }}
                                                        onPress={() => {
                                                            navigation.goBack();
                                                        }}>
                                                        管理員版面
                                                    </Text>
                                                </View>
                                        </View>
                                    </ScrollView>
                                </SafeAreaView>

                            }
                            style={{
                                backgroundColor: '#FEF4D0',
                            }}
                            ListHeaderComponentStyle={{
                                width: "100%",
                                paddingLeft: 10,
                                alignItems: 'flex-start'
                            }}
                            contentContainerStyle={{
                                alignItems: 'center'
                            }}
                            data={posts}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={post => (
                                    <Post
                                        // photos={loadPhotos(post.item.id)}
                                        index={post.index}
                                        ipost={post.item}
                                        isAdmin
                                        setRefresh={() => setRefresh(true)}
                                    />
                        )}/>

                    
            
    )
}

const styles = StyleSheet.create({
    scrollView: {
      backgroundColor: '#FEF4D0',
    },
    image: {
        width: '100%',
        height: 200,
        resizeMode: 'cover',
    },
    backButton: {
        // paddingTop: 40,
        marginTop: 10,
        marginLeft: 10,
    },
    container: {
        paddingTop: 10,
        alignItems: 'center',
    },
    postContainer: {
        backgroundColor: 'white',
        marginVertical: 10,
        marginHorizontal: 30,
        alignItems: 'center'
    }
  });