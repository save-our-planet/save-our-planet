import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { Animated, Easing, KeyboardAvoidingView, Platform, RefreshControl, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, useWindowDimensions, View } from "react-native";

import { PostHighLights } from "./components/HomePageRelated/postHighlights";
import HeartSVG from "../dev/photos/svg/love.svg";
import MailSVG from "../dev/photos/svg/mailbox.svg";
import AdminSVG from "../dev/photos/svg/admin.svg";
import SettingSVG from "../dev/photos/svg/gear.svg";
import { TopButtons } from "./components/HomePageRelated/topButtons";
import { useDispatch, useSelector, useStore } from "react-redux";
import { RootState } from "../store";
import { DMBox } from "./components/HomePageRelated/directMessageBox";
import { ObjectiveSummary } from "./components/HomePageRelated/objectiveSummary";
import { REACT_APP_BACKEND_URL } from "../env";

export function HomePage(){
    const dimensions = useWindowDimensions();
    const loading = useSelector((state: RootState)=> state.nav.loading)
    // const token = useSelector((state: RootState) => state.auth.token)
    const [isAdmin, setIsAdmin] = useState<boolean>(false);
    const [refresh, setRefresh] = useState<boolean>(true);
    const [showDMBox, setShowDMBox] = useState<boolean>(false);
    const [userID, setUserID] = useState<number>()
    const state = useStore().getState();
    const dispatch = useDispatch();
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    const [username, setUsername] = useState<string>('')

    async function checkAdmin(){
        let res = await fetch(`${REACT_APP_BACKEND_URL}/checkAdmin`,{
            headers: {
              Authorization: 'Bearer ' + state.auth.token,
            },
          });
        let result = await res.json();
        if(result.length > 0){
            setUserID(result[0].user_id)
            setUsername(result[0].name)

            if (result[0].is_admin){
                setIsAdmin(result[0].is_admin)
            }
        }
    }

    const [refreshing, setRefreshing] = useState<boolean>(false)

    async function onRefresh(){
        setRefreshing(true)
        setTimeout(function(){
            setRefreshing(false)
            
        }
        ,1000)
    }

    useEffect(()=>{
        checkAdmin()
    }, [isAuthenticated])

    return(
        
        <SafeAreaView style={[{flex: 1, }, styles.safeAreaView]}>
            
            <ScrollView 
            // stickyHeaderIndices={[0]}
            refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  onRefresh={() => onRefresh()}/>}
            style={{
                backgroundColor: '#DCEBD8',
                // flex: 1,
                // position: 'relative',
                height: dimensions.height,
            }}
            contentInsetAdjustmentBehavior="automatic">
                
                <View style={{
                    // backgroundColor: 'white',
                    paddingHorizontal: 30,
                    paddingVertical: 10,
                    opacity: 0.95,
                    // borderBottomLeftRadius: dimensions.width,
                    // borderBottomRightRadius: dimensions.width,
                    }}>
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                                alignItems: 'flex-end',
                                paddingHorizontal: 20
                            }}
                        >

                        <View>

                    {isAdmin && 
                        <TopButtons 
                        navPage={"AdminPage"} 
                        image={
                        <AdminSVG style={{
                            opacity: 0.9,
                        }} />}
                        />}

                    {!isAdmin && <Text style={{
                        marginLeft: 20,
                        fontSize: 26,
                        fontWeight: '600'
                    }}>
                        {username}
                    </Text>}
                        </View>

                    <View style={{
                        width: '100%',
                        flexDirection: 'row-reverse',
                        alignContent: 'space-around'
                    }} >  

                    <TopButtons
                    navPage={"Setting"}
                    
                    image={
                    <SettingSVG style={{
                        // width: 40,
                        // height: 40,
                        opacity: 0.9,
                        // alignSelf: 'flex-end',
                        // position: 'relative',
                        // margin: 10
                    }}/>}/>

                    <TopButtons
                    navPage={"SavedPosts"}
                    
                    image={
                    <HeartSVG style={{
                        // width: 40,
                        // height: 40,
                        opacity: 0.9,
                        // alignSelf: 'flex-end',
                        // position: 'relative',
                        // margin: 10
                    }}/>}/>

                    <TopButtons
                    onPress={() => setShowDMBox(true)}
                    // style={{flex:1,
                    // backgroundColor:'red'}}
                    image={
                    <MailSVG style={{
                        // width: 40,
                        // height: 40,
                        opacity: 0.9,
                        // alignSelf: 'flex-end',
                        // position: 'relative',
                        // margin: 10
                    }}/>}/>

                    
                    
                </View>

                </View>
                    
                </View>
                
                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    }}>
                    {/* <View style={styles.container}>
                        <Text
                            style={{
                            fontSize: 30,
                            fontWeight: 'bold',
                            textAlign: 'center',
                            }}
                            onPress={() => setShowDMBox(true)}
                            >
                            🌱 ('v') 🌱
                        </Text>
                    </View> */}
                    <PostHighLights style={styles.homePageSection}/>
                    <ObjectiveSummary style={styles.homePageSection}/>
                </View>
                <View>
                    <DMBox
                        userID={userID}
                        isAdmin={isAdmin}
                        isVisible={showDMBox}
                        hideModal={() => setShowDMBox(false)} />
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    safeAreaView: {
        backgroundColor: '#DCEBD8'
    },
    container: {
        paddingTop: 40,
        alignItems: 'center',
    },
    homePageSection: {
        backgroundColor: 'white',
        borderRadius: 20,
        // padding: 10,
        margin: 15,
        shadowColor: 'grey',
        shadowOpacity: 0.5,
        shadowOffset: {
            width: 5,
            height: 5,
        },
    }
})