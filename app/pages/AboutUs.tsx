import {useNavigation} from "@react-navigation/native";
import React from "react";
import {
    Dimensions,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    View,
} from "react-native";
import Icon from "react-native-ionicons";

export function AboutUs() {
    const navigation = useNavigation();

    return (
        <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
                    <View>
                        <View
                            style={{
                                flexDirection: "row",
                                alignItems: "center",
                                margin: 15,
                            }}>
                            <View style={{flex: 1}}>
                                <Icon
                                    name={"chevron-back-outline"}
                                    onPress={() => {
                                        navigation.goBack();
                                    }}
                                />
                            </View>

                            <View style={{flex: 10}}>
                                <Text
                                    style={{
                                        fontSize: 30,
                                        textAlign: "center",
                                    }}>
                                    關於我們
                                </Text>
                            </View>

                            <View style={{flex: 1}}></View>
                        </View>
                    </View>

                    <View style={styles.main}>
                        <Text
                            style={{
                                fontSize: 20,
                                marginLeft: 25,
                                marginRight: 25,
                                textAlign: 'center',
                                lineHeight: 28
                            }}>
                            我們是Tecky Academy 科啟學院，MicroMaster In AI & Programming
                            第12期準畢業同學，希望透過此應用程式推廣大眾環保意識，建立綠色生活習慣。
                        </Text>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#DCEBD8",
    },
    scrollView: {
        height: "100%",
        backgroundColor: "#DCEBD8",
    },
    main: {
        paddingTop: 20,
        alignItems: "center",
    },
});
