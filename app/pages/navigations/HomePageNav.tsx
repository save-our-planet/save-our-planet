import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { HomePage } from "../HomePage";
import { SavedPosts } from "../SavedPosts";
import { AdminPage } from "../AdminPage";
import { SettingNavigator } from "../Setting";
import { useSelector } from "react-redux";
import { RootState } from "../../store";
import { Login } from "../Login";
import { SignUp } from "../Signup";

export function HomePageNavigator() {
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);


    const homePageStack = createStackNavigator();

    return (
        <homePageStack.Navigator headerMode="none">
            {!isAuthenticated && <homePageStack.Screen name='Login' component={Login} options={{headerShown: false}}/>}
            {!isAuthenticated && <homePageStack.Screen name='Sign-up' component={SignUp} options={{headerShown: false}}/>}
            {isAuthenticated && <homePageStack.Screen name="HomePage" component={HomePage} options={{animationTypeForReplace: 'pop'}} />}
            {isAuthenticated && <homePageStack.Screen name="SavedPosts" component={SavedPosts} />}
            {isAuthenticated && <homePageStack.Screen name="AdminPage" component={AdminPage}  />}
            {isAuthenticated && <homePageStack.Screen name="Setting" component={SettingNavigator} />}
        </homePageStack.Navigator>
    )
}