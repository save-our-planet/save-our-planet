import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { useSelector } from "react-redux";
import { RootState } from "../../store";
import { CreatePost } from "../CreatePost";
import { Login } from "../Login";
import { SignUp } from "../Signup";

export function CreatePostNavigator() {

    const createPostStack = createStackNavigator();
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);

    return (
        <createPostStack.Navigator headerMode="none">
            {!isAuthenticated && <createPostStack.Screen name='Login' component={Login} options={{headerShown: false}}/>}
            {!isAuthenticated && <createPostStack.Screen name='Sign-up' component={SignUp} options={{headerShown: false}}/>}
            <createPostStack.Screen name="CreatePost" component={CreatePost} />
        </createPostStack.Navigator>
    )
}