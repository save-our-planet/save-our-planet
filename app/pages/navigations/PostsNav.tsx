
import { createStackNavigator, TransitionPresets } from "@react-navigation/stack";
import { CreatePost } from "../CreatePost";
import { Posts } from "../Posts";
import { useSelector } from "react-redux";
import { RootState } from "../../store";
import { Login } from "../Login";
import { SignUp } from "../Signup";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import React , { useCallback } from "react";
import { CreatePostNavigator } from "./CreatePostNav";

export function PostsNavigator() {

    const postsStack = createStackNavigator();
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    const transitionScreenOptions = {
        ...TransitionPresets.SlideFromRightIOS, // This is where the transition happens
    };

    return (
        <postsStack.Navigator headerMode="none" >
            <postsStack.Screen name="Posts" component={Posts} options={{animationTypeForReplace: "pop"}}/>
            {!isAuthenticated && <postsStack.Screen name='Login' component={Login} options={{headerShown: false}}/>}
            {!isAuthenticated && <postsStack.Screen name='Sign-up' component={SignUp} options={{headerShown: false}}/>}
            <postsStack.Screen name="CreatePost" component={CreatePostNavigator} />
        </postsStack.Navigator>
    )
}