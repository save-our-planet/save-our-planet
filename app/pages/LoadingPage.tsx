import React, { useEffect, useState } from "react";
import { View, Text, ActivityIndicator, useWindowDimensions} from "react-native";
import { useSelector } from "react-redux";
import { RootState } from "../store";
import Modal from 'react-native-modal';

export function Loading(){
    const loading = useSelector((state: RootState) => state.nav.loading);
    // const [dots, setDots] = useState<string>('.')
    const dimensions = useWindowDimensions()

    return (
        // <Modal
        //       testID={'1'}
        //       isVisible={true}
        //       backdropColor="#aaa"
        //       backdropOpacity={0.8}
        //       animationIn="zoomIn"
        //       animationOut="zoomOut"
        //       animationInTiming={250}
        //       animationOutTiming={250}
        //       backdropTransitionInTiming={300}
        //       backdropTransitionOutTiming={500}>
        <View style={{
            position: 'absolute',
            // backgroundColor: 'red',
            width: dimensions.width,
            height: dimensions.height,
            justifyContent: 'center',
            alignItems: 'center',
            // opacity: 0.8,
            zIndex: 10
        }}>
            <View style={{
                width: 100,
                height: 100,
                backgroundColor: '#666666',
                opacity: 0.8,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <ActivityIndicator 
                    color= 'white'
                    size = 'large'
                />
            </View>
        </View>
        // </Modal>
    )

}