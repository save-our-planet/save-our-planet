import React from "react";
import { Modal, StyleSheet, Text, TextInput, useWindowDimensions, View} from "react-native";

export function Preview(props: {
    visibility: boolean,
    onChangeText: (text: any) => void,
    onClose: () => void,
    value: string,
}){

    const dimensions = useWindowDimensions()

    return(
            <Modal
                animationType='fade'
                transparent={true}
                visible={props.visibility}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text onPress={props.onClose}>X</Text>
                        <TextInput 
                        style={{
                            width: dimensions.width * 0.7,
                            height: dimensions.height * 0.5,
                        }}
                        onChangeText={props.onChangeText}
                        value={props.value}
                        // placeholder={props.placeholder}
                        />
                    </View>
                </View>
            </Modal>
    )
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
        backgroundColor: '#00000060',
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        opacity: 1,
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
  },
})