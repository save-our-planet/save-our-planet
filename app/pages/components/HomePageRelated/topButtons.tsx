import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Animated, Easing, TouchableWithoutFeedback, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

export function TopButtons(props: {
    navPage?: "AdminPage" | "SavedPosts" | "Setting"
    image: JSX.Element,
    onPress?: () => void,
    style?: {}
}){
    const navigation = useNavigation();
    let scaleValue = new Animated.Value(0); // declare an animated value
    const cardScale = scaleValue.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [1, 0.8, 0.7],
    });
    let transformStyle = {
        transform: [{scale: cardScale}],
    };

    return(
        <View style={{
            width: 40,
            height: 40,
            marginHorizontal: 5,
            justifyContent: 'center',
            alignItems: 'center',
            marginLeft: 10
            // backgroundColor: 'red'
        }}>

        <TouchableWithoutFeedback
                    onPressIn={() => {
                        scaleValue.setValue(0);
                        Animated.timing(scaleValue, {
                        toValue: 1,
                        duration: 250,
                        easing: Easing.linear,
                        useNativeDriver: true,
                        }).start();
                        
                    }}
                    onPressOut={() => {
                        Animated.timing(scaleValue, {
                        toValue: 0,
                        duration: 100,
                        easing: Easing.linear,
                        useNativeDriver: true,
                        }).start();
                        props.navPage && navigation.navigate(props.navPage);
                        props.onPress && props.onPress()
                    }}>
                    <Animated.View style={transformStyle}>
                        <View style={[{
                            // position: 'absolute',
                            width: 50,
                            height: 50,
                            padding: 5,
                            // borderBottomLeftRadius: 10,
                            // borderBottomRightRadius: 10,
                            // justifyContent: 'center',
                            // alignItems: 'center',
                        }, props.style]}>

                            {props.image}
                        </View>

                    </Animated.View>
                    </TouchableWithoutFeedback>
        </View>
                

    )

}