import React, {useEffect, useState} from "react";
import {Dimensions, Image, StyleSheet, Text, useWindowDimensions, View} from "react-native";
// import MedalSVG from "../../../dev/photos/svg/medal.svg";
import * as Progress from "react-native-progress";
import {useDispatch, useStore} from "react-redux";
import {useNavigation} from "@react-navigation/native";
import {ScrollView, TouchableOpacity} from "react-native-gesture-handler";
import {REACT_APP_BACKEND_URL} from "../../../env";
import {beginLoading, doneLoading} from "../../../redux/nav/action";

const images: any = {
    Bottle: require("../../../dev/photos/medals/Bottle.png"),
    BringYourOwnCup: require("../../../dev/photos/medals/BringYourOwnCup.png"),
    BringYourOwnCutlery: require("../../../dev/photos/medals/BringYourOwnCutlery.png"),
    Electric: require("../../../dev/photos/medals/Electric.png"),
    First_login_award: require("../../../dev/photos/medals/First_login_award.png"),
    GoVegeterian: require("../../../dev/photos/medals/Go_vegeterian.png"),
    Grocery: require("../../../dev/photos/medals/Grocery.png"),
    LunchBox: require("../../../dev/photos/medals/LunchBox.png"),
    PlasticCutlery: require("../../../dev/photos/medals/PlasticCutlery.png"),
    Recycle: require("../../../dev/photos/medals/Recycle.png"),
    RecycleBag: require("../../../dev/photos/medals/RecycleBag.png"),
    ShareToFriend: require("../../../dev/photos/medals/ShareToFriend.png"),
    Straw: require("../../../dev/photos/medals/Straw.png"),
    Transport: require("../../../dev/photos/medals/Transport.png"),
    TurnOffLight1hour: require("../../../dev/photos/medals/TurnOffLight1hour.png"),
};
export function ObjectiveSummary(props: {style: any}) {
    const state = useStore().getState();
    const navigation = useNavigation();

    const dimensions = useWindowDimensions();
    const [medals, setMedals] = useState<
        {
            id: number;
            name: string;
            description: string;
            unit: string;
            category_id: number;
            required_star: number;
            objective_category: string;
            user_id: number;
            level: number;
            ImgName: string;
        }[]
    >([]);
    const [finishedMedalsLength, setFinishedMedalsLength] = useState(0);
    const dispatch = useDispatch();

    async function loadObjective() {
        const res = await fetch(`${REACT_APP_BACKEND_URL}/objective`, {
            headers: {
                Authorization: "Bearer " + state.auth.token,
            },
        });
        const results = await res.json();
        // console.log(results.data);
        let allMedals = results.data;
        // console.log(allMedals);

        setMedals(results.data);
        const finishedMedals = await allMedals.filter(
            (medal: any) => medal.level === medal.required_star
        );
        // console.log(finishedMedals);

        setFinishedMedalsLength(finishedMedals.length);
    }
    async function firstLogin() {
        const getFirstLogin = await fetch(`${REACT_APP_BACKEND_URL}/objective/login`, {
            headers: {
                Authorization: "Bearer " + state.auth.token,
            },
        });
        await getFirstLogin.json();
        await loadObjective();
    }
    function determineMedal(level: number, id: number, imgName: string) {
        let width = 60;
        let height = 60;
        if (level < 5) {
            return (
                <>
                    <View style={styles.trophyNormalBackground}>
                        <Image style={{height: height, width: width}} source={images[imgName]} />
                    </View>
                </>
            );
        } else if (level >= 5 && level < 10) {
            return (
                <View style={styles.trophyBronzeBackground}>
                    <Image style={{height: height, width: width}} source={images[imgName]} />
                </View>
            );
        } else if (level >= 10 && level < 15) {
            return (
                <View style={styles.trophySilverBackground}>
                    <Image style={{height: height, width: width}} source={images[imgName]} />
                </View>
            );
        } else if (level >= 15) {
            return (
                <View style={styles.trophyGoldBackground}>
                    <Image style={{height: height, width: width}} source={images[imgName]} />
                </View>
            );
        }
    }
    // function findFinishedMedals() {
    //     const finishedMedals = medals.filter((medal) => medal.level === medal.required_star);
    //     // console.log(finishedMedals);

    //     setFinishedMedalsLength(finishedMedals.length);
    // }
    // useEffect(() => {
    //     loadObjective();
    //     // findFinishedMedals();

    // }, []);
    useEffect(() => {
        const updateObject = navigation.addListener("focus", () => {
            // console.log("1234");
            // dispatch(beginLoading())
            // The screen is focused
            // Call any action
            loadObjective().then(() => firstLogin());
            // .then(()=> dispatch(doneLoading()))

            // findFinishedMedals();
            // console.log(finishedMedalsLength);
        });

        // Return the function to unsubscribe from the event so it gets removed on unmount
        return updateObject;
    }, [navigation]);

    return (
        <>
            <View style={[props.style, {width: dimensions.width * 0.8}]}>
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate("Trophy");
                    }}>
                    <Text
                        style={{
                            fontSize: 20,
                            fontWeight: "600",
                            paddingTop: 20,
                            paddingBottom: 20,
                            alignSelf: "center",
                            // borderBottomWidth:5,
                            // borderBottomColor:"#FFFFFF"
                        }}>
                        任務進度
                    </Text>

                    <View>
                        {medals
                            .slice(finishedMedalsLength, finishedMedalsLength + 3)
                            .map((medal, i) => (
                                <View key={i}>
                                    {medal.level === medal.required_star ? (
                                        <View key={i}></View>
                                    ) : (
                                        <View key={i}>
                                            <View key={i} style={styles.objectiveRow}>
                                                <Text style={{fontSize: 20, width: "75%"}}>
                                                    {medal.description}
                                                </Text>
                                                <Text style={{fontSize: 18}}>
                                                    {medal.level}/{medal.required_star}
                                                </Text>
                                            </View>
                                            <View
                                                style={{
                                                    alignItems: "center",
                                                    marginBottom: 10,
                                                    flex: 1,
                                                }}>
                                                <Progress.Bar
                                                    progress={medal.level / medal.required_star}
                                                    width={Dimensions.get("window").width*0.7}
                                                />
                                            </View>
                                        </View>
                                    )}
                                </View>
                            ))}
                    </View>
                </TouchableOpacity>
            </View>
            <View style={[props.style, {width: dimensions.width * 0.8}]}>
                {/* <TouchableOpacity
                    onPressOut={() => {
                        navigation.navigate("Trophy");
                    }}> */}
                <Text
                    style={{
                        fontSize: 20,
                        fontWeight: "600",
                        paddingTop: 20,
                        paddingBottom: 20,
                        alignSelf: "center",
                        // borderBottomWidth:5,
                        // borderBottomColor:"#FFFFFF"
                    }}>
                    已獲得獎章
                </Text>
                {/* </TouchableOpacity> */}

                <ScrollView horizontal={true} contentContainerStyle={styles.medalsRow}>
                    {medals.map((medal, i) => (
                        <TouchableOpacity
                            key={i}
                            onPress={() => {
                                navigation.navigate("Trophy");
                            }}>
                            <View key={i}>
                                {medal.level < medal.required_star / 3 ? (
                                    <View key={i}></View>
                                ) : (
                                    <View key={i}>
                                        {determineMedal(medal.level, medal.id, medal.ImgName)}
                                    </View>
                                )}
                            </View>
                        </TouchableOpacity>
                    ))}
                </ScrollView>
            </View>
        </>
    );
}
const styleHeight = 60;
const styleWidth = 60;
const stylePadding = 5;
const styles = StyleSheet.create({
    objectiveRow: {
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 30,
        paddingVertical: 20,
    },
    medalsRow: {
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    trophyNormalBackground: {
        width: styleWidth,
        height: styleHeight,
        paddingTop: stylePadding,
        borderRadius: 90,
        backgroundColor: "#000000",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: 5,
    },
    trophyBronzeBackground: {
        width: styleWidth,
        height: styleHeight,
        paddingTop: stylePadding,
        borderRadius: 90,
        backgroundColor: "#c27e3d",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: 5,
    },
    trophySilverBackground: {
        width: styleWidth,
        height: styleHeight,
        paddingTop: stylePadding,
        borderRadius: 90,
        backgroundColor: "#C0C0C0",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: 5,
    },
    trophyGoldBackground: {
        width: styleWidth,
        height: styleHeight,
        paddingTop: stylePadding,
        borderRadius: 90,
        backgroundColor: "#FFD700",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: 5,
    },
});
