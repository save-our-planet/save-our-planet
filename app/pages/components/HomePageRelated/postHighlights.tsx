import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { Image, StyleSheet, Text, useWindowDimensions } from "react-native";
import { View } from "react-native";
import SwiperFlatList from "react-native-swiper-flatlist";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../store";
import { REACT_APP_BACKEND_URL, REACT_APP_S3 } from "../../../env";
import { doneLoading, beginLoading } from "../../../redux/nav/action";
import { TouchableOpacity } from "react-native-gesture-handler";

export function PostHighLights(props: {
    style: any
}){
    const dispatch = useDispatch();
    const navigation = useNavigation()
    const [post, setPost] = useState<any>({});
    const posts = useSelector((state: RootState) => state.posts.posts)
    const dimensions = useWindowDimensions()

    async function loadNewestPost(){
        let res = await fetch(`${REACT_APP_BACKEND_URL}/newestPost`);
        let newestPost = await res.json();

        newestPost[0].photos = await loadPhotos(newestPost[0].id)
        
        setPost(newestPost[0])
    }

    async function loadPhotos(postID: number){

        const photosRes = await fetch(`${REACT_APP_BACKEND_URL}/post-photos/${postID}`);
        const photos = await photosRes.json()
        
        return photos
    }

    useEffect(()=>{
        dispatch(beginLoading())
        loadNewestPost().then(() => {
            dispatch(doneLoading())})
    }, [posts])

    // useEffect(()=>{
        
    //     loadNewestPost()
        
    // },[posts])

    return(
        <TouchableOpacity onPress={() => navigation.navigate("Posts")} style={[props.style, {alignItems: 'center', width: dimensions.width * 0.8}]}>
            <Text style={{
                fontSize: 20,
                fontWeight: '600',
                margin: 5
            }}>最新文章</Text>
            <SwiperFlatList
                autoplay={false}
                autoplayDelay={2}
                autoplayLoop={false}
                index={0}
                showPagination={false}
                data={post.photos}
                renderItem={({ item }) => (
                    <View style={{height: 200, width: dimensions.width * 0.8}}>
                        <Image style={{height: 200, width: dimensions.width * 0.8}} source={{uri: `${REACT_APP_S3}/${item.path}`}}/>
                    </View>
                )}/>
            <View >
                <Text style={[styles.text, {fontSize: 16, fontWeight: '800'}]}>{post.title}</Text>
                <Text style={styles.text}>{post.name}</Text>
                <Text style={styles.text}>{post.address}</Text>

            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    text:{
        margin: 5,
        textAlign: 'center'
    }
})