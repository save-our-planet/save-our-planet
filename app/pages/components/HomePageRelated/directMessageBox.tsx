
import { Picker } from '@react-native-picker/picker';
import React, { useEffect, useState } from 'react'
import { ActivityIndicator, Button, KeyboardAvoidingView, Platform, ScrollView, StyleSheet, Text, useWindowDimensions, View } from "react-native";
import { FlatList, TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import Modal from 'react-native-modal';
import { useDispatch, useStore } from 'react-redux';
import { socket } from '../../../App';
import { NotiModal } from '../NotiModal';
import { REACT_APP_BACKEND_URL } from "../../../env";
import { beginLoading, doneLoading } from '../../../redux/nav/action';

export function DMBox(props: {
    isVisible: boolean,
    hideModal: () => void
    isAdmin: boolean,
    userID?: number
}){
    const dimensions = useWindowDimensions()
    const [directMessages, setDMs] = useState<{
        content: string,
        name: string,
        admin_id: number,
        user_id: number,
        sender_id: number,
        is_read: boolean,
        created_at: string
        }[]>([]);
    const state = useStore().getState();
    socket.on('direct-message', (dm: {
        content: string,
        name: string,
        receiver_name: string,
        admin_id: number,
        user_id: number,
        sender_id: number,
        is_read: boolean,
        created_at: string}) => {
            console.log("DM: ", dm);
            console.log("props.userID:", props.userID);
            console.log("dm.sender_id:", dm.sender_id);
            
            if(!props.userID){
                dm.name = dm.receiver_name
            }
                setDMs([
                    dm,
                    ...directMessages
                ])
    })
    const [draftDM, setDraftDM] = useState<string>('')
    const [allUsers, setAllUsers] = useState<{
        id: number,
        name: string
        is_admin: boolean
    }[]>([])
    const [selectedUser, setSelectedUser] = useState<number>(99999)
    const [shownDMs, setShownDMs] = useState<{
        content: string,
        name: string,
        receiver_name?: string,
        admin_id: number,
        user_id: number,
        sender_id: number,
        is_read: boolean,
        created_at: string
        }[]>([])
    const [emptyDMModal, setEmptyDMModal] = useState<boolean>(false)
    const [sendBtnLoading, setSendBtnLoading] = useState<boolean>(false)
    const dispatch = useDispatch()
    
    async function loadDMs() {
        
        if(props.isAdmin == true){
            
            let res = await fetch(`${REACT_APP_BACKEND_URL}/adminDMs`,{
                headers: {
                    Authorization: 'Bearer ' + state.auth.token,
                  },
            })
            let dms = await res.json()
            setDMs(dms)
            setShownDMs(dms)

        } else {
            let res = await fetch(`${REACT_APP_BACKEND_URL}/direct-messages`, {
                headers: {
                    Authorization: 'Bearer ' + state.auth.token,
                  },
            })
            let dms = await res.json()
            setDMs(dms)
            setShownDMs(dms)
        }
    }

    async function sendDM(){
        if(draftDM.replace(/\s/g, '') == ''){
            setEmptyDMModal(true);
            return;
        }
        
        if(props.isAdmin === true){
            let formData = new FormData();
            formData.append('content', draftDM);
            formData.append('userID', selectedUser);
            formData.append('isAdmin', true);

            await fetch(`${REACT_APP_BACKEND_URL}/direct-message`,{
                method: 'POST',
                headers: {
                    Authorization: 'Bearer ' + state.auth.token,
                },
                body: formData
            })
        } else {
            for(let user of allUsers){
                
                if(user.is_admin){
                    console.log("ADMIN: ", user);
                    let formData = new FormData();
                    formData.append('content', draftDM);
                    formData.append('adminID', user.id);
                    formData.append('isAdmin', false);

                    await fetch(`${REACT_APP_BACKEND_URL}/direct-message`,{
                        method: 'POST',
                        headers: {
                            Authorization: 'Bearer ' + state.auth.token,
                        },
                        body: formData
                    })
                } 
            }
        }
        setDraftDM('')
    }

    async function loadUserList(){
        let res = await fetch(`${REACT_APP_BACKEND_URL}/allUsers`, {
            headers: {
                Authorization: 'Bearer ' + state.auth.token,
              },
        })

        let result = await res.json()
        if(result.length > 0){

            result.unshift({
                id: 99999,
                name: '所有用戶',
                is_admin: false
            })
            setAllUsers(result)
        }
    }

    function filterDMs(){
        directMessages.length > 0 && setShownDMs(directMessages.filter(dm => selectedUser == 99999 || dm.user_id == selectedUser))
    }


    useEffect(()=>{
        dispatch(beginLoading())
        Promise.all([
            loadDMs(),
            loadUserList()
        ]).then(()=>dispatch(doneLoading()))
        
    }, [props.isAdmin])

    useEffect(()=>{
        dispatch(beginLoading())
        Promise.all([
            filterDMs()
        ]).then(()=>dispatch(doneLoading()))
    }, [selectedUser, directMessages])

    return(
        <Modal
            testID={'successfully-register'}
              isVisible={props.isVisible}
              backdropColor="#aaa"
              backdropOpacity={0.8}
              animationIn="zoomIn"
              animationOut="zoomOut"
              animationInTiming={450}
              animationOutTiming={450}
              backdropTransitionInTiming={600}
              backdropTransitionOutTiming={800}
              style={{
                  alignItems: 'center'
              }}>
                  <KeyboardAvoidingView 
                  behavior={"position"}
                  style={{marginTop: 60}}
                  contentContainerStyle={{marginBottom: 30}}
                    // keyboardVerticalOffset={300}
                  >

                  
                <View
                    style={[
                        styles.content, 
                        {
                            width: dimensions.width * 0.9, 
                            // height: dimensions.height * 0.8
                            height: "auto",
                            paddingBottom: 20
                        }]}>
                        
                    <View style={{
                        width: dimensions.width * 0.8,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between'
                    }}>
                        <Text style={{
                            fontSize: 30,
                            fontWeight: 'bold',
                            textAlign: 'center',
                            margin: 10,}}>
                        系統訊息
                        </Text>
                        
                        <TouchableOpacity
                        onPress={props.hideModal}>
                            <Text style={{
                                fontSize: 50,
                                fontWeight: '500'
                            }}>
                                -
                            </Text>
                        </TouchableOpacity>
                        </View>
                    {props.isAdmin && <View style={{
                        // backgroundColor: 'red',
                        width: '100%'
                    }}>

                        <Picker
                            selectedValue={selectedUser}
                            itemStyle={{
                                fontSize: 16,
                                height: dimensions.height * 0.18
                            }}
                            onValueChange={(value: any, index)=>{
                                setSelectedUser(value)
                            }}>
                                {allUsers.map((d, i)=> (
                                    <Picker.Item key={i} label={d.name} value={d.id}/>
                                ))}

                        </Picker>
                    </View>}

                    <View style={[styles.dmsContainer, 
                        props.isAdmin && {
                        width: dimensions.width * 0.8, 
                        height: dimensions.height * 0.4
                        },

                        !props.isAdmin && {
                            width: dimensions.width * 0.8,
                            height: dimensions.height * 0.64
                        }
                        ]}>
                        <FlatList
                            data={shownDMs}
                            ListEmptyComponent={(
                                <Text style={{
                                    fontSize: 20,
                                    textAlign: "center",
                                    margin: 20
                                }}>
                                    沒有訊息
                                </Text>
                            )}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={message => (
                                <View style={{
                                    margin: 10,
                                    flexDirection: "row",
                                    justifyContent: 'space-between',
                                    borderBottomWidth: 0.3,
                                    borderColor: 'grey',
                                    paddingBottom: 5
                                    }}>
                                    <View>
                                        <Text style={{
                                            flexWrap: 'wrap',
                                            width: dimensions.width * 0.5
                                        }}>
                                            {message.item.content}
                                            </Text>
                                        <Text style={{fontWeight: "700"}}>
                                            {((message.item.sender_id === message.item.admin_id) && props.isAdmin ||
                                            (message.item.sender_id === message.item.user_id) && !props.isAdmin) ? "You" : message.item.name}
                                        &nbsp;to&nbsp;
                                            {((message.item.sender_id === message.item.user_id) && props.isAdmin ||
                                            (message.item.sender_id === message.item.admin_id) && !props.isAdmin) ? "You" : message.item.name}
                                        </Text>
                                        
                                    </View>
                                    <View style={{
                                        width: dimensions.width * 0.2,
                                        flexDirection: 'column',
                                        alignItems: 'flex-end'
                                        }}>
                                        {/* <Text>
                                            {message.item.created_at}
                                        </Text> */}
                                        <Text style={{
                                            fontWeight: "700",
                                            textAlign: 'right',
                                            flexWrap: 'wrap'
                                        }}>
                                            {message.item.created_at}
                                        </Text>
                                    </View>
                                </View>
                            )}>

                        </FlatList>
                    </View>
                {(!props.isAdmin || selectedUser != 99999) && <View style={{
                    marginTop: 10,
                    flexDirection: 'row',
                    alignItems: 'flex-end'
                }}>

                    <TextInput
                    multiline
                    style={{
                        width: dimensions.width * 0.8 - 60,
                        height: 60,
                        borderColor: 'grey',
                        borderRadius: 10,
                        borderWidth: 0.5,
                        paddingLeft: 10,
                        textAlign: 'left',
                        textAlignVertical: 'top',
                    }}
                    value={draftDM}
                    onChangeText={text => setDraftDM(text)}
                    />
                    {!sendBtnLoading && <TouchableOpacity style={{
                        width: 50,
                        marginLeft: 10,
                        height: 30,
                        backgroundColor: 'tomato',
                        borderRadius: 10,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                    onPress={() => {
                        setSendBtnLoading(true)
                        sendDM().then(()=>setSendBtnLoading(false))
                    }}
                    >
                        <Text style={{
                            color: 'white',
                            fontWeight: '700'
                        }}>Send</Text>
                    </TouchableOpacity>}
                    {sendBtnLoading && <TouchableOpacity style={{
                        width: 50,
                        marginLeft: 10,
                        height: 30,
                        backgroundColor: 'tomato',
                        borderRadius: 10,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                    onPress={() => {

                    }}
                    >
                        <ActivityIndicator size='small' color='white'/>
                        
                    </TouchableOpacity>}


                        
                </View>}
              <NotiModal
                testID={"empty-message"}
                text={"請輸入訊息內容"}
                buttonTestId={"empty-message"}
                modalVisible={emptyDMModal}
                toggleModal={() => setEmptyDMModal(false)}
              />
              
              </View>
              </KeyboardAvoidingView>
            </Modal>

    )
}

const styles = StyleSheet.create({
    content: {
        backgroundColor: 'white',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderRadius: 15,
        borderColor: 'rgba(0, 0, 0, 0.1)',
      },
      dmsContainer:{
        borderRadius: 10,
        borderColor: 'grey',
        borderWidth: 0.5,
      },
      contentTitle: {
        fontSize: 20,
        marginBottom: 12,
        textAlign: 'center',
        lineHeight: 30,
      },
      buttonGroup: {
        // alignItems: 'flex-start',
        // padding: 5,
        // width: '100%',
        // justifyContent: 'space-around',
        // flexDirection: 'row',
        // flexWrap: 'wrap',
      },
})