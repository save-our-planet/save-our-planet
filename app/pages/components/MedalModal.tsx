import React from "react";
import {View, Text, StyleSheet, Image, Dimensions} from "react-native";
import Modal from "react-native-modal";
// import CopperMedalSVG from "../../dev/photos/svg/medalcopper.svg";
// import SilverMedalSVG from "../../dev/photos/svg/medalSilver.svg";
// import GoldMedalSVG from "../../dev/photos/svg/medalGold.svg";

const images: any = {
    Bottle: require("../../dev/photos/medals/Bottle.png"),
    BringYourOwnCup: require("../../dev/photos/medals/BringYourOwnCup.png"),
    BringYourOwnCutlery: require("../../dev/photos/medals/BringYourOwnCutlery.png"),
    Electric: require("../../dev/photos/medals/Electric.png"),
    First_login_award: require("../../dev/photos/medals/First_login_award.png"),
    GoVegeterian: require("../../dev/photos/medals/Go_vegeterian.png"),
    Grocery: require("../../dev/photos/medals/Grocery.png"),
    LunchBox: require("../../dev/photos/medals/LunchBox.png"),
    PlasticCutlery: require("../../dev/photos/medals/PlasticCutlery.png"),
    Recycle: require("../../dev/photos/medals/Recycle.png"),
    RecycleBag: require("../../dev/photos/medals/RecycleBag.png"),
    ShareToFriend: require("../../dev/photos/medals/ShareToFriend.png"),
    Straw: require("../../dev/photos/medals/Straw.png"),
    Transport: require("../../dev/photos/medals/Transport.png"),
    TurnOffLight1hour: require("../../dev/photos/medals/TurnOffLight1hour.png"),
};

export function MedalModal(props: {
    isModalVisible: boolean;
    onBackdropPress: () => void;
    medalType: string;
    imgName: string;
}) {
    function checkMedal() {
        let width = 200;
        let height = 200;
        switch (props.medalType) {
            case "獲得銅牌！":
                return (
                    // <CopperMedalSVG style={styles.bigTrophy} width={200} height={200} fill="#000" />
                    <View style={styles.trophyBronzeBackground}>
                        <Image
                            style={{height: height, width: width}}
                            source={images[props.imgName]}
                        />
                    </View>
                );
            case "獲得銀牌！":
                return (
                    // <SilverMedalSVG style={styles.bigTrophy} width={200} height={200} fill="#000" />
                    <View style={styles.trophySilverBackground}>
                        <Image
                            style={{height: height, width: width}}
                            source={images[props.imgName]}
                        />
                    </View>
                );
            case "獲得金牌！":
                return (
                    // <GoldMedalSVG style={styles.bigTrophy} width={200} height={200} fill="#000" />
                    <View style={styles.trophyGoldBackground}>
                        <Image
                            style={{height: height, width: width}}
                            source={images[props.imgName]}
                        />
                    </View>
                );
        }
    }

    function checkMedalWord() {
        switch (props.medalType) {
            case "獲得銅牌！":
                return "#cd7f32";
            case "獲得銀牌！":
                return "white";
            case "獲得金牌！":
                return "gold";
        }
    }
    return (
        <>
            <View style={{flex: 1}}>
                <Modal
                    testID={"copper-modal"}
                    isVisible={props.isModalVisible}
                    backdropColor="#aaa"
                    backdropOpacity={0.95}
                    animationIn="zoomIn"
                    animationOut="zoomOut"
                    animationInTiming={800}
                    animationOutTiming={800}
                    backdropTransitionInTiming={400}
                    backdropTransitionOutTiming={800}
                    onBackdropPress={() => props.onBackdropPress()}>
                    <View style={styles.medalModalContent}>
                        {/* <CopperMedalSVG style={styles.bigTrophy} width={200} height={200} fill="#000" /> */}
                        {checkMedal()}
                        <View>
                            <Text
                                testID={"close-button"}
                                // onPress={toggleCopperModalClose}
                                style={[styles.getMedalWord, {color: checkMedalWord()}]}
                                // color="white"
                            >
                                {props.medalType}
                            </Text>
                        </View>
                    </View>
                </Modal>
            </View>
        </>
    );
}
let borderRadius = 99;
let styleHeight = 190;
let styleWidth = 190;
let stylePadding = 20;
const styles = StyleSheet.create({
    bigTrophy: {
        margin: 10,
    },

    medalModalContent: {
        // backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 15,
        borderColor: "rgba(0, 0, 0, 0.1)",
    },

    getMedalWord: {
        fontSize: 50,
        // color: "white",
    },
    trophyBronzeBackground: {
        width: styleWidth,
        height: styleHeight,
        borderRadius: borderRadius,
        paddingTop: stylePadding,
        backgroundColor: "#c27e3d",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: (Dimensions.get("window").width - 90 * 3) / (4 * 2),
    },
    trophySilverBackground: {
        width: styleWidth,
        height: styleHeight,
        borderRadius: borderRadius,
        paddingTop: stylePadding,
        backgroundColor: "#C0C0C0",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: (Dimensions.get("window").width - 90 * 3) / (4 * 2),
    },
    trophyGoldBackground: {
        width: styleWidth,
        height: styleHeight,
        borderRadius: borderRadius,
        paddingTop: stylePadding,
        backgroundColor: "#FFD700",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: (Dimensions.get("window").width - 90 * 3) / (4 * 2),
    },
});
