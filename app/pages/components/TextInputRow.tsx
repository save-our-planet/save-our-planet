import React from "react";
import { View, Text, StyleSheet, useWindowDimensions } from "react-native";
import { TextInput } from "react-native-gesture-handler";


export function TextInputRow(props:{
    label: string,
    onChangeText: (text: any) => void,
    placeholder: string,
    value: string
}){ 
    const windowWidth = useWindowDimensions().width;
    
    return (
    <View style={[{ width: windowWidth * 0.7 },styles.inputRow]}>
        <View style={styles.label}>
            <Text style={styles.labelText}>
                {props.label}
            </Text>
        </View>
        <TextInput 
        style={styles.textInput}
        onChangeText={props.onChangeText}
        value={props.value}
        // placeholder={props.placeholder}
        />
    </View>
)}

const styles = StyleSheet.create({
    inputRow: {
        // display: 'flex',
        // flexDirection: 'column',
        flex: 1,
        margin: 10,
        
    },
    label: {
        flex: 1,
        display:'flex',
        justifyContent: 'center',
        marginTop: 6,
    },
    labelText:{
        color: '#888888'
    },
    textInput:{
        // flex: 3,
        borderColor: '#cccccc',
        marginTop: 8,
        // borderRadius: 10,
        borderBottomWidth: 0.5,
        textAlign: 'left'
    }
});