import React from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';

import BlankBigStar from '../../dev/photos/svg/blankBigStar.svg';
import FullBigStar from '../../dev/photos/svg/fullBigStar.svg';

export function BigStar(props: {
  level: number;
  starId: number;
  requiredStar: number
}) {
  const level = props.level;
  return (
  
    <>
      {level  >= props.starId * (props.requiredStar / 3)  ? (
        <FullBigStar
          style={styles.bigStar}
          width={50}
          height={50}
          fill="#000"
        />
      ) : (
        <BlankBigStar
          style={styles.bigStar}
          width={50}
          height={50}
          fill="#000"
        />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  bigStar: {
    margin: 10,
  },
});
