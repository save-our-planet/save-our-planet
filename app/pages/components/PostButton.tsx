import React from "react";
import { StyleSheet, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Icon, { IconName } from "react-native-ionicons";

export function PostButton(props: {
    label: string,
    ioniconName?: IconName,
    iconColor?: string,
    onPress: () => void,
}){
    return(
        <TouchableOpacity
            style={styles.button}
            onPress={props.onPress}>
            {props.ioniconName && <Icon style={[{color: props.iconColor}, styles.buttonIcon]} name={props.ioniconName} />}
            <Text style={styles.labelText}> {props.label}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    labelText:{
        color: '#888888'
    },
    button:{
        flexDirection: 'row',
        alignItems: 'center',
        margin: 10,
    },
    buttonIcon:{
    }
});