import React, { useState } from "react";
import { Button, KeyboardAvoidingView, StyleSheet, Text, useWindowDimensions, View } from "react-native";
import { TextInput, TouchableOpacity } from "react-native-gesture-handler";
import { PostButton } from "./PostButton";

export function PostContent(props:{
    value: string,
    onChangeText: (text: any) => void,
    onClear: () => void,
    changeSavedStatus: () => void,
    saved: boolean
}){
    const dimensions = useWindowDimensions()

    return(
        <View style={[{ width: dimensions.width * 0.8 },styles.container]}>
            <Text style={{color: '#888'}}>文章內容</Text>
            {!props.saved && 
            <TextInput 
                multiline={true}
                style={[{ width: dimensions.width * 0.6 },styles.textArea]}
                onChangeText={props.onChangeText}
                value={props.value}
                />}

            {props.saved && 
            <Text style={[styles.textArea, { width: dimensions.width * 0.6, borderWidth: 0}]}>
                {props.value}
            </Text>}
            <View style={{
                flexDirection: 'row'
            }}>

                <PostButton
                    label='清除'
                    // ioniconName='trash-outline'
                    // iconColor='purple'
                    onPress={props.onClear}
                    />
                {!props.saved && 
                <PostButton
                    label='儲存'
                    // ioniconName='save-outline'
                    // iconColor='gold'
                    onPress={props.changeSavedStatus}
                    />}
                
                {props.saved && 
                <PostButton
                    label='編輯'
                    // ioniconName='create-outline'
                    // iconColor='gold'
                    onPress={props.changeSavedStatus}
                    />}
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        backgroundColor: 'white',
        alignItems: 'center',
        padding: 10,
        borderRadius: 20,
    },
    textArea: {
        marginVertical: 10,
        padding: 10,
        height: 200,
        borderColor: '#ccc',
        borderRadius: 10,
        borderWidth: 0.5,
        textAlign: 'left',
        textAlignVertical: 'top',
    }
})