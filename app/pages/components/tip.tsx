import React, { useEffect, useState } from "react";
import { Image, Text, useWindowDimensions, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import SwiperFlatList from "react-native-swiper-flatlist";
import { useDispatch } from "react-redux";
import { REACT_APP_BACKEND_URL } from "../../env";
import { beginLoading, doneLoading } from "../../redux/nav/action";

export interface ITip {
    id: number,
    category: string,
    classification: string,
    characteristic: string,
    item: string,
    recycle_point: string,
    remark?: string,
}

export function TipContainer(props: {
    tipItem: ITip
}){
    const dispatch = useDispatch();
    const [photos, setPhotos] = useState([])
    // fetch image(s) (with tipItem.id) HERE
    // const photos = [`${REACT_APP_BACKEND_URL}/uploads-1613035884070.jpeg`, `${REACT_APP_BACKEND_URL}/uploads-1613035884067.jpeg`]

    async function loadPhotos(){
        let res = await fetch(`${REACT_APP_BACKEND_URL}/tips-photos/${props.tipItem.id}`)
        let result = await res.json();
        setPhotos(result)
    }

    useEffect(()=>{
        dispatch(beginLoading());
        loadPhotos().then(()=>dispatch(doneLoading()))
    }, [])

    const dimensions = useWindowDimensions()
    const [expanded, setExpanded] = useState<boolean>(false)

    return (
        <>
            <TouchableOpacity 
            onPress={() => setExpanded(!expanded)}
            style={{
                marginVertical: 10,
                paddingVertical: 10,
                backgroundColor: 'white',
                shadowColor: 'grey',
                shadowOpacity: 0.5,
                shadowOffset: {
                    width: 0,
                    height: 0,
                },
                shadowRadius: 7,
                borderRadius: 10,
                width: dimensions.width * 0.8
            }}>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingHorizontal: 20,
                    alignItems: 'center'
                }}>

                <Text 
                
                style={{
                    textAlign: 'center',
                    fontSize: 18,
                    fontWeight: '500'
                }}>
                    {props.tipItem.classification}
                </Text>
                <TouchableOpacity
                    style={{
                        width: 30,
                        height: 30,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                    onPress={() => setExpanded(!expanded)}
                >
                    <Text style={{
                        fontSize: 20
                    }}>
                        {expanded && "-"}
                        {!expanded && "+"}
                    </Text>
                </TouchableOpacity>


                </View>

                {expanded &&
                <View style={{
                    marginTop: 10,
                    marginBottom: 5,
                }}>
                    <View>
                        <View style={{
                            flexDirection: 'row',
                            width: dimensions.width * 0.8,
                            alignItems: 'flex-start',
                            justifyContent: 'center',
                            marginVertical: 10,
                        }}>
                            <Text style={{
                                lineHeight: 20,
                                width: dimensions.width * 0.15,
                                fontWeight: '500'
                            }}>
                                特性：
                            </Text>
                            <Text 
                            style={{
                                lineHeight: 20,
                                width: dimensions.width * 0.6
                            }}>
                                {props.tipItem.characteristic.replace(/\s/g, "")}
                            </Text>
                        </View>
                            
                        <View style={{
                            flexDirection: 'row',
                            width: dimensions.width * 0.8,
                            alignItems: 'flex-start',
                            justifyContent: 'center',
                            marginVertical: 10,
                        }}>
                            <Text style={{
                                lineHeight: 20,
                                width: dimensions.width * 0.15,
                                fontWeight: '500'
                            }}>
                                例子：
                            </Text>
                            <Text
                            style={{
                                lineHeight: 20,
                                width: dimensions.width * 0.6
                            }}>
                                {props.tipItem.item}
                            </Text>
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            width: dimensions.width * 0.8,
                            alignItems: 'flex-start',
                            justifyContent: 'center',
                            marginVertical: 10,
                        }}>
                            <Text style={{
                                lineHeight: 20,
                                width: dimensions.width * 0.15,
                                fontWeight: '500'
                            }}>
                                回收點：
                            </Text>
                            <Text
                            style={{
                                lineHeight: 20,
                                width: dimensions.width * 0.6
                            }}>
                                {props.tipItem.recycle_point}
                            </Text>
                        </View>

                        {props.tipItem.remark && <View style={{
                            flexDirection: 'row',
                            width: dimensions.width * 0.8,
                            alignItems: 'flex-start',
                            justifyContent: 'center',
                            marginVertical: 10,
                        }}>
                            <Text style={{
                                lineHeight: 20,
                                width: dimensions.width * 0.15,
                                fontWeight: '500'
                            }}>
                                備註：
                            </Text>
                            <Text
                            style={{
                                lineHeight: 20,
                                width: dimensions.width * 0.6
                            }}>
                                {props.tipItem.remark}
                            </Text>
                        </View>}

                    </View>
                
                <SwiperFlatList
                style={{
                    width: dimensions.width * 0.8
                }}
                renderAll={true}
                autoplay={false}
                autoplayLoop={false}
                // autoplayDelay={4}
                // index={0}
                // autoplayLoopKeepAnimation
                showPagination={true}
                paginationStyleItem={{
                    borderWidth: 2,
                    borderRadius: 10,
                    borderColor: '#DCEBD8',
                    width: 12,
                    height: 12,
                }}
                paginationActiveColor={'black'}
                paginationDefaultColor={'#999'}
                // disableGesture
                data={photos}
                renderItem={photo => (
                    <View style={{height: 200, width: dimensions.width * 0.8}}>
                        <Image style={{
                            height: 200, 
                            width: dimensions.width * 0.8, 
                            backgroundColor: 'white', 
                            resizeMode: 'stretch',
                            // alignItems: 'center',
                            }}
                            source={{uri: `${REACT_APP_BACKEND_URL}/${photo.item.path}`}}/>
                    </View>
                )}/>
                </View>

                }

            </TouchableOpacity>
        </>
    )
}