import React, { useState } from "react";
import { View, Text, Button, StyleSheet } from "react-native";
import Modal from 'react-native-modal';

export function NotiModal(props: {
    testID: string,
    text: string,
    buttonTestId: string,
    modalVisible: boolean,
    toggleModal: () => void
}) {

    return (
        <View style={{flex: 1}}>
            <Modal
              testID={props.testID}
              isVisible={props.modalVisible}
              backdropColor="#aaa"
              backdropOpacity={0.8}
              animationIn="zoomIn"
              animationOut="zoomOut"
              animationInTiming={250}
              animationOutTiming={250}
              backdropTransitionInTiming={300}
              backdropTransitionOutTiming={500}>
              <View style={styles.content}>
                <Text style={styles.contentTitle}>
                  {props.text}
                </Text>
                <Text></Text>
                <View style={styles.buttonGroup}>
                  <Button
                    testID={props.buttonTestId}
                    onPress={props.toggleModal}
                    title="關閉"
                  />
                </View>
              </View>
            </Modal>
          </View>
    )
} 

const styles = StyleSheet.create({
    content: {
      backgroundColor: 'white',
      padding: 22,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 15,
      borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    contentTitle: {
      fontSize: 20,
      marginBottom: 12,
      textAlign: 'center',
      lineHeight: 30,
    },
    buttonGroup: {
      alignItems: 'flex-start',
      padding: 5,
      width: '100%',
      justifyContent: 'space-around',
      flexDirection: 'row',
      flexWrap: 'wrap',
    },
  });
  