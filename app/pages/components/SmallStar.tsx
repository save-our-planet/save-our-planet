import React from "react";
import {ScrollView, StyleSheet, View} from "react-native";
import {Animated} from "react-native";
import {TouchableWithoutFeedback} from "react-native";
import {Easing} from "react-native";
import {useNavigation} from "@react-navigation/native";
import BlankSmallStar from "../../dev/photos/svg/blankSmallStar.svg";
import FullSmallStar from "../../dev/photos/svg/fullSmallStar.svg";

export function SmallStar(props: {
    level: number;
    starId: number;
    requiredStar: number;
    onClick: () => void;
}) {
    let scaleValue = new Animated.Value(0); // declare an animated value
    const cardScale = scaleValue.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [1, 0.8, 0.7],
    });
    let transformStyle = {
        transform: [{scale: cardScale}],
    };
    const level = props.level;
    return (
        <>
            {level % (props.requiredStar / 3) >= props.starId &&
            level % (props.requiredStar / 3) !== 0 ? (
                <FullSmallStar style={styles.bigStar} width={30} height={30} fill="#000" />
            ) : (
                <TouchableWithoutFeedback
                    key={props.starId}
                    onPressIn={() => {
                        scaleValue.setValue(0);
                        Animated.timing(scaleValue, {
                            toValue: 1,
                            duration: 250,
                            easing: Easing.linear,
                            useNativeDriver: true,
                        }).start();
                        // console.log('starID: ', props.starId);
                    }}
                    onPressOut={() => {
                        Animated.timing(scaleValue, {
                            toValue: 0,
                            duration: 100,
                            easing: Easing.linear,
                            useNativeDriver: true,
                        }).start();
                        props.onClick();
                        // console.log('smallStarLevel: ', props.level);

                    }}>
                    <Animated.View style={transformStyle}>
                        <BlankSmallStar style={styles.bigStar} width={30} height={30} fill="#aaa" />
                    </Animated.View>
                </TouchableWithoutFeedback>
            )}
        </>
    );
}

const styles = StyleSheet.create({
    bigStar: {
        margin: 10,
        flex:1,
    },
});
