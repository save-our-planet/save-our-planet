import React, {useEffect, useState} from "react";
import {Dimensions, Image, ScrollView, StyleSheet, View} from "react-native";
import {Animated} from "react-native";
import {TouchableWithoutFeedback} from "react-native";
import {Easing} from "react-native";
import {useNavigation} from "@react-navigation/native";
export function Medal(props: {medal: any}) {
    const navigation = useNavigation();
    let scaleValue = new Animated.Value(0); // declare an animated value
    const cardScale = scaleValue.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [1, 0.8, 0.7],
    });
    let transformStyle = {
        transform: [{scale: cardScale}],
    };
    let level = props.medal.level;
    let imgName = props.medal.ImgName;
    // console.log("Medal.tsx level: ", level);
    const images: any = {
        Bottle: require("../../dev/photos/medals/Bottle.png"),
        BringYourOwnCup: require("../../dev/photos/medals/BringYourOwnCup.png"),
        BringYourOwnCutlery: require("../../dev/photos/medals/BringYourOwnCutlery.png"),
        Electric: require("../../dev/photos/medals/Electric.png"),
        First_login_award: require("../../dev/photos/medals/First_login_award.png"),
        GoVegeterian: require("../../dev/photos/medals/Go_vegeterian.png"),
        Grocery: require("../../dev/photos/medals/Grocery.png"),
        LunchBox: require("../../dev/photos/medals/LunchBox.png"),
        PlasticCutlery: require("../../dev/photos/medals/PlasticCutlery.png"),
        Recycle: require("../../dev/photos/medals/Recycle.png"),
        RecycleBag: require("../../dev/photos/medals/RecycleBag.png"),
        ShareToFriend: require("../../dev/photos/medals/ShareToFriend.png"),
        Straw: require("../../dev/photos/medals/Straw.png"),
        Transport: require("../../dev/photos/medals/Transport.png"),
        TurnOffLight1hour: require("../../dev/photos/medals/TurnOffLight1hour.png"),
    };

    function determineMedal() {
        let width = 90;
        let height = 90;
        if (level < 5) {
            return (
                <>
                    <View style={styles.trophyNormalBackground}>
                        <Image
                            style={{height: height, width: width}}
                            source={images[imgName]}
                        />
                    </View>
                </>
            );
        } else if (level >= 5 && level < 10) {
            return (
                <View style={styles.trophyBronzeBackground}>
                    <Image
                        style={{height: height, width: width}}
                        source={images[imgName]}
                    />
                </View>
            );
        } else if (level >= 10 && level < 15) {
            return (
                <View style={styles.trophySilverBackground}>
                    <Image
                        style={{height: height, width: width}}
                        source={images[imgName]}
                    />
                </View>
            );
        } else if (level >= 15) {
            return (
                <View style={styles.trophyGoldBackground}>
                    <Image
                        style={{height: height, width: width}}
                        source={images[imgName]}
                    />
                </View>
            );
        }
    }

    useEffect(() => {
        determineMedal()

    }, []);

    return (
        <TouchableWithoutFeedback
            key={props.medal.id}
            onPressIn={() => {
                scaleValue.setValue(0);
                Animated.timing(scaleValue, {
                    toValue: 1,
                    duration: 250,
                    easing: Easing.linear,
                    useNativeDriver: true,
                }).start();
                // console.log('medal' + props.medal.description);
            }}
            onPressOut={() => {
                Animated.timing(scaleValue, {
                    toValue: 0,
                    duration: 100,
                    easing: Easing.linear,
                    useNativeDriver: true,
                }).start();
                // console.log(props.medal);

                
            }}
            onPress={()=>{
                navigation.navigate("MedalGoal", props.medal);
            }}>
            <Animated.View style={transformStyle}>{determineMedal()}</Animated.View>
        </TouchableWithoutFeedback>
    );
}
const paddingStyle = 10
const styles = StyleSheet.create({
    // myTrophy: {
    //     marginBottom: 30,
    // },
    trophyNormalBackground: {
        width: 80,
        height: 80,
        borderRadius: 90,
        paddingTop: paddingStyle,
        backgroundColor: "#000000",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: (Dimensions.get("window").width - 90 * 3) / (4 * 2),
    },
    trophyBronzeBackground: {
        width: 80,
        height: 80,
        borderRadius: 90,
        paddingTop: paddingStyle,
        backgroundColor: "#c27e3d",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: (Dimensions.get("window").width - 90 * 3) / (4 * 2),
    },
    trophySilverBackground: {
        width: 80,
        height: 80,
        borderRadius: 90,
        paddingTop: paddingStyle,
        backgroundColor: "#C0C0C0",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: (Dimensions.get("window").width - 90 * 3) / (4 * 2),
    },
    trophyGoldBackground: {
        width: 80,
        height: 80,
        borderRadius: 90,
        paddingTop: paddingStyle,
        backgroundColor: "#FFD700",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        margin: (Dimensions.get("window").width - 90 * 3) / (4 * 2),
    },
});
