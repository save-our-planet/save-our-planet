import React, { useEffect } from "react";
import { useState } from "react";
import { View, Text, Image, Button, StyleSheet, Dimensions, useWindowDimensions, Animated, TouchableWithoutFeedback, Easing } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import SwiperFlatList from "react-native-swiper-flatlist";
import { IPost } from '../Posts'
import BookmarkSVG from '../../dev/photos/svg/heart(1).svg';
import GreyBookmarkSVG from '../../dev/photos/svg/heart(2).svg';
import Icon from "react-native-ionicons";
import { useDispatch, useSelector, useStore } from "react-redux";
import { RootState } from "../../store";
import { loadPosts } from "../../redux/posts/action";
import { forModalPresentationIOS } from "@react-navigation/stack/lib/typescript/src/TransitionConfigs/CardStyleInterpolators";
import { REACT_APP_BACKEND_URL, REACT_APP_S3 } from "../../env";

export default function Post(props: {
    ipost: IPost,
    index: number,
    isAdmin?: boolean,
    setRefresh: () => void
    // photos: any
}){
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    const [isHidden, setIsHidden] = useState<boolean>(props.ipost.is_hidden)
    const [isSaved, setIsSaved] = useState<boolean>(false)
    const windowWidth = useWindowDimensions().width;
    const [expanded, setExpanded] = useState(false);
    const dimensions = useWindowDimensions()
    const posts = useSelector((state: RootState) => state.posts.posts)
    const state = useStore().getState()
    const dispatch = useDispatch()

    let scaleValue = new Animated.Value(0); // declare an animated value
    const cardScale = scaleValue.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [1, 0.8, 0.7],
    });
    let transformStyle = {
        transform: [{scale: cardScale}],
    };

    async function checkSavedStatus(){
        let res = await fetch(`${REACT_APP_BACKEND_URL}/save-post/${props.ipost.id}`, {
            headers: {
              Authorization: 'Bearer ' + state.auth.token,
            }
        });
        let isSaved = await res.json();
        
        setIsSaved(isSaved.result)
        
    }

    async function savePost(){
        let res = await fetch(`${REACT_APP_BACKEND_URL}/save-post/${props.ipost.id}`, {
            method: "POST",
            headers: {
                Authorization: 'Bearer ' + state.auth.token,
            }
        })
        let isSaved = await res.json();
        props.setRefresh();
        setIsSaved(isSaved.result)
        dispatch(loadPosts())
    }

    async function deletePost(){
        let res = await fetch(`${REACT_APP_BACKEND_URL}/post/${props.ipost.id}`, {
            method: 'DELETE',
            headers: {
                Authorization: 'Bearer ' + state.auth.token,
            }
        })
        let isDeleted = await res.json();
        props.setRefresh();
        setIsHidden(!isHidden);
        dispatch(loadPosts());

        let formData = new FormData();
        formData.append('isAdmin', true);
        formData.append('userID', props.ipost.user_id)
        formData.append('content', `你的文章'${props.ipost.title}'已被刪除`)

        let dmRes = await fetch(`${REACT_APP_BACKEND_URL}/direct-message`, {
            method: 'POST',
            body: formData,
            headers: {
                Authorization: 'Bearer ' + state.auth.token,
            }
        })
        let result = await dmRes.json()
        
    }

    async function putBackPost(){
        let res = await fetch(`${REACT_APP_BACKEND_URL}/backPost/${props.ipost.id}`, {
            method: 'PUT',
            headers: {
                Authorization: 'Bearer ' + state.auth.token,
            }
        })
        let isPutBack = await res.json();
        props.setRefresh();
        setIsHidden(!isHidden)
        dispatch(loadPosts())

        let formData = new FormData();
        formData.append('isAdmin', true);
        formData.append('userID', props.ipost.user_id)
        formData.append('content', `你的文章'${props.ipost.title}'已被復活`)

        let dmRes = await fetch(`${REACT_APP_BACKEND_URL}/direct-message`, {
            method: 'POST',
            body: formData,
            headers: {
                Authorization: 'Bearer ' + state.auth.token,
            }
        })
        let result = await dmRes.json()
        
    }

    useEffect(()=>{
        checkSavedStatus()
    }, [posts])

    return (
    <View key={props.index} style={[
        (props.ipost.is_hidden && !props.isAdmin) && {display: 'none'},
        { width: windowWidth * 0.8 },
        styles.postContainer]}>
            <View style={{
                position: 'absolute',
                right: 0,
                left: 0,
                top: 0,
                bottom: 0,
                padding: 10,
                flexDirection: 'row',
                justifyContent: 'space-between'
            }}>
                <View>
                    <TouchableWithoutFeedback
                    key={props.index}
                    onPressIn={() => {
                        scaleValue.setValue(0);
                        Animated.timing(scaleValue, {
                        toValue: 1,
                        duration: 250,
                        easing: Easing.linear,
                        useNativeDriver: true,
                        }).start();
                        
                    }}
                    onPressOut={() => {
                        Animated.timing(scaleValue, {
                        toValue: 0,
                        duration: 100,
                        easing: Easing.linear,
                        useNativeDriver: true,
                        }).start();

                        savePost();
                    }}>
                    <Animated.View style={transformStyle}>
                        {isAuthenticated &&!isSaved && <GreyBookmarkSVG style={styles.bookmark} />}
                        {isAuthenticated && isSaved && <BookmarkSVG style={styles.bookmark} />}
                    </Animated.View>
                </TouchableWithoutFeedback>
                </View>
            
            {(props.isAdmin && !isHidden) && 
            <TouchableOpacity
                onPress={() => deletePost()}>
                <Icon name="trash-outline" />
            </TouchableOpacity>}

            {(props.isAdmin && isHidden) && 
            <TouchableOpacity
            onPress={() => putBackPost()}>
                <Icon name="arrow-undo-circle-outline" />
            </TouchableOpacity>
            }

            </View>
        <View style={{padding: 8, minHeight: 60, justifyContent:'center'}}>
            <Text style={[
                (isHidden && props.isAdmin) && {color: 'grey', textDecorationLine: 'line-through'},
                {
                margin: 3,
                fontSize: 20,
                maxWidth: dimensions.width * 0.50,
                fontWeight: '700',
                textAlign: 'justify',
            }]}>
                {props.ipost.title}
            </Text>

        </View>


        {props.ipost.photos && <SwiperFlatList
        autoplay={false}
        autoplayDelay={2}
        autoplayLoop={false}
        index={0}
        showPagination={false}
        data={props.ipost.photos}
        renderItem={({ item }) => (
            <View style={{height: 200, width: dimensions.width * 0.8}}>
                <Image style={{height: 200, width: dimensions.width * 0.8}} source={{uri: `${REACT_APP_S3}/${item.path}`}}/>
            </View>
        )}/>}

        {!expanded &&
            <View style={{
                width: '100%',
                padding: 3,
                alignItems: 'flex-end'
            }}>
                <Button title='展開' onPress={() => setExpanded(!expanded)} />

            </View>
        }

        {expanded && 
            <View style={{padding: 10, width: '100%'}}>
                <TouchableOpacity 
                onPress={() => setExpanded(!expanded)}>

                    <Text style={{fontWeight: '800', marginVertical: 5}}>{props.ipost.shop_name}</Text>
                    <Text style={{fontWeight: '800', marginVertical: 5}}>{props.ipost.shop_address}</Text>
                    <Text>
                        {props.ipost.content}
                    </Text>

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'flex-end'
                        }}>
                        <View>
                            <Text style={{fontWeight: '800', marginVertical: 5}}>{props.ipost.username}</Text>
                            <Text>{props.ipost.created_at}</Text>
                        </View>
                            <View style={{
                                // borderRadius: 10,
                                // backgroundColor: '#ccc'
                            }}>
                                <Text style={{color: 'grey', padding: 10, fontSize: 16, fontWeight:'800'}}>#{props.ipost.eco_type}</Text>
                            </View>
                    </View>
                        
                </TouchableOpacity>
            </View>
        }
    </View>)
}

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 200,
        resizeMode: 'cover',
    },
    postContainer: {
        backgroundColor: 'white',
        marginVertical: 10,
        marginHorizontal: 30,
        alignItems: 'center',
        borderRadius: 20,
        shadowColor: 'grey',
        shadowOpacity: 0.5,
        shadowOffset: {
            width: 5,
            height: 5,
        },
    },
    bookmark:{
        width: 40,
        height: 40,
    }
  });