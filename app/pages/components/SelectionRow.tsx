import { Picker } from "@react-native-picker/picker";
import React from "react";
import { View, Text, StyleSheet, StyleProp, ViewStyle } from "react-native";

export function SelectionRow(props: {
    style?: StyleProp<ViewStyle>,
    label: string,
    value: string | number | undefined,
    useItemAsValue?: boolean | false,
    onValueChange: (itemValue: any) => void,
    list: Array<any>
        // <{name: string, id: string | number, created_at?:string , updated_at?: string}>
}){
    
    return(
        <View style={[props.style, styles.pickerRow]}>
            <View style={styles.label}>
                <Text style={styles.labelText}>
                    {props.label}
                </Text>
            </View>
            <Picker
            selectedValue={props.value}
            style={styles.picker}
            itemStyle={styles.pickerText}
            onValueChange={props.onValueChange}>
                {props.list.map((d, i) => (
                    <Picker.Item key={i} label={props.useItemAsValue? d : d.name} value={props.useItemAsValue? d : d.id} />
                ))}
            </Picker>
        </View>
    )
}

const styles = StyleSheet.create({
    label: {
        flex: 1,
        display:'flex',
        justifyContent: 'center',
        marginTop: 6,
    },
    labelText:{
        textAlign: 'center',
        color: '#888888'
    },
    picker:{
        padding: 0,
        backgroundColor: '#00000010',
        borderRadius: 10,
        margin: 10
    },
    pickerText:{
        fontSize: 16
    },
    pickerRow:{
        flex: 1,
    },
});