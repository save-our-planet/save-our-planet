import { Picker } from '@react-native-picker/picker';
import React, { useCallback, useEffect, useState } from 'react';
import { Dimensions, Image, ImageSourcePropType, ListViewBase, SegmentedControlIOSComponent } from 'react-native';
import { useWindowDimensions } from 'react-native';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextInputRow } from './components/TextInputRow';
import { SelectionRow } from './components/SelectionRow';
import { PostButton } from './components/PostButton';
import ImagePicker from 'react-native-image-crop-picker';
import { PostContent } from './components/PostContent';
import SwiperFlatList from 'react-native-swiper-flatlist';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector, useStore } from 'react-redux';
import Icon from 'react-native-ionicons';
import { loadPosts } from '../redux/posts/action';
import { NotiModal } from './components/NotiModal';
import { REACT_APP_BACKEND_URL } from "../env";
import { beginLoading, doneLoading } from '../redux/nav/action';
import { Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { RootState } from '../store';

export function CreatePost(){
    const dimensions = useWindowDimensions();
    const navigation = useNavigation();
    const [postTitle, setPostTitle] = useState<string>('');
    const [shopName, setShopName] = useState<string>('');
    const [shopAddress, setShopAddress] = useState<string>('');
    const [shopDistrict, setShopDistrict] = useState<string>('');
    const [shopEcoType, setShopEcoType] = useState<string>('');
    const [content, setContent] = useState<string>('');
    const [photos, setPhotos] = useState<any>([]);
    const [savedContent, setSavedContent] = useState<boolean>(false);
    const [districts, setDistricts] = useState<{name: string, id: string}[]>([]);
    const [ecoTypes, setEcoTypes] = useState<{name: string, id: string}[]>([]);
    const [showModal, setShowModal] = useState<boolean>(false);

    const [emptyTitle, setEmptyTitle] = useState<boolean>(false);
    const [noPhotos, setNoPhotos] = useState<boolean>(false);
    const [emptyShopName, setEmptyShopName] = useState<boolean>(false);
    const [emptyAddress, setEmptyAddress] = useState<boolean>(false);

    const dispatch = useDispatch();
    const state = useStore().getState();

    async function loadDistricts() {
        // console.log(process.env.REACT_APP_BACKEND_URL);
        const res = await fetch(`${REACT_APP_BACKEND_URL}/district`);
        const results = await res.json();
        
        setDistricts(results.data.map((el: any) => {
            return {
                name: el.name,
                id: el.id
            }}))
        
        setShopDistrict(results.data[0].id)
    }

    async function loadEcoTypes() {
        const res = await fetch(`${REACT_APP_BACKEND_URL}/ecoTypes`);
        const results = await res.json();
        setEcoTypes(results.map((el: any) => {
            return {
                name: el.name,
                id: el.id
            }}))
        setShopEcoType(results[0].id)
    }

    async function submitPost(){
        if (postTitle.replace(/\s/g, '') == ''){
            setEmptyTitle(true);
            return;
        } else if (shopName.replace(/\s/g, '') == '') {
            setEmptyShopName(true);
            return;
        } else if (shopAddress.replace(/\s/g, '') == '') {
            setEmptyAddress(true);
            return;
        } else if (photos.length == 0) {
            setNoPhotos(true);
            return;
        }

        const shopData = new FormData();
        shopData.append('name', shopName);
        shopData.append('address', shopAddress);
        shopData.append('districtID', shopDistrict);
        const shopRes = await fetch(`${REACT_APP_BACKEND_URL}/shop`,{
            method: 'POST',
            body: shopData,
            headers: {
                Authorization: 'Bearer ' + state.auth.token,
            }
        });
        const shopID = (await shopRes.json()).data[0].id;
        console.log(shopID);
        
        const postData = new FormData();
        postData.append('title', postTitle);
        postData.append('content', content);
        postData.append('ecoTypeID', shopEcoType);
        postData.append('shopID', shopID);
        const postRes = await fetch(`${REACT_APP_BACKEND_URL}/post`, {
            method: 'POST',
            body: postData,
            headers: {
                Authorization: 'Bearer ' + state.auth.token,
            }
        });
        const postID = (await postRes.json()).result;
        console.log('postID: ', postID);
        

        const photosData = new FormData();
        for (let photo of photos){
            photosData.append('uploads', { type:'image/jpg', uri: photo.sourceURL, name: photo.filename});
        }

        const photosRes = await fetch(`${REACT_APP_BACKEND_URL}/post-photos/${postID}`, {
            method: 'POST',
            body: photosData,
            headers: {
                Authorization: 'Bearer ' + state.auth.token,
            }
        });
        const result = await photosRes.json();
        if(result.result){
            navigation.navigate('Posts')
        }
    
        dispatch(loadPosts())
        
    }

    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);

    const isFocused = useIsFocused();
    const [changePage, setChangePage] = useState<boolean>(false);    

    useEffect(()=>{
        if(!isAuthenticated){
            navigation.navigate("Login")
        // } else if (changePage) {
        //     navigation.goBack();
        //     setChangePage(false)
        } else if (!isFocused || changePage){
            navigation.reset({
                index: 0,
                routes: [
                    {name: "Posts"}
                ],

            })
        }
    }, [isFocused, changePage])

    useEffect(()=>{
        loadDistricts();
        loadEcoTypes();
    }, [])

    const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 0

    return(
            
            <KeyboardAwareScrollView
            style={styles.safeAreaView}
            contentInsetAdjustmentBehavior="automatic">
                
                <View style={{flexDirection: 'row'}}>
                    <View style={styles.backButton}>
                        <Icon
                            name={"chevron-back-outline"}
                            onPress={() => {
                                setChangePage(true)
                            }}
                        />
                    </View>
                    <View style={styles.container}>
                        <Text
                            style={{
                            fontSize: 30,
                            fontWeight: 'bold',
                            textAlign: 'center',
                            }}
                            onPress={() => {
                                setChangePage(true)
                            }}>
                            新增文章
                        </Text>
                    </View>
                </View>
                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    }}>
                    {/* <KeyboardAvoidingView style={[{ width: dimensions.width * 0.8 } ,styles.postContainer]} behavior='position' keyboardVerticalOffset={keyboardVerticalOffset}> */}
                    <View style={[{ width: dimensions.width * 0.8 },styles.postContainer]}>
                        <TextInputRow 
                            label='標題'
                            onChangeText={text => setPostTitle(text)}
                            placeholder='Post Title'
                            value={postTitle}/>
                        <TextInputRow 
                            label='商舖名稱'
                            onChangeText={text => setShopName(text)}
                            placeholder='Shop Name'
                            value={shopName}/>
                        <TextInputRow 
                            label='商舖地址'
                            onChangeText={text => setShopAddress(text)}
                            placeholder='Address'
                            value={shopAddress}/>
                        <View style={{
                            flexDirection: 'row',
                            margin: 20,
                        }}>
                            <SelectionRow
                                label='商舖地區'
                                value={shopDistrict}
                                onValueChange={(itemValue: any) => setShopDistrict(itemValue)}
                                list={districts}/>
                            <SelectionRow
                                label='環保分類'
                                value={shopEcoType}
                                onValueChange={(itemValue: any) => setShopEcoType(itemValue)}
                                list={ecoTypes}/>
                        </View>
                        
                    </View>
                    {/* </KeyboardAvoidingView> */}

                    <PostContent 
                    value={content}
                    onChangeText={(text) => setContent(text)}
                    onClear={() => setContent('')}
                    saved={savedContent}
                    changeSavedStatus={() => setSavedContent(!savedContent)}
                    />

                    <View style={styles.postContainer}>
                        <View style={{
                                flexDirection: 'row',
                            }}>
                                <PostButton
                                    label='上傳圖片'
                                    // ioniconName='images-outline'
                                    // iconColor='green'
                                    onPress={() => ImagePicker.openPicker({
                                        mediaType: 'photo',
                                        multiple: true,
                                        maxFiles: 10,
                                    }).then(images => {
                                        setPhotos(images);
                                    })} />
                                {/* <PostButton 
                                    label='拍照'
                                    // ioniconName='camera-outline'
                                    // iconColor='orange'
                                    onPress={() => ImagePicker.openCamera({
                                        mediaType: 'photo',
                                        multiple: true
                                    })}/> */}
                            </View>
                            <View style={{
                            marginTop: 20,
                            justifyContent: 'center',
                            height: 200,
                            width: dimensions.width * 0.8}}>
                                {photos.length == 0 && <Text style={{textAlign:'center'}}>請上傳圖片</Text>}
                                {photos.length > 0 && <SwiperFlatList
                                    autoplay
                                    autoplayDelay={2}
                                    autoplayLoop
                                    index={0}
                                    showPagination={false}
                                    data={photos}
                                    renderItem={({ item }) => (
                                        <View style={{height: 200, width: dimensions.width * 0.8}}>
                                            <Image style={{height: 200, width: dimensions.width * 0.8, resizeMode: 'cover'}} source={{uri: item.sourceURL}}/>
                                        </View>
                                    )}
                                />}
                        </View>
                    </View>
                    <View style={{backgroundColor: 'white', marginVertical: 20, borderRadius: 10}}>
                        <PostButton
                        label='提交'
                        onPress={()=>{
                            dispatch(beginLoading())
                            submitPost().then(()=>dispatch(doneLoading()))
                        }}
                        />

                    </View>

                    <NotiModal
                        testID={"empty-title"}
                        text={`請填寫文章標題`}
                        buttonTestId={"close-button"}
                        modalVisible={emptyTitle}
                        toggleModal={() => setEmptyTitle(false)}
                    />

                    <NotiModal
                        testID={"empty-shop-name"}
                        text={`請填寫商舖名稱`}
                        buttonTestId={"close-button"}
                        modalVisible={emptyShopName}
                        toggleModal={() => setEmptyShopName(false)}
                    />  

                    <NotiModal
                        testID={"empty-address"}
                        text={`請填寫商舖地址`}
                        buttonTestId={"close-button"}
                        modalVisible={emptyAddress}
                        toggleModal={() => setEmptyAddress(false)}
                    />  

                    <NotiModal
                        testID={"no-photos"}
                        text={`請上傳最少一張圖片`}
                        buttonTestId={"close-button"}
                        modalVisible={noPhotos}
                        toggleModal={() => setNoPhotos(false)}
                    />



                    
                </View>
                
            </KeyboardAwareScrollView>
            
    )
}

const styles = StyleSheet.create({
    safeAreaView: {
      backgroundColor: '#DCEBD8',
      
    },
    backButton: {
        marginTop: 10,
        marginLeft: 10,
    },
    container: {
        paddingTop: 10,
        alignItems: 'center',
    },
    postContainer: {
        backgroundColor: 'white',
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
    },
    label: {
        flex: 1,
        display:'flex',
        justifyContent: 'center',
        marginTop: 6,
    },
    labelText:{
        color: '#888888'
    },
    textInput:{
        borderColor: '#cccccc',
        marginTop: 8,
        borderBottomWidth: 0.5,
        textAlign: 'left'
    },
    picker:{
        // flex: 3,
    },
    pickerText:{
        fontSize: 16
    },
    pickerRow:{
        flex: 1
    },
    button:{
        flexDirection: 'row',
        alignItems: 'center',
        margin: 10,
    },
    buttonIcon:{
        color: 'green'
    }
});