import React, {Component, useEffect, useState} from "react";
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    StatusBar,
    SafeAreaView,
    ScrollView,
    Dimensions,
    Button,
    Platform,
    InteractionManager,
} from "react-native";
import MapView, {PROVIDER_GOOGLE, Region} from "react-native-maps";
import {Marker} from "react-native-maps";
import {useNavigation} from "@react-navigation/native";
import Icon from "react-native-ionicons";
import {useDispatch, useSelector, useStore} from "react-redux";
import {RootState} from "../store";
import { beginLoading, doneLoading } from "../redux/nav/action";
import { AfterInteractions } from 'react-native-interactions';

export interface IMap {
    name: string;
    address: string;
    location_type: string;
    // type: string;
}

export function RecycleMap() {
    const navigation = useNavigation();
    const state = useStore().getState();
    const [region, setRegion] = useState<Region>({
        latitude: 22.302711,
        longitude: 114.177216,
        latitudeDelta: 0.1100,
        longitudeDelta: 0.1100,
    });
    const [recyclePointName, setRecyclePointName] = useState<string>();
    const [recyclePointDescription, setRecyclePointDescription] = useState<string>();
    const [recyclePointAddress, setRecyclePointAddress] = useState<string>();
    const [showMap, setShowMap] = useState<boolean>(false)
    const dispatch = useDispatch();

    const location = useSelector((state: RootState) => state.map.location);

    async function fetchAddress(locationInput: IMap) {
        try{
            let res = await fetch(
            "https://maps.google.com/maps/api/geocode/json?address=" +
                locationInput.address +
                "&key=AIzaSyCM3IPnzdsFPyaB8dEPTu8VIgdIS1XaLzU"
            );
            
            
            let result = await res.json();
                
                setRegion({
                    latitude: result.results[0].geometry.location.lat,
                    longitude: result.results[0].geometry.location.lng,
                    latitudeDelta: 0.0018,
                    longitudeDelta: 0.0018,
                });
                
                setRecyclePointName(locationInput.name);
                setRecyclePointDescription(locationInput.location_type);
                setRecyclePointAddress(locationInput.address);

            
        } catch(e){
            
            fetchAddress({
                ...locationInput,
                address: locationInput.address.slice(0, - 1)
            })
        }
    }

    useEffect(() => {
        if (location) {
            dispatch(beginLoading())
            fetchAddress(location)
            .then(()=>setShowMap(true))
            .then(()=>dispatch(doneLoading()))
        }
    }, [location]);

    return (
        <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                {/* <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}> */}
                <View style={styles.header}>
                    {/* <Text>Header</Text> */}

                    <View
                        style={{flexDirection: "row", alignItems: "center"}}
                    >
                        <View style={styles.backButton}>
                            <Icon
                                name={"chevron-back-outline"}
                                onPress={() => {
                                    navigation.goBack();
                                }}
                            />
                        </View>

                        <View
                            style={{
                                flex: 10,
                                justifyContent: "center",
                                alignItems: "center",
                                // height: 60,
                                // padding: 10,
                            }}>
                            <Text style={{fontSize:30}}>{recyclePointName}</Text>
                        </View>

                        <View
                            style={{flex: 1}}>
                            
                        </View>

                    </View>
                </View>

                <View style={styles.main}>
                    <Text> main</Text>

                    <AfterInteractions>
                        {showMap && <MapView
                            style={styles.map}
                            provider={PROVIDER_GOOGLE}
                            region={region}
                        >
                            <Marker
                                key={1}
                                coordinate={region}
                                // title={recyclePointAddress}
                            />
                        </MapView>}
                    </AfterInteractions>
                </View>

                {/* <View style={styles.footer}>
                        <Text>footer</Text>
                    </View> */}
                {/* </ScrollView> */}
            </SafeAreaView>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#DCEBD8",
    },

    scrollView: {
        height: "100%",
        backgroundColor: "#DCEBD8",
    },

    header: {
        margin: 10,
        // backgroundColor:"red"
    },

    main: {
        // ...StyleSheet.absoluteFillObject,
        justifyContent: "flex-end",
        alignItems: "flex-end",
        height: Dimensions.get("window").height,
        width: Dimensions.get("window").width,
        // justifyContent: 'center'
    },
    footer: {
        flex: 1,
    },

    input: {
        textAlign: "center",
        height: 40,
        borderColor: "gray",
        backgroundColor: "#ffffff",
        borderWidth: 0,
        borderRadius: 10,
        marginBottom: 20,
        width: "80%",
    },
    button: {
        display: "flex",
        justifyContent: "center",
        backgroundColor: "#ffffff",
        borderRadius: 10,
        padding: 10,
        margin: 5,
    },
    content: {
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 15,
        borderColor: "rgba(0, 0, 0, 0.1)",
    },
    contentTitle: {
        fontSize: 20,
        marginBottom: 12,
        textAlign: "center",
        lineHeight: 30,
    },
    buttonGroup: {
        alignItems: "flex-start",
        padding: 5,
        width: "100%",
        justifyContent: "space-around",
        flexDirection: "row",
        flexWrap: "wrap",
    },
    backButton: {
        // margin: 10
        // marginBottom:10
    },

    map: {
        ...StyleSheet.absoluteFillObject,
    },
});
