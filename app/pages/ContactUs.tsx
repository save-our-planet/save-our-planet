import {useNavigation} from "@react-navigation/native";
import React, {useCallback} from "react";
import {
    Alert,
    Dimensions,
    Linking,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import Icon from "react-native-ionicons";

const email = "saveourhome1012@gmail.com";
const OpenURLButton = (url: string, children: string) => {
    const handlePress = useCallback(async () => {
        // Checking if the link is supported for links with custom URL scheme.
        const supported = await Linking.canOpenURL("mailto:" + url);

        if (supported) {
            // Opening the link with some app, if the URL scheme is "http" the web link should be opened
            // by some browser in the mobile
            await Linking.openURL("mailto:" + url);
        } else {
            Alert.alert(`No app for opening this link: ${url}`);
        }
    }, [url]);

    return (
        <TouchableOpacity onPress={handlePress}>
            <Text style={{fontSize: 24, color: "#2676d9"}}>{children}</Text>
        </TouchableOpacity>
    );
};

export function ContactUs() {
    const navigation = useNavigation();

    return (
        <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.container}>
                <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
                    <View>
                        <View
                            style={{
                                flexDirection: "row",
                                alignItems: "center",
                                margin: 15,
                            }}>
                            <View style={{flex: 1}}>
                                <Icon
                                    name={"chevron-back-outline"}
                                    onPress={() => {
                                        navigation.goBack();
                                    }}
                                />
                            </View>

                            <View style={{flex: 10}}>
                                <Text
                                    style={{
                                        fontSize: 30,
                                        textAlign: "center",
                                    }}>
                                    聯絡我們
                                </Text>
                            </View>

                            <View style={{flex: 1}}></View>
                        </View>
                    </View>

                    <View style={styles.main}>{OpenURLButton(email, email)}</View>
                </ScrollView>
            </SafeAreaView>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#DCEBD8",
    },
    scrollView: {
        height: "100%",
        backgroundColor: "#DCEBD8",
    },
    main: {
        marginHorizontal: Dimensions.get("window").width * 0.05,
        flex: 1,
        paddingTop: 20,
        alignItems: "center",
        // height: Dimensions.get("window").height * 0.7,
    },
});
