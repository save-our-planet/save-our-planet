import {useNavigation} from '@react-navigation/native';
import {Dispatch} from 'redux';
import {RootState} from '../../store';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { REACT_APP_BACKEND_URL } from "../../env";
import { doneLoading } from '../nav/action';

export function loginSuccess(token: string) {
  return {
    type: '@@auth/LOGIN_SUCCESS' as '@@auth/LOGIN_SUCCESS',
    token,
  };
}

export function loginLoading() {
  return {
    type: '@@auth/LOGIN_LOADING' as '@@auth/LOGIN_LOADING',
  }
}

export function loginFailed() {
  return {
    type: '@@auth/LOGIN_FAILED' as '@@auth/LOGIN_FAILED',
  };
}

export function logoutSuccess() {
  return {
    type: '@@auth/LOGOUT_SUCCESS' as '@@auth/LOGOUT_SUCCESS',
  };
}

export type AuthActions =
  | ReturnType<typeof loginSuccess>
  | ReturnType<typeof loginFailed>
  | ReturnType<typeof logoutSuccess>
  | ReturnType<typeof loginLoading>


export async function login(username: string, password: string){
  const res = await fetch(`${REACT_APP_BACKEND_URL}/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({username, password}),
  });

  const json = await res.json();
  console.log('action.ts res.json: ', json);
  const token = json.token;

  if (json.token) {
    const storeToken = async (token: string) => {
      try {

        await AsyncStorage.setItem('@storageToken_Key', token);
        console.log('setItem success');
        
      } catch (e) {
        console.log((e))
      }
    };
    storeToken(token);
    return token
  }
  return null
}



// export function dispatchLogin(username: string, password: string) {
//   return async (dispatch: Dispatch<any>) => {
//     console.log('action.ts login function running');

//     const res = await fetch(`${REACT_APP_BACKEND_URL}/login`, {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json',
//       },
//       body: JSON.stringify({username, password}),
//     });

//     console.log('ran action.ts login res');

//     const json = await res.json();
//     console.log('action.ts res.json: ', json);
//     const token = json.token;

//     if (json.token) {
//       const storeToken = async (token: string) => {
//         try {
//           //AsyncStorage promise.then version
//           // const jsonValue = JSON.stringify(token)
//           // AsyncStorage.setItem('@storageToken_Key', token)
//           //     .then(
//           //         async () => {
//           //             console.log('setItem success')
//           //             const myToken = await AsyncStorage.getItem('@storageToken_Key')
//           //             console.log(`myToken is ${myToken}`)
//           //         }
//           //     )

//           await AsyncStorage.setItem('@storageToken_Key', token);
//           console.log('setItem success');
//           // const myToken = await AsyncStorage.getItem('@storageToken_Key');
//           // console.log(`myToken is ${myToken}`);
//           // console.log("action.ts if(json.token): ", await AsyncStorage.setItem('@storageToken_Key', token))
//         } catch (e) {
//           console.log((e))
//         }
//       };
//       storeToken(token);

//       dispatch(loginSuccess(json.token));
//     } else {
//       dispatch(loginFailed());
//     }
//   };
// }

export function checkLogin(token: string | null) {
  return async (dispatch: Dispatch<any>) => {
    if (token == null) {
      dispatch(loginFailed());
      dispatch(doneLoading())
      return;
    }
    
    const res = await fetch(`${REACT_APP_BACKEND_URL}/currentUser`, {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    });
    
    const json = await res.json();
    console.log("json: " ,json);
    
    
    if (json) {
      dispatch(loginSuccess(token));
      dispatch(doneLoading())
    } else {
      dispatch(loginFailed());
      dispatch(doneLoading())
    }
  };
}
