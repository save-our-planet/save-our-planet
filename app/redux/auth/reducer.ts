import { AuthActions } from "./action"

export interface AuthState {
    isAuthenticated: boolean | null | "loading"
    username: string | null
    token: string | null
}

const initialState: AuthState = {
    isAuthenticated: null,
    username: null,
    token: null
}

export const authReducer = (state:AuthState = initialState, action: AuthActions): AuthState => {
    
    if (action.type === '@@auth/LOGIN_LOADING') {
        return {
            ...state,
            isAuthenticated: 'loading'
        }
    }

    if (action.type === '@@auth/LOGIN_SUCCESS') {
        return {
            ...state,
            isAuthenticated: true,
            token: action.token
        }
    }

    if (action.type === '@@auth/LOGIN_FAILED') {
        return {
            ...state,
            isAuthenticated: false
        }
    }

    if (action.type === '@@auth/LOGOUT_SUCCESS') {
        return {
            ...state,
            isAuthenticated: null,
            username: null,
            token: null
        }
    }
    
    return state;
}