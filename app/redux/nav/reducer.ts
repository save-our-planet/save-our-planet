
import { NavActions } from "./action"

export interface NavState {
    loading: boolean
}

const initialState: NavState = {
    loading: true
}

export const navReducer = (state:NavState = initialState, action: NavActions): NavState => {
    if (action.type === '@@nav/LOADING') {
        return {
            loading: true
        }
    }

    if (action.type === '@@nav/DONE_LOADING') {
        return {
            loading: false
        }
    }

    return state
}