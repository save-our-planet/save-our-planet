
export function beginLoading(){
    return{
        type: '@@nav/LOADING' as '@@nav/LOADING',
    }
}

export function doneLoading(){
    return{
        type: '@@nav/DONE_LOADING' as '@@nav/DONE_LOADING',
    }
}

export type NavActions = ReturnType<typeof beginLoading>
                            | ReturnType<typeof doneLoading>
