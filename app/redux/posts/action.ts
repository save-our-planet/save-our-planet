import {Dispatch} from "redux";
import { IPost } from "../../pages/Posts";
import { REACT_APP_BACKEND_URL } from "../../env";
import { useDispatch } from "react-redux";
import { doneLoading } from "../nav/action";

export function postsUpdated(posts: Array<IPost>){
    return {
        type: '@@posts/updated' as '@@posts/updated',
        posts
    };
}

export type PostsActions = ReturnType<typeof postsUpdated>

export function loadPosts(){
    return async (dispatch: Dispatch<any>) => {
        const postsRes = await fetch(`${REACT_APP_BACKEND_URL}/posts`);
        const posts = await postsRes.json();
        
        for(let post of posts){
            post.photos = await loadPhotos(post.id)
        }
        dispatch(doneLoading())
        return dispatch(postsUpdated(posts))
    }
}

async function loadPhotos(postID: number){

    const photosRes = await fetch(`${REACT_APP_BACKEND_URL}/post-photos/${postID}`);
    const photos = await photosRes.json()
    
    return photos
}