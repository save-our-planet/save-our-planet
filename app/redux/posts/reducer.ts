import { IPost } from "../../pages/Posts"
import { PostsActions } from "./action"

export interface PostsState{
    posts: Array<IPost>
}

const initialState: PostsState = {
    posts: []
}

export const postsReducer = (state:PostsState = initialState, action: PostsActions): PostsState => {
    if (action.type === '@@posts/updated') {
        
        return {
            posts: action.posts
        }
    }
    return state
}