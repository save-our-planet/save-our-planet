import { IMap } from "../../pages/RecycleMap"
import {MapActions} from "./action"

export interface MapState {
    location: IMap | null
}

const initialState: MapState = {
    location: null
}

export const mapReducer = (state:MapState = initialState, action: MapActions): MapState => {
    if (action.type === '@@map/SET_LOCATION') {
        return {
            location: action.location
        }
    }
    return state
}