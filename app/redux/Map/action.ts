
import { IMap } from "../../pages/RecycleMap";


export function setMapLocation(location: IMap){
    return{
        type: '@@map/SET_LOCATION' as '@@map/SET_LOCATION',
        location
    }
}

export type MapActions = ReturnType<typeof setMapLocation>
