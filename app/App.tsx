/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useEffect } from 'react';
import SplashScreen from 'react-native-splash-screen';
import io from 'socket.io-client';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider} from 'react-redux';
import {store} from './store';
import Screen from './Screen';
import { REACT_APP_BACKEND_URL } from "./env";

// export const windowWidth = useWindowDimensions().width;
const Stack = createStackNavigator();
export const socket = io(`${REACT_APP_BACKEND_URL}`);

const App = () => {

  useEffect(()=>{
    SplashScreen.hide();
  }, []);

  return (
    <Provider store={store}>
      
      <NavigationContainer>
          <Screen />
      </NavigationContainer>
    </Provider>
  );
};


export default App;
