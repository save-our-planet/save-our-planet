import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunk from "redux-thunk";
import { authReducer, AuthState } from "./redux/auth/reducer";
import { mapReducer, MapState } from "./redux/Map/reducer";
import { navReducer, NavState } from "./redux/nav/reducer";
import { postsReducer, PostsState } from "./redux/posts/reducer";
export interface RootState {
    auth: AuthState,
    posts: PostsState,
    map: MapState,
    nav: NavState,
} 

const rootReducer = combineReducers<RootState>({
    auth: authReducer,
    posts: postsReducer,
    map: mapReducer,
    nav: navReducer,
})

export const store = createStore(rootReducer, compose(
    applyMiddleware(thunk),
));