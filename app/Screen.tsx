import React, { useEffect } from 'react';
import { KeyboardAvoidingView, Modal, Platform, StyleSheet, View } from 'react-native';

import { Colors } from 'react-native/Libraries/NewAppScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Login } from './pages/Login';
import { HomeTab } from './pages/Home';
import { SignUp } from './pages/Signup';
import { Provider, useDispatch, useSelector } from 'react-redux';
import { RootState } from './store';
import { checkLogin, login, loginFailed, loginSuccess } from './redux/auth/action';
import { store } from './store';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Loading } from './pages/LoadingPage';

const Stack = createStackNavigator();

const Screen = () => {
    const loading = useSelector((state: RootState) => state.nav.loading);
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    const dispatch = useDispatch();

    useEffect( () => {
      AsyncStorage.getItem('@storageToken_Key')
        .then(el => {
        dispatch(checkLogin(el))
      })
      // console.log("Screen.tsx - loginSuccess")
    },[isAuthenticated])
    
  return (
      <>
          {/* <Loading /> */}
          {loading && <Loading />}
          <Stack.Navigator>
            {/* {!isAuthenticated && <Stack.Screen name='Login' component={Login} options={{headerShown: false}}/>}
            {!isAuthenticated && <Stack.Screen name='Sign-up' component={SignUp} options={{headerShown: false}}/>} */}
            {/* {isAuthenticated &&  */}
            <Stack.Screen name='Home-Tab' component={HomeTab} options={{headerShown: false}}/>
            {/* } */}
          </Stack.Navigator>

      </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    height: '100%',
    backgroundColor: '#DCEBD8',
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: '#DCEBD8',
  },
  input: {
    height: 40,
    borderColor: 'gray',
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 10,
    margin: 5,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  }
});

export default Screen;
